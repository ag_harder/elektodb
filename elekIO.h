 /*
 * ELEKIO_H for HALO Hardware
 * 14 Slot Backplane
 *
 */

/* #define DEBUG_NOHARDWARE 0 */

#ifndef ELEKIO_H
#define ELEKIO_H
/*
#include <stdint.h>
*/

#include <sys/types.h>

#ifndef int8_t
#define int8_t signed char
#endif

#ifndef uint8_t
#define uint8_t unsigned char
#endif

#ifndef uint16_t
#define uint16_t unsigned short
#endif

#ifndef int16_t
#define int16_t short
#endif

#ifndef int32_t
#define int32_t int
#endif

#ifndef uint32_t
#define uint32_t unsigned int
#endif

#ifndef int64_t
#define int64_t long long
#endif

#ifndef uint64_t
#define uint64_t unsigned long long
#endif

#ifdef _WIN32

struct timeval {
  long    tv_sec;
  long    tv_usec;
};

struct timespec {
	long    tv_sec;         /* seconds */
	long    tv_nsec;        /* nanoseconds */
};


#undef __FD_SETSIZE
#define __FD_SETSIZE    1024

typedef int socklen_t;


#endif

#include <sys/types.h>
#pragma pack(push,1)

#define INIT_MODULE_FAILED  0
#define INIT_MODULE_SUCCESS 1

#define MAX_ADC_CARD_HORUS              3       /* number of 16bit ADC Cards in HORUS-AIR */
#define MAX_ADC_CARD_CALIB             1       /* number of 16bit ADC Cards in Calibrator */
#define MAX_ADC_CHANNEL_PER_CARD       8       /* number of Channels on each 16bit ADC Card */

#define MAX_MFC_CARD_HORUS              1       /* number of MFC Cards in HORUS-AIR */
#define MAX_MFC_CARD_CALIB             1       /* number of MFC Cards in Calibrator */
#define MAX_MFC_CHANNEL_PER_CARD       4       /* number of Channels on each MFC Card */

#define MAX_VALVE_CARD_HORUS            2       /* number of Valve Cards in HORUS-AIR */
#define MAX_VALVE_CHANNEL_PER_CARD     14      /* number of Valves on Card */
#define MAX_VALVE_CARD_VOLTRED_CNT     25       /* number of cycles to wait before valvecard voltage is reduced to non-release voltage*/

#define MAX_DCDC4_CARD_HORUS            1       /* number of DCDC4 in Lift */
#define MAX_DCDC4_CHANNEL_PER_CARD     4       /* number of Channels on DCDC4 */

#define MAX_SCR3XB_CALIB               1       /* number of SCR3XB Cards in Calibrator (for AC power devices) */
#define MAX_SCR3XB_CHANNEL_PER_CARD    3       /* number of Channels on SCR3XB*/

#define MAX_LICORS_CALIB               1       /* number of LICORs on Calibrator Unit */

#define MAXSPECTRALLINES               3840    /* number of spectrum lines for HR4000 */

#define MAX_MFC_PROFIBUS               10      /* number of MFC Channels from the Profibus mapped */

#ifndef DEBUG_NOHARDWARE
#define ELK_TIMEOUT (unsigned) 0x1000
#else
#define ELK_TIMEOUT (unsigned) 0x0001
#endif

#define ELK_BASE (uint16_t)0x200
#define ELK_ADR  ELK_BASE
#define ELK_DATA (ELK_BASE + (uint16_t) 2)
#define ELK_TODO (ELK_BASE + (uint16_t) 4)
#define ELK_QSIZE 4

#define ELK_BACKPLANE_BASE   0xa400

#define ELK_STEP_BASE        0xa640
#define ELK_STEP_SETPOS      (ELK_STEP_BASE + 0x0010)
#define ELK_STEP_GETPOS      (ELK_STEP_BASE + 0x0008)
#define ELK_STEP_SETSPD      (ELK_STEP_BASE + 0x0014) /* velo=0x14 acc=0x15 */
#define ELK_STEP_GETSPD      (ELK_STEP_BASE + 0x000c)
#define ELK_STEP_GETENCPOS   (ELK_STEP_BASE + 0x0020)
#define ELK_STEP_GETINDPOS   (ELK_STEP_BASE + 0x0024)
#define ELK_STEP_SETVOLT     (ELK_STEP_BASE + 0x0016) /* zero=x16 slow=x17 */
#define ELK_STEP_STATUS      (ELK_STEP_BASE + 0x001e) /* switch is bit 0&1 */
#define ELK_STEP_MODE        (ELK_STEP_BASE + 0x001c) /* mode, 4=copy encoder to index with next index */

/* We have the filament card @0xA600*/
#define ELK_STEP_OHR_BASE        0xa600
#define ELK_STEP_OHR_SETPOS      (ELK_STEP_OHR_BASE + 0x0010)
#define ELK_STEP_OHR_GETPOS      (ELK_STEP_OHR_BASE + 0x0008)
#define ELK_STEP_OHR_SETSPD      (ELK_STEP_OHR_BASE + 0x0014) /* velo=0x14 acc=0x15 */
#define ELK_STEP_OHR_GETSPD      (ELK_STEP_OHR_BASE + 0x000c)
#define ELK_STEP_OHR_GETENCPOS   (ELK_STEP_OHR_BASE + 0x0020)
#define ELK_STEP_OHR_GETINDPOS   (ELK_STEP_OHR_BASE + 0x0024)
#define ELK_STEP_OHR_SETVOLT     (ELK_STEP_OHR_BASE + 0x0016) /* zero=x16 slow=x17 */
#define ELK_STEP_OHR_STATUS      (ELK_STEP_OHR_BASE + 0x001e) /* switch is bit 0&1 */
#define ELK_STEP_OHR_MODE        (ELK_STEP_BASE + 0x001c) /* mode, 4=copy encoder to index with next index */


#define ELK_DACPWM_BASE      (ELK_BACKPLANE_BASE+0x04)
#define ELK_DACPWM_BASE_WP   (ELK_BACKPLANE_BASE+0x04)
#define ELK_VALVE_BASE       (ELK_BACKPLANE_BASE+0x08)
#define ELK_DAC_BASE         (ELK_BACKPLANE_BASE+0x40)
#define ELK_DAC_BASE_WP      (ELK_BACKPLANE_BASE+0x40)
#define ELK_DAC_BASE_CALIB   (ELK_BACKPLANE_BASE+0x40)
#define ELK_PWM_BASE         (ELK_BACKPLANE_BASE+0x60)
#define ELK_PWM_DCDC4_BASE   (ELK_PWM_BASE)
#define ELK_PWM_VALVE_BASE   (ELK_PWM_BASE+0x08)


#define ELK_PWM_VALVE_BASE_WP (0xa460)

#define ELK_ADC_BASE         (ELK_BACKPLANE_BASE+0x80)
#define ELK_ADC_BASE_WP      (ELK_BACKPLANE_BASE+0x80)

#define ELK_MFC_BASE         (ELK_BACKPLANE_BASE+0xe0)          /* Base adr. of MFC ADC channels */
#define ELK_MFC_BASE_WP      (ELK_BACKPLANE_BASE+0xe0)
#define ELK_MFC_BASE_CALIB   (ELK_BACKPLANE_BASE+0xa0)


#define ELK_ADC_CONFIG          (0x0010)                        /* add to base addr */
#define ELK_ADC_NUM_ADR         (0x0020)                        /* number of addresses each ADC channel has */

#define ELK_MFC_CONFIG          (ELK_ADC_CONFIG)                /* offs. base addr for MFC Config*/
#define ELK_MFC_NUM_ADR         (ELK_ADC_NUM_ADR)               /* number of addresses each MFC channel has */
#define ELK_DAC_NUM_ADR         (MAX_DCDC4_CHANNEL_PER_CARD<<1) /* number of addresses each MFC channel has */

#define ELK_MFC_CONFIG_WP       (ELK_ADC_CONFIG)                /* offs. base addr for MFC Config*/
#define ELK_MFC_NUM_ADR_WP      (ELK_ADC_NUM_ADR)               /* number of addresses each MFC channel has */
#define ELK_DAC_NUM_ADR_WP      (MAX_DCDC4_CHANNEL_PER_CARD<<1) /* number of addresses each MFC channel has */

#define ELK_MFC_CONFIG_CALIB    (ELK_ADC_CONFIG)                /* offs. base addr for MFC Config*/
#define ELK_MFC_NUM_ADR_CALIB   (ELK_ADC_NUM_ADR)               /* number of addresses each MFC channel has */
#define ELK_DAC_NUM_ADR_CALIB   (MAX_DCDC4_CHANNEL_PER_CARD<<1) /* number of addresses each MFC channel has */


/* define for SCR3 in calibrator unit */
#define ELK_SCR_BASE           (ELK_BACKPLANE_BASE+0x10)        /* SCR Power card */

#define ELK_STAT_TIMEOUT 0
#define ELK_STAT_OK      1

/***********************/
/* COUNTER CARD OH+HO2 */
/***********************/
#define ELK_COUNTER_BASE        0xa300
#define ELK_COUNTER_ADC         (ELK_COUNTER_BASE)
#define ELK_COUNTER_DELAY_SHIFT (ELK_COUNTER_BASE + 0x0010)
#define ELK_COUNTER_DELAY_GATE  (ELK_COUNTER_BASE + 0x0018)
#define ELK_COUNTER_STATUS      (ELK_COUNTER_BASE + 0x0020)
#define ELK_COUNTER_MASK        (ELK_COUNTER_BASE + 0x0030)
#define ELK_COUNTER_SUMCOUNTS   (ELK_COUNTER_BASE + 0x0038)
#define ELK_COUNTER_COUNTS      (ELK_COUNTER_BASE + 0x0080)

#define ELK_COUNTER_STATUS_FLIP 0x8000
#define ELK_COUNTER_STATUS_BUSY 0x0200

#define MAX_COUNTER_CHANNEL        3    /* number of channels per Card */
#define MAX_COUNTER_GATE           2    /* number of Gate Channel      */
#define MAX_COUNTER_PAGE           3    /* number of pages per Channel */
#define MAX_COUNTER_TIMESLOT       (MAX_COUNTER_SLOTS_PER_PAGE*MAX_COUNTER_PAGE)  /* number of timeslots for each channel */
#define MAX_COUNTER_SLOTS_PER_PAGE 64   /* number of timeslots on each page */


#define ADC_CHANNEL_COUNTER_CARD 8
#define COUNTER_MASK_WIDTH       12     /* number of words for the mask reg. (12x16=192=160+32) */

struct ChannelListType {              /* generic Type for data channels */
    uint16_t Addr;
    uint16_t Value;
}; /* ChannelListType */

struct CounterChannelDataType {
    uint16_t  ShiftDelay;              /* delay after Masterdelay until shift register starts shifting */
    uint16_t  GateDelay;               /* delay after Masterdelay until GainGate is activated */
    uint16_t  GateWidth;               /* time width while GainGate is on */
    uint16_t  Data[MAX_COUNTER_TIMESLOT]; /* Shift register in which incoming counts are stored and summed */
    uint16_t  Mask[COUNTER_MASK_WIDTH];/* Masks interesting counts from data */
    uint16_t  Counts;                  /* sum of all Data counts masked by Mask */
    uint16_t  Pulses;                  /* number of pulses accumulated in Data */
}; /* ChannleDataType */

struct CounterCardType {

	struct timeval                  TimeOfUpdate;                                   /* time of update of the data */
    uint16_t                        ADCData[ADC_CHANNEL_COUNTER_CARD];  /* PMTthresh, LaserTrigThresh, MCP1Thresh,  */
                                                  /* MCP2Thresh, +5, +28 +3.3 +1.5 */
    uint16_t                        MasterDelay;
    struct CounterChannelDataType   Channel[MAX_COUNTER_CHANNEL];
}; /* CounterCardType */

/***********************/
/* COUNTER CARD NO2    */
/***********************/

/* register mapping CC2 */
/* counts */

#define CC2_BASE (0xD000)

#define OFF_CC2_RAMMIN (0x00)   /* first address of usable counts RAM (RW)*/
#define OFF_CC2_RAMMAX (0xEF)   /* end of counts RAM (RW) */

/* registers */

#define CC2_SIGNATURE  (0xCC02) /* Counter Card II */

#define OFF_CC2_SIG    (0xF0)   /* card signature is found here (RO) */
#define OFF_CC2_REV    (0xF1)   /* HW revision (RO) */

#define OFF_CC2_CTRL   (0xF2)   /* control register */
#define CTRL_STARTCOPY (0x0001) /* bit 0 starts copying of counter values into RAM */
#define CTRL_TESTMODE  (0x0004) /* bit 2 is testmode (counter increment with each laserpulse */
#define CTRL_MOD_EN    (0x0008) /* bit 3 modulation master enable bit */
#define CTRL_MOD_MAIN  (0x0010) /* bit 4 enables main modulation output (1st diode)*/
#define CTRL_MOD_AUX   (0x0020) /* bit 5 enables aux modulation output (2nd diode)*/
#define CTRL_RESET     (0x8000) /* bit 15 resets counters*/

#define OFF_CC2_SPARE1 (0xF3)   /* spare */
#define OFF_CC2_SPARE2 (0xF4)   /* spare */

#define OFF_CC2_CNTDEL (0xF5)   /* counter start delay in 4ns increments after*/
                                /* falling edge of laser trigger */

#define OFF_CC2_PULSEW (0xF6)   /* modulation pulse width in 4ns increments */
#define OFF_CC2_PAUSEW (0xF7)   /* modulation pause width in 4ns increments */

#define OFF_CC2_ADC0   (0xF8)   /* ADC Channel #0, PMT Threshold */
#define OFF_CC2_ADC1   (0xF9)   /* ADC Channel #1, ext. SYNC Threshold */
#define OFF_CC2_ADC2   (0xFA)   /* ADC Channel #2, comp. Thresh. MCP1 */
#define OFF_CC2_ADC3   (0xFB)   /* ADC Channel #3, +15V monitor */
#define OFF_CC2_ADC4   (0xFC)   /* ADC Channel #4, +5V monitor */
#define OFF_CC2_ADC5   (0xFD)   /* ADC Channel #5, +28V monitor */
#define OFF_CC2_ADC6   (0xFE)   /* ADC Channel #6, +3V3 monitor */
#define OFF_CC2_ADC7   (0xFF)   /* ADC Channel #7, +1.2V core monitor */

/* overlay 16bit memory organisation with 32bit counters*/
union NO2CounterType
{
   uint16_t RawArray[112*2]; /* we have 112 counters, with 32 bits each*/
     uint32_t CounterArray[112];     /* Status, end switch, index      */
};

/* 16bit overlay for CC2 Control register, to allow access to single bits */
union NOCtrlRegType
{
   uint16_t RawRegister;
   uint16_t Bit_Reset:1;
   uint16_t Reserved:9;
   uint16_t Bit_ModAux:1;
   uint16_t Bit_ModMain:1;
   uint16_t Bit_ModEnable:1;
   uint16_t Bit_TestMode:1;
   uint16_t Bit_EvenMoreReserved:1;
   uint16_t Bit_StartCopy:1;
};

struct NO2ControlRegType
{
   uint16_t CardSignature; /* should read 0xCC02 */
   uint16_t CardRevision;  /* most recent is 4, with support for dual diode laser*/
   union NOCtrlRegType CardControl; /* control register, union for bit manipulation */
   uint16_t CardSpare1;
   uint16_t CardSpare2;
   uint16_t DelayRegister; /* counter start delay (4ns * X)after falling edge of laser trigger */
   uint16_t PulseWidth; /* Pulse width in 4ns multiples */
   uint16_t PauseWidth; /* Pause width in 4ns multiples */
   uint16_t ADCChannels[8]; /* voltage monitoring on card*/
};

/* Structure for CC2 card */
struct NO2CounterCardType
{
   struct timeval              TimeOfUpdate;                                   /* time of update of the data */
   union  NO2CounterType       Counters;
   struct NO2ControlRegType    Controls;
};

/*************************************************************************************************************/
/* Backplane ADC                                                                                             */
/*************************************************************************************************************/

/* we have 4 consecutive words for ADC Channels, so layout is like (ADC0.0 ADC1.0 ADC2.0 dummy.0)            */
/*                                                                 (ADC0.1 ADC1.1 ADC2.1 dummy.1)            */
/*                                                                 (..... etc ....)                          */
/*                                                                                                           */
/* 32 words in total */

#define ELK_BACKPLANE_ADC_BASE (0xF000)

struct BackplaneSubADCType
{
   uint16_t DCCurrents_A;
   uint16_t DCCurrents_B;
   uint16_t DCVoltages;
   uint16_t TrueRMS_Reserved;
};

/* we have 8 channels for each SubADC
 */

union BackplaneADCUnionType
{
   uint16_t RawArray[32];
   struct BackplaneSubADCType Channel[8];
};

struct BackplaneADCType
{
   struct timeval                 TimeOfUpdate;                                   /* time of update of the data */
   union  BackplaneADCUnionType   ADC;
};
/*************************************************************************************************************/
/* Filament Card                                                                                             */
/*************************************************************************************************************/

/* 32 words in total @0xA600 */

#define ELK_FILA_BASE (0xA600)

/* this will hold the different registers
 */

struct FilamentSplitType
{
   /* */
   /* 4 words or 8 bytes with card ID String
    */
   uint8_t  aSignature[8];     /* WA: 0x00-0x03 */
   uint16_t usDummy;           /* WA: 0x04*/
   uint16_t usEmissionVoltage; /* WA: 0x05 set > 100 */
   uint16_t usEmissionCurrent; /* WA: 0x06 set according to requirements (2.9mA/ct roughly) */
   uint8_t  ucCommStatus;      /* WA: 0x07 LSB */
   uint8_t  ucErrCount;        /* WA: 0x07 MSB */
   uint8_t  ucOscFineCount;    /* WA: 0x08 LSB */
   uint8_t  ActionCnt;         /* WA: 0x08 MSB */
   uint16_t  aMainFill[7];     /* WA: 0x09-0x0F */

   /* read back area from card (0x10 Words) */
   uint16_t usEmissionURead;   /* WA: 0x10 */
   uint16_t usEmissionIRead;   /* WA: 0x11*/
   uint16_t WordArray[14];
};

/* we have 8 channels for each SubADC
 */

union FilamentUnionType
{
   uint16_t RawArray[32];
   struct FilamentSplitType Registers;
};

struct FilamentCardType
{
   struct timeval                 TimeOfUpdate;                                   /* time of update of the data */
   union FilamentUnionType Filament;
};


/*************************************************************************************************************/
/* ETALON                                                                                                    */
/*************************************************************************************************************/

enum EtalonActionType { /* update also in etalon.c */
    ETALON_ACTION_TOGGLE_ONLINE_LEFT,          /* etalon is on the left ONLINE Position */
    ETALON_ACTION_TOGGLE_ONLINE_RIGHT,         /* etalon is on the right ONLINE Position */
    ETALON_ACTION_TOGGLE,                      /* etalon is toggling between on and offline */
    ETALON_ACTION_TOGGLE_OFFLINE_LEFT,         /* etalon is on the left OFFLINE Position */
    ETALON_ACTION_TOGGLE_OFFLINE_RIGHT,        /* etalon is on the right OFFLINE Position */
    ETALON_ACTION_NOP,                         /* etalon is doing no automated operation */
    ETALON_ACTION_DITHER_ONLINE,               /* stay online and dither */
    ETALON_ACTION_DITHER_ONLINE_LEFT,          /* stay online and dither left side */
    ETALON_ACTION_DITHER_ONLINE_RIGHT,         /* stay online and dither right side */
    ETALON_ACTION_SCAN,                        /* etalon is scanning */
    ETALON_ACTION_HOME,                        /* etalon is on a home run */
    ETALON_ACTION_FIND_ONLINE,		       /* etalon sets ONLINE Position to largest recent ref. signal */

    ETALON_ACTION_MAX
};

#define ETALON_STEPRATIO_ENCODER_MOTOR    2
#define ETALON_SCAN_POS_START      5
#define ETALON_SCAN_POS_STOP       20000
#define ETALON_SCAN_STEP_WIDTH     8

#define ETALON_STEP_POS_ONLINE     500
#define ETALON_STEP_DITHER         16
#define ETALON_STEP_OFFLINE        64

#define ETALON_STEP_ESW_RIGHT      8       /* 0x0100 Bit position of right end switch in status word of stepper card */
#define ETALON_STEP_ESW_LEFT       9       /* 0x0200 position of left end switch in status word of stepper card */

#define ETALON_CHANNEL_REF_CELL    0           /* Reference Cell Channel Number for etalon etc.. */


#define ETALON_DEFAULT_ACCSPD 0x1020

#define LIFE_REFSIGNAL 300

struct LongWordType {
  uint16_t Low;
  uint16_t High;
}; /* struct LongWordType */


union PositionType {
  struct LongWordType PositionWord;
  uint32_t Position;
}; /* union PositionType */

struct StatusFieldType {                                            /* Bit Field */
  uint16_t RefChannel:2;                                       /* Channel number of Reference Cell */
  uint16_t Unused1:6;                                          /* */
  uint16_t EndswitchRight:1;                                   /* Endschalter rechts aktiv */
  uint16_t EndswitchLeft:1;                                    /* Endschalter links aktiv */
  uint16_t Unused2:6;                                          /*  */
};

union StatusType {
  struct StatusFieldType StatusField;
  uint16_t StatusWord;                    /* Status, end switch, index      */
};

struct EtalonDataType {

  struct timeval     TimeOfUpdate;                                   /* time of update of the data */
  union PositionType Set;
  union PositionType Current;
  union PositionType Encoder;
  union PositionType Index;
  union PositionType Online;
  union PositionType ScanStart;
  union PositionType ScanStop;
  uint16_t           ScanStepWidth;
  uint16_t           DitherStepWidth;
  uint16_t           OfflineStepLeft;
  uint16_t           OfflineStepRight;

  uint16_t CurSpeed;               /* Current Speed */
  uint16_t SetSpeed;
  uint16_t SetAccl;
  union StatusType Status;		/* Status, end switch, index      */

}; /* EtalonDataType */

/*************************************************************************************************************/
/* OHR Defines */
/*************************************************************************************************************/

#define OHRSTEP_DEF_ACCSPD 0x1010


/*************************************************************************************************************/


struct ADCChannelConfigBitType {
    uint16_t MuxChannel:3;
    uint16_t Bridge:1;
    uint16_t Gain:2;
    uint16_t Offset:2;
    uint16_t Unused:8;
}; /* ADCChannelConfigBitType */

union ADCChannelConfigType {
    struct ADCChannelConfigBitType ADCChannelConfigBit;
    uint16_t ADCChannelConfig;
}; /* ADCConfigType */

struct ADCChannelDataType {
    uint16_t ADCData;
    uint32_t SumDat;
    uint32_t SumSqr;
}; /* ADCChannelType */

struct ADCCardType {

  struct timeval                 TimeOfUpdate;                                   /* time of update of the data */
  uint16_t NumSamples;                                                    /* number of Samples for statistik */
  struct ADCChannelDataType ADCChannelData[MAX_ADC_CHANNEL_PER_CARD];
  union  ADCChannelConfigType ADCChannelConfig[MAX_ADC_CHANNEL_PER_CARD];
}; /* ADCCardType */

/* Adjust here for Mirror Magic Alignment Foo */

#define DIODE_UV_ADCCARDMASTER_NUMBER 0
#define DIODE_UV_ADCCARDMASTER_CHANNEL 2
#define DIODE_WZ1IN_ADCCARDMASTER_NUMBER 1
#define DIODE_WZ1IN_ADCCARDMASTER_CHANNEL 0
#define DIODE_WZ2IN_ADCCARDMASTER_NUMBER 1
#define DIODE_WZ2IN_ADCCARDMASTER_CHANNEL 2

struct MFCChannelConfigBitType {
  uint16_t MuxChannel:3;
  uint16_t Unused:5;
  uint16_t Ch0:2;
  uint16_t Ch1:2;
  uint16_t Ch2:2;
  uint16_t Ch3:2;
}; /* MFCChannelConfigBitType */

union MFCChannelConfigType {
    struct MFCChannelConfigBitType MFCChannelConfigBit;
    uint16_t MFCChannelConfig;
}; /* MFCChannelConfigType */

struct MFCChannelDataType {
    uint16_t SetFlow;
    uint16_t Flow;
    uint32_t SumDat;
    uint32_t SumSqr;
}; /* MFCChannelType */

struct MFCCardType {
  struct timeval                 TimeOfUpdate;                                   /* time of update of the data */
  uint16_t NumSamples;                                                    /* number of Samples for statistik */
  struct MFCChannelDataType MFCChannelData[MAX_MFC_CHANNEL_PER_CARD];
  union  MFCChannelConfigType MFCChannelConfig[MAX_MFC_CHANNEL_PER_CARD];
}; /* MFCCardType */

struct SCRCardType {
  uint16_t SCRPowerValue[MAX_SCR3XB_CHANNEL_PER_CARD];
}; /* SCRCardType */

struct MFCConfigType {             /* this configuration data is supposed to be in a nice XML file, it is not stored in Status */
    uint64_t MaxFlow;              /* manufacturer stated max flow in SCCM */
    double   SetSlope;             /* Set point slope Flow/counts */
    double   SetOffset;            /* Set point offset in SCCM */
    double   MeasSlope;            /* Measured flow slope SCCM/counts */
    double   MeasOffset;           /* Measured flow offset in SCCM */
} ; /* MFCConfigType */

/*************************************************************************************************************/

struct ValveCardType {
    struct timeval                 TimeOfUpdate;                                   /* time of update of the data */
	uint16_t    ValveVolt;                                       /* voltage applied for Card */
	uint16_t    Valve;                                           /* each bit reprsensts one valve */
	uint16_t    VoltRedCnt;										 /* voltage reduction counter */
	};
/*************************************************************************************************************/


struct DCDC4CardType {
  struct timeval  TimeOfUpdate;                                   /* time of update of the data */
  uint16_t        Channel[MAX_DCDC4_CHANNEL_PER_CARD];                         /* voltage applied for Channel */
};


/*************************************************************************************************************/
#define MAX_TEMP_SENSOR   40
#define MAX_TEMP_SENSOR_CARD_HORUS 2
#define MAX_TEMP_SENSOR_CARD_WP 1
#define MAX_TEMP_SENSOR_CARD_CALIB 1
#define MAX_TEMP_TIMEOUT  100                                        /* maximum Timeout to wait for Temperature Card to be ready */
#define MAX_TEMP_MISSED_READING 5                                    /* number of maximal reading failures before removing a temperature */

#define ELK_TEMP_BASE           0xb000                               /* Base of Temperature Card (Calibrator, legacy)*/
#define ELK_TEMP_CTRL           (ELK_TEMP_BASE+0x000)                /* Controlword */
#define ELK_TEMP_ERR_CRC        (ELK_TEMP_BASE+0x002)                /* Number of CRC Errors */
#define ELK_TEMP_ERR_NORESPONSE (ELK_TEMP_BASE+0x004)                /* Number of No Response Errors */
#define ELK_TEMP_FOUND          (ELK_TEMP_BASE+0x006)                /* Number of Sensors found */
#define ELK_TEMP_DATA           (ELK_TEMP_BASE+0x008)                /* here the data field starts */
#define ELK_TEMP_DATA2          (ELK_TEMP_BASE + 0x200)              /* offset to next bank */

#define ELK_TEMP_BASE_IS           0xb000                            /* Base of Temperature Card for Infrastructure*/

#define ELK_TEMP_CTRL_IS           (ELK_TEMP_BASE_IS+0x000)          /* Controlword */
#define ELK_TEMP_ERR_CRC_IS        (ELK_TEMP_BASE_IS+0x002)          /* Number of CRC Errors */
#define ELK_TEMP_ERR_NORESPONSE_IS (ELK_TEMP_BASE_IS+0x004)          /* Number of No Response Errors */
#define ELK_TEMP_FOUND_IS          (ELK_TEMP_BASE_IS+0x006)          /* Number of Sensors found */
#define ELK_TEMP_DATA_IS           (ELK_TEMP_BASE_IS+0x008)          /* here the data field starts */
#define ELK_TEMP_DATA2_IS          (ELK_TEMP_BASE_IS+0x200)          /* offset to next bank */

#define ELK_TEMP_BASE_DL           0x8000                            /* Base of Temperature Card for DyeLaser Plate*/

#define ELK_TEMP_CTRL_DL           (ELK_TEMP_BASE_DL+0x000)          /* Controlword */
#define ELK_TEMP_ERR_CRC_DL        (ELK_TEMP_BASE_DL+0x002)          /* Number of CRC Errors */
#define ELK_TEMP_ERR_NORESPONSE_DL (ELK_TEMP_BASE_DL+0x004)          /* Number of No Response Errors */
#define ELK_TEMP_FOUND_DL          (ELK_TEMP_BASE_DL+0x006)          /* Number of Sensors found */
#define ELK_TEMP_DATA_DL           (ELK_TEMP_BASE_DL+0x008)           /* here the data field starts */
#define ELK_TEMP_DATA2_DL          (ELK_TEMP_BASE_DL+0x200)           /* offset to next bank */

#define ELK_TEMP_BUSYFLAG        0x0001                               /* we Request access */
#define ELK_TEMP_UPDATEFLAG      0x0002                               /* AVR is busy       */
#define ELK_TEMP_SCANFLAG        0x0004                               /* AVR is scanning for         */

struct TempSensorFieldType {                                            /* Bit Field */
  uint16_t TempFrac:4;                                             /* Temperatur Fraction 1/16 K */
  int16_t  TempMain:8;                                             /* Temperatur K --> Ges Temp=TempMain+TempFrac/16 */
  uint16_t bValid:1;                                               /* Temperatur Daten Gueltig=1 */
  uint16_t bCRCError:1;                                            /* CRC Fehler, Pruefsumme falsch */
  uint16_t bNoResponse:1;                                          /* Sensor antowrtet nicht */
  uint16_t bAlarmFlag:1;                                           /* Sensor meldet Alarm */
  unsigned char aROMCode[6];                                            /* 6 Byte ID vom Sensor */
  signed char cTempLimitMax;
  signed char cTempLimitMin;
};

struct TempSensorWordType {
  uint16_t WordTemp;
  uint16_t WordID[3];
  uint16_t WordLimit;
}; /*  TempSensorWordType */


union TempSensorDataType {
  struct TempSensorFieldType Field;
  struct TempSensorWordType Word;
}; /* TempSensorDataType  */


struct TempSensorControlFieldType {
  uint16_t Busy:1;
  uint16_t Update:1;
  uint16_t Scan:1;
  uint16_t Unused:12;
  uint16_t Reset:1;
};

union TempSensorControlType {
  struct TempSensorControlFieldType Field;
  uint16_t Word;
};

struct TempSensorCardType {
  struct timeval              TimeOfUpdate;                                   /* time of update of the data */
  union TempSensorControlType Control;
  uint16_t                    NumErrCRC;
  uint16_t                    NumErrNoResponse;
  uint16_t                    NumSensor;
  union TempSensorDataType    TempSensor[MAX_TEMP_SENSOR];

}; /* TempSensorCardType */

/*************************************************************************************************************/
/* LICOR stuff */
/*************************************************************************************************************/

struct LicorStatusFieldType
{
   uint16_t Reserved:13;    /* not used up to now */
   uint16_t Found:1;        /* LICOR is recognized by calibrator task */
   uint16_t Initialising:1;  /* LICOR is warming up */
   uint16_t DataValid:1;     /* LICOR data are valid */
};

union LicorStatusType
{
   struct LicorStatusFieldType Field;
   uint16_t Word;
};

struct LicorH2OCO2Type
{
   union LicorStatusType      Status;
   uint16_t                   LicorTemperature; /* Unit: degree kelvin * 100 e.g. 20 degree celsius -> 273,15 + 20,0 => 29315 */
   uint16_t                   AmbientPressure;  /* Unit: kPA * 100 e.g. 1002.7 mBar => 10027 */

   int16_t                    CO2A;             /* CO2 concentration cell A in mymol/mol, coding scheme T.B.D. */
   int16_t                    CO2B;             /* CO2 concentration cell B in mymol/mol, coding scheme T.B.D. */
   int16_t                    CO2D;             /* CO2 differential concentration in mymol/mol, coding scheme T.B.D. */

   int16_t                    H2OA;             /* H2O concentration cell A in mmol/mol, coding scheme T.B.D. */
   int16_t                    H2OB;             /* H2O concentration cell B in mmol/mol, coding scheme T.B.D. */
   int16_t                    H2OD;             /* H2O differential concentration in mmol/mol, coding scheme T.B.D. */
};

struct PIDregulatorType
{
   uint16_t Setpoint;          /* Kelvin x 100, e.g. 20�C => 27315 + (20 x 100) = 29315 */
   uint16_t ActualValueH2O;    /* Actual Value converted from thermistor Water */
   uint16_t ActualValueHeater; /* Actual Value converted from thermistor Heater Catridge*/
   uint16_t KP;                /* proportional coefficient */
   uint16_t KI;                /* intergrating coefficient */
   uint16_t KD;                /* differtial coefficient */
   uint16_t ControlValue;      /* regulator output */
};

/*************************************************************************************************************/
/* BUTTERFLY  */
/*************************************************************************************************************/

/* the stepperdriver on the butterfly tells us some parameters*/
/* might be interesting for debugging */

struct ButterflyMotorStatusFieldType
{
   uint16_t OCA:1;      /* overcurrent phase A */
   uint16_t OCB:1;      /* overcurrent phase B */
   uint16_t OLA:1;      /* open load bridge A */
   uint16_t OLB:1;      /* open load bridge B */
   uint16_t OCHS:1;     /* overcurrent highside */
   uint16_t UV:1;       /* undervoltage */
   uint16_t OTPW:1;     /* overtemperature prewarning */
   uint16_t OT:1;       /* overtemperature */
   uint16_t ONE:1;      /* always "1" */
   uint16_t LOAD:3;     /* load indicator */
   uint16_t unused:4;	/* not used (probably 0) */
};

union ButterflyMotorStatusType {
  struct ButterflyMotorStatusFieldType StatusField;
  uint16_t ButterflyStatusWord;
};

/* the AVR on the butterfly tells us the cause of the last RESET */
/* might be interesting for debugging */

struct ButterflyCPUStatusFieldType
{
   uint16_t UNUSED:11;   /* not used, 0 */
   uint16_t JTRF:1;      /* JTAG reset (from debugger)*/
   uint16_t WDRF:1;      /* watchdog reset */
   uint16_t BORF:1;      /* brown out reset flag */
   uint16_t EXTRF:1;     /* external reset flag (normal RC-reset)*/
   uint16_t PORF:1;      /* Power on reset flag */
};

union ButterflyCPUStatusType {
  struct ButterflyCPUStatusFieldType StatusField;
  uint16_t ButterflyCPUWord;
};

/* The butterfly has positions from 0 to 2499, only 0-624 make sense (90�)
The valve is closed when position is valid and zero,
and fully opened when position is valid and 624.

The AVR controller on the butterfly sends back the target position
it has understood, this number is reflected in the TargetPositionGot field

When the elekIOServ receives a Set Position command it writes the required
position into the TargetPositionSet field, from where it is read by
the serial communication thread in butterfly.c and sent via RS232 to the
AVR

Normally both fields should be identical after about 500ms, otherwise this
might indicate a communication problem */

struct ButterflyType {
  struct timeval                    TimeOfUpdate;                                   /* time of update of the data */
  uint16_t                          PositionValid;        /* tells us if the encoder has seen the index hole once */
  uint16_t                          CurrentPosition;      /* tells us where the butterfly stands */
  uint16_t                          TargetPositionGot;    /* the target position the AVR should go to (readback from AVR)*/
  uint16_t                          TargetPositionSet;    /* the target position desired */
  union ButterflyMotorStatusType    MotorStatus;          /* motor status bitfield */
  union ButterflyCPUStatusType      CPUStatus;            /* CPU status */
}; /* ButterflyType */

/*************************************************************************************************************/
/* AUX & METEO stuff */
/*************************************************************************************************************/

struct AuxStatusFieldType
{
   uint16_t Reserved:9;              /* not used up to now */
   uint16_t LicorPressureDataValid:1;/* Pressure from Calibrator is valid */
   uint16_t BahmasBlock1Valid:1;     /* Noseboom Data, Humidity etc.*/
   uint16_t BahmasBlock2Valid:1;     /* Ships Gyro data are valid*/
   uint16_t BahmasBlock3Valid:1;	 /* Ships Sonar data are valid*/
   uint16_t BahmasBlock4Valid:1;     /* Ships Meteo data are valid*/
   uint16_t BahmasBlock5Valid:1;     /* Ships GPS data are valid*/
   uint16_t BahmasIWG1Valid:1;		 /* Meteobox data are valid */
};

union AuxStatusType
{
   struct AuxStatusFieldType Field;
   uint16_t Word;
};

struct AuxDataValidType
{
   union AuxStatusType      Status;
};

struct MeteoBoxType
{
   double   dWindSpeed;               /* Windspeed in m/s */
   uint16_t uiWindDirection;          /* 45� resolution */
   double   dRelHum;                  /* 000.0 - 100.0 % */
   double   dAirTemp;                 /* Temperature in degree celsius */
   double   dGasSensorVoltage;        /* dirt sensor */
};

struct LicorPressureType
{
   uint16_t uiAmbientPressure;        /* Unit: kPA * 100 e.g. 1002.7 mBar => 10027 */
};                                    /* field for elekIOServ to copy Licor pressure data */

struct ShipSonarType
{
   double   dFrequency;               /* Frequency used for sonar */
   double   dWaterDepth;
};

struct ShipMeteoType
{
   double   dWindSpeed;                /* wind speed */
   double   dWindDirection;            /* wind direction */
};

struct ShipGyroType
{
   double   dDirection;                /* direction */
};

struct ShipWaterType
{
   double   dSalinity;                /* gramms per litre */
   double   dWaterTemp;               /* water temp in degrees celsius */
};

struct SpectralLineType
{
   uint16_t uiWaveLength;             /* Wavelength in 0.01 nm e.g. 65000 -> 650.00nm */
   uint16_t uiCounts;                 /* Counts for each spectral line */
};
/*************************************************************************************************************/

enum InstrumentActionType { /* update also in instrument.c */
  INSTRUMENT_ACTION_NOP,
  INSTRUMENT_ACTION_MEASURE,
  INSTRUMENT_ACTION_CAL,
  INSTRUMENT_ACTION_DIAG,
  INSTRUMENT_ACTION_POWERUP,
  INSTRUMENT_ACTION_POWERDOWN,
  INSTRUMENT_ACTION_CAL_MEASURE,
  INSTRUMENT_ACTION_TEST,

  INSTRUMENT_ACTION_MAX
};



struct InstrumentFlagsType {                      /* set of flags for the instrument Server status */
  struct timeval TimeOfUpdate;                    /* time of update of the data */
  unsigned char  StatusSave:1;                    /* indicates if Status should be saved to disk */
  unsigned char  StatusQuery:1;                   /* indicates if Status should be Queried from elekIOServ */
  enum EtalonActionType EtalonAction;             /* indicates what the etalon is doing */
  enum InstrumentActionType InstrumentAction;     /* indicates what the instrument is doing (measuring, calibrating, etc.) */
  /*  enum DebugType        Debug;                     */
}; /* ServerFlagsType */

/* structure for TSC for time difference measurements Master <-> Slave */

struct TSCType
{
  uint16_t  TSCWord0;
  uint16_t  TSCWord1;
  uint16_t  TSCWord2;
  uint16_t  TSCWord3;
}; /* TSCStruct */

union TSCUn
{
    struct TSCType TSCSplit;
	 uint64_t TSCValue;
};

struct TSCDataType
{

    struct timeval  TimeOfUpdate;                                   /* time of update of the data */
    union TSCUn     TSCReceived;
	union TSCUn     TSCProcessed;
	};


/*************************************************************************************************************/

struct GPSDataType {					/* data type for GPS data*/
    struct timeval    TimeOfUpdate;                                   /* time of update of the data */
	unsigned char     ucUTCHours;			/* binary, not BCD coded (!) 0 - 23 decimal*/
	unsigned char     ucUTCMins;			/* binary, 0-59 decimal */
	unsigned char     ucUTCSeconds;			/* binary 0-59 decimal */

	double            dLongitude;					/* "Laengengrad" I always mix it up...
										signed notation,
										negative values mean "W - west of Greenwich"
										positive values mean "E - east of Greenwich" */

	double dLatitude;					/* "Breitengrad" I always mix it up...
										signed notation,
										negative values mean "S - south of the equator"
										positive values mean "N - north of the equator */
	float fAltitude;					/* altitude above the geoid in metres */
	float fHDOP;						/* Horizontal Dillution Of Precision, whatever it means....*/
	unsigned char ucNumberOfSatellites; /* number of satellites seen by the GPS receiver */
	unsigned char ucLastValidData;		/* number of data aquisitions (5Hz) with no valid GPS data
										will stick at 255 if no data received for a long period */
	uint16_t uiGroundSpeed;				/* speed in cm/s above ground */
	uint16_t uiHeading;					/* 10 times heading in degrees e.g. 2700 decimal = 270,0 Degress = west */

};

struct ShipGPSDataType {				/* data type for GPS data*/
	unsigned char ucUTCHours;			/* binary, not BCD coded (!) 0 - 23 decimal*/
	unsigned char ucUTCMins;			/* binary, 0-59 decimal */
	unsigned char ucUTCSeconds;			/* binary 0-59 decimal */
        unsigned char ucUTCDay;                         /* day 1-31 */
        unsigned char ucUTCMonth;                       /* month 1-12 */
        uint16_t      uiUTCYear;                         /* year 4 digits */
	double dLongitude;				/* "Laengengrad" I always mix it up...
										signed notation,
										negative values mean "W - west of Greenwich"
										positive values mean "E - east of Greenwich" */

	double dLatitude;				/* "Breitengrad" I always mix it up...
										signed notation,
										negative values mean "S - south of the equator"
										positive values mean "N - north of the equator
										will stick at 255 if no data received for a long period */
	double dGroundSpeed;				/* speed in knots above ground */
	double dCourseOverGround;			/* heading in degrees */

};

/*************************************************************************************************************/
/* defines for Calibrator */
#define CALIB_VMFC_TOTAL        10000        /* virtual MFC address for total (Dry+Humid Flow) calibrator flow */
#define CALIB_VMFC_ABS          1000         /* MFC ADR OFFSET to specify flow in counts instead of sccm */
#define CALIB_SETFLOW_SUCCESS   1            /* success to set flow */
#define CALIB_SETFLOW_FAIL      0            /* fail to set flow */
/*************************************************************************************************************/

enum WhichController
{
    MIRROR_UV_LASER,
    MIRROR_BLUE_LASER,
    MAX_MIRROR_CONTROLLER
};

enum WhichMirror {
    MIRROR_GREEN_1,
    MIRROR_GREEN_2,
    MIRROR_UV_1,
    MIRROR_UV_2,

    MAX_MIRROR
};

enum MirrorAxis {
    XAXIS,
    YAXIS,

    MAX_MIRROR_AXIS
};

#define REALIGN_MINUTES 30 /* mirror.c starts realignment every REALIGN_MINUTES, or never for negative numbers */
#define MIN_UV_DIFF_CTS 8 /* eq. 0.1 mW UV Diode Power */
#define DELTA_XPOSITION_MIRROR_GR1 0	/* used by second green mirror */
#define DELTA_YPOSITION_MIRROR_GR1 0	/* used by second green mirror */
#define DELTA_XPOSITION_MIRROR_GR2 25	/* used by second green mirror */
#define DELTA_YPOSITION_MIRROR_GR2 50	/* used by second green mirror */
#define DELTA_XPOSITION_MIRROR_UV1 200  	/* used by UV Mirror */
#define DELTA_YPOSITION_MIRROR_UV1 200  	/* used by UV Mirror */
#define DELTA_XPOSITION_MIRROR_UV2 0  	/* used by UV Mirror */
#define DELTA_YPOSITION_MIRROR_UV2 0  	/* used by UV Mirror */

struct AxisType {
  int32_t Position;
}; /* union AxisType */

struct MirrorType {
  struct AxisType Axis[MAX_MIRROR_AXIS];
}; /* MirrorType */

struct MovingFlagFieldType
{
  uint16_t MovingFlagByte:8;	/* Bitnumber=MirrorNumber*MAX_MIRROR_AXIS+MirrorAxis */
  uint16_t Realigning:1;      /* moving due to realignment routine */
  uint16_t unused:7;	/* not used (probably 0) */
};


union MovingFlagType {
  struct MovingFlagFieldType Field;
  uint16_t Word;
};


struct MirrorDataType {

  struct timeval        TimeOfUpdate;                                   /* time of update of the data */
  struct MirrorType     Mirror[MAX_MIRROR];
  union MovingFlagType  MovingFlag;
  uint16_t              MinUVDiffCts;
  uint16_t              RealignMinutes;

}; /* MirrorDataType */

/*************************************************************************************************************/

/* Data structures for PROFIBUS card
introduced in HOPE2012 campaign */

union FullScaleType {
  struct LongWordType FullScaleWord;
  float fMaxFlow;
}; /* union FullScaleType */

/* we need to swap wordwise and interpret as float*/
union MotIntFloatType {
  struct LongWordType Word;
  float fTheFloat;
}; /* union MotIntFloatType */

struct profibusMFCType
{
    uint16_t                usFlow;                                 /* FLOW from 0-32000 (32000 = full scale of MFC), can go above 100% */
    uint16_t                usSetPointRead;                         /* SETPOINT MFC was told to use (0-32000) */
    union FullScaleType     unFullScale;                            /* MFC Maxflow is coded in 32bit using IEEE754 floats */
    uint8_t                 ucSerialNumber[20];                     /* ASCII serial Number of the MFC, to make it unique for cal etc., NOT ZERO-TERMINATED */
    uint8_t                 ucUnit[7];                              /* ASCII unit string, usually like "mls/min", NOT ZERO-TERMINATED */
	uint8_t					ucPadding;								/* make structure multiple of 2, this results in 36 byte size */
};

/* Reference: */
/* JUMO B 70.3041.2.3 */
/* Schnittstellenbeschreibung PROFIBUS DP */
/* January 2008 */

/* ModBusAccessMode, see below */
#define MODBUS_READ_INT		(0x11)
#define MODBUS_READ_FLOAT	(0x13)
#define MODBUS_WRITE_INT	(0x21)
#define MODBUS_WRITE_FLOAT	(0x23)

union ModBusParameterType
{
	uint32_t	IntegerData;
	float		FloatData;
};
struct sModBusTunnel
{
	uint8_t		JobOK:1;
	uint8_t		JobBroken:1;
	uint8_t		JobToggle2:1;
	uint8_t		JobToggle1:1;
	uint8_t		reserved:4;
	uint8_t		ModBusAccessMode;
	uint16_t	ModBusAddress;
	union ModBusParameterType	Data;
};

union ModBusTunnelType
{
	struct sModBusTunnel	ModBusStruct;
	uint8_t					aAcyclicDataWrite[8];
};

/* reference: Jumo GSD File 12.05.2014 */

struct sReadJumoPID
{
	uint8_t 	ucInterfaceMode;
	union ModBusTunnelType	ReadModBus;
	float 		fActualReading;
	float 		fSetPointReading; /* FIXME: not MOT/INT swapped due to bug in ProfibusCard */
	float 		fControlOutput1;  /* FIXME: not MOT/INT swapped due to bug in ProfibusCard */
	float 		fControlOutput2;
	float 		fAnalogueInput1;
	float 		fAnalogueInput2;
	uint16_t	sDigitalMarker;
	float		fAnalogueMarker;
	float		fSetPoint1;
	uint16_t	sRelayOutputs0021;
	uint16_t	sBinaryInputs0023;
	uint16_t	sControlContacts0025;
	uint16_t	sBinarySignals0026;
};

struct sWriteJumoPID
{
	union ModBusTunnelType	WriteModBus;
};

struct sJumoPIDType
{
    struct profibusMFCType  sDummyMFCs[6];      /* get proper offset, we have 6 MFCs in front */
    struct sReadJumoPID     sJumoReadStruct;    /* data transferred from PID to HORUS-AIR */
    struct sWriteJumoPID    sJumoWriteStruct;   /* data transferred from HORUS-AIR to PID */
    uint8_t                 ucPadding[85];       /* TODO:fix array size */
};

union MFCEntryType
{
	struct profibusMFCType	sMFCData[MAX_MFC_PROFIBUS];							/* we prepare for 30 MFCs, to use the UDP payload max*/
	uint8_t					ucRawPayload[MAX_MFC_PROFIBUS*36];					/* 36 is sizeof(profibusMFCType) */
    struct sJumoPIDType     sJUMO;                                              /* overlay also the PID data */
};

struct profibusStatusType
{
    struct timeval          TimeOfDayProfibus;                     	/* PROFIBUS doesn't have RTC, so this is time since power-on */
    uint16_t                usComStatus;                            /* COM Status */
    union MFCEntryType      Payload;                        		/* Payload is ~1K, this should last for some MFCs.... */

};

struct profibusMFCStatusType
{
  struct timeval             TimeOfUpdate;                                   /* time of update of the data */
  struct profibusStatusType  MFCData;
  uint8_t              		 uiDataValid;
};

/*************************************************************************************************************/
enum BlueLaserPwrStatusType{

	OFF,
	Standby,
	ON

};

enum BlueLaserModulationModeType{

	UNDEFINED,
	MO1,		/* set modulated bias-current source internaly to high */
	MO2,		/* set modulated power-current source (mod-current) internaly to high */
	MO3,		/* set modulated bias-current and modulated power-current source simultaneous internaly to high */
	MF			/* set modulated bias-current and modulated power-current source simultaneous internaly to low */

};

struct blueLaserStatusType{

  /* data structure for the blue laser status data*/

  struct timeval        TimeOfUpdate;                                   /* time of update of the data */
  uint16_t 				usPeakPower;							 /* peak power in % (0x000-0xFFF) */
  uint16_t 				usAvgPower;								 /* average power from 0000 to 4095 [dec], CCC [hex] is always 100% */
  uint16_t 				usActualBias;
  uint16_t 				usModulatedBiasLevel;					 /* modulated bias-level[hex] */
  uint16_t 				usModLevel;								 /* mod-level internal set for drive max. current[hex]*/
  uint16_t 				usDiodeTemperature;						 /* laser diode temperature */
  uint16_t 				usCtrlVoltage;						 	 /* control voltage */
  uint16_t 				usTriggerVoltage;						 /* Threashold for digital trigger */
  enum BlueLaserModulationModeType	DigitalModulationMode;		 /* digital modulation mode (e.g. deepstar mode)*/
  enum BlueLaserPwrStatusType		PwrStatus;  				 /* Power status: 0=OFF, 1=Standby, 2=ON */

};

/* Flags for DyeLaser */
#define BM_DYE_LASER_ADJ_PRESS (0x0001)



struct DyeLaserStatusType
{

	/* data structure for the blue laser status data*/

	struct timeval        TimeOfUpdate;                                   /* time of update of the data */
	float                fSetPressure;							 /* set Pressure of Dye Cavity */
	float                fIsPressure;                      /* Pressure of Dye Cavity */
	float                fSetTempDyeLaser;                 /* set Point DyeLaserTemperature */
	float                fSIsTempDyeLaser;                 /*  DyeLaserTemperature */
	float                fSetTempLaserPlate;                 /* set Point DyeLaserTemperature */
	float                fSIsTempLaserPlate;                 /*  DyeLaserTemperature */

	uint16_t	            usFlags;					            /* see above */
};


enum GreenLaserPwrStatusType{

	GREEN_OFF,
	GREEN_STANDBY,
	GREEN_ON

};

/* Flags for Green Laser */
#define BM_SHUTTER_OPEN		(0x0001)
#define BM_USER_INTERLOCK	(0x0002)
#define BM_DIODES_ON		(0x0004)

#define	BM_COMM_ERROR		(0x2000)
#define BM_ERROR			(0x4000)
#define BM_IS_CONNECTED		(0x8000)

struct greenLaserStatusType
{

	/* data structure for the green laser status data*/

    struct timeval  TimeOfUpdate;                                   /* time of update of the data */
    uint16_t	    usFlags;					/* see above */
	uint16_t	    usErrorCode;				/* encoding of error messages (T.B.D.) */
	float 		    fDiodeCurrentReading;		/* Diode Current */
	float		    fDiodeCurrentSetpoint;		/* Diode Setpoints */
	float		    fHeatsinkTemperature[4];	/* Diode Heatsink Assembly Temps */
	float 		    fIRPower;					/* infrared power */
	float		    fQSwitchFrequency;			/* mod-level internal set for drive max. current[hex]*/
	float 		    fTHGTemp;					/* THG Crystal Temp 20-99�C */
	float 		    fSHGTemp;					/* SHG Crystal Temp 110-190�C */
	float		    fSpare1;					/* for expansion */
	float		    fSpare2;					/* for expansion */
};

/* header in front of each data packet */
struct BahamasHeaderType
{
    uint8_t ucStartHeader[4];
    uint8_t ucNotUsed[4];
    uint8_t ucPacketNo[4];
    uint8_t ucEndHeader[4];
};

/* taken from Bahamas Data Description.xml, 7.1.2015 */
/* Block#1 on Port 20511 */
/* 22 Parameters (including Timestamp) */
struct BahamasBlock1Type
{
    double  TIMESTAMP;              /* Time and Date, unit="sec" */
	double  HP;                     /* Pressure Altitude from Boom, unit="m" */
    double  IAS;                    /* Indicated Air Speed from Boom, unit="m/s" */
	double  TAS;                    /* True Air Speed from Boom, unit="m/s" */
    double  MC;                     /* Mach Number from Boom, unit="M" */
	double  ALPHA;                  /* Angle of Attack from Boom, unit="rad" */
	double  BETA;                   /* Angle of Sideslip from Boom, unit="rad" */
    double  P1;                     /* Vapour Pressure of Water, unit="Pa"*/
    double  P21;                    /* Saturation Vapour Pressure of Water vs Water, unit="Pa" */
    double  P31;                    /* Saturation Vapour Pressure of Water vs Ice, unit="Pa" */
    double  TV;                     /* Virtual Temperature, unit="K" */
    double  MIXRATIO_H;             /* Mixing Ratio from Humicap, unit="kg/kg" */
    double  ABSHUM_H;               /* Absolute Humidity from Humicap, unit="kg/m^3" */
    double  TD_H;                   /* Dewpoint Temperature from Humicap, unit="K" */
    double  TF_H;                   /* Frostpoint Temperature from Humicap, unit="K" */
    double  THETA;                  /* Potential Temperature of Humid Air, unit="K" */
    double  TSL;                    /* Static Temperature from TAT Left, unit="K" */
    double  TSR;                    /* Static Temperature from TAT Right, unit="K" */
    double  ISA_T;                  /* International Standard Atmosphere - Temperature, unit="K" */
    double  DT_ISA;                 /* Delta ISA_T minus Static Temperature, unit="K" type="double" */
    double  RHOS;                   /* Density from Static Parameters, unit="kg/m^3" */
    double  VVI_B;                  /* Vertical Velocity Indicated from Boom", unit="m/s" */
};

/* Block#2 on Port 20512 */
/* 33 Parameters (including Timestamp) */
struct BahamasBlock2Type
{
    double  TIMESTAMP;              /* Time and Date, unit="sec" */
    double  B_PSA;                  /* Noseboom Static Pressure Analog Sensor, Pressure Reading, unit="Pa" */
    double  B_PSAT;                 /* Noseboom Static Pressure Analog Sensor, Core Temperature, unit="K" */
    double  B_AI;                   /* Anti Icing, unit="V" */
    double  B_TEST2;                /* Noseboom Testkanal 2 (einst Backup Drucksensor), unit="V" */
    double  B_QCA;                  /* Noseboom Impact Pressure Analog Sensor, Pressure Reading, unit="Pa" */
    double  B_QCAT;                 /* Noseboom Impact Pressure Analog Sensor, Core Temperature, unit="K" */
    double  B_DPA;                  /* Noseboom Alpha Pressure Analog Sensor, Pressure Reading, unit="Pa" */
    double  B_DPAT;                 /* Noseboom Alpha Pressure Analog Sensor, Core Temperature, unit="K" */
    double  B_DPB;                  /* Noseboom Beta Pressure Analog Sensor, Pressure Reading, unit="Pa" */
    double  B_DPBT;                 /* Noseboom Beta Pressure, Analog Sensor, Core Temperature, unit="K" */
    double  B_AX;                   /* Noseboom Accelerometer (x-direction), Acceleration, unit="m/s^2" */
    double  B_AY;                   /* Noseboom Accelerometer (y-direction), Acceleration, unit="m/s^2" */
    double  B_AZ;                   /* Noseboom Accelerometer (z-direction), Acceleration, unit="m/s^2" */
    double  RDO_H;                  /* Aircraft Radio Altimeter Analog Output, Altitude, unit="m" */
    double  C_P;                    /* Cabin Pressure Analog Sensor, Pressure Reading, unit="Pa" */
    double  C_PT;                   /* Cabin Pressure Analog Sensor, Core Temperature, unit="K" */
    double  N_T1;                   /* Nose Compartment, Temperature 1, unit="K" */
    double  N_T2;                   /* Nose Compartment, Temperature 2, unit="K" */
    double  N_T3;                   /* Nose Compartment, Temperature 3, unit="K" */
    double  B_T1;                   /* Noseboom Sensor Tray, Temperature 1 near acceleration sensors, unit="K" */
    double  B_T2;                   /* Noseboom Sensor Tray, Temperature 2 near mid of tray, unit="K" */
    double  N_T4;                   /* Nose Box Temperature (once B_T3), unit="K" */
    double  N_TAT1;                 /* Nose TAT Probe, TAT #1 - links, unit="K" */
    double  N_TAT2;                 /* Nose TAT Probe, TAT #2 - rechts, unit="K" */
    double  N_HUMT;                 /* Nose Humidity Channel, Temperature", unit="K" */
    double  N_TC1T;                 /* Sensor Box Temp-Conditioner Module #1, Temperature, unit="K" */
    double  N_TC2T;                 /* Sensor Box Temp-Conditioner Module #2, Temperature, unit="K" */
    double  N_P;                    /* Nose Compartment, Pressure, unit="Pa" */
    double  N_HUMP;                 /* Nose Humidity Channel Analog Pressure Sensor, Pressure Reading, unit="Pa" */
    double  N_HUMPT;                /* Nose Humidity Channel Analog Pressure Sensor, Core Temperature, unit="K" */
    double  N_RHC;                  /* Nose Capacitive Humidity Sensor, Relative Humidity, unit="%" */
    double  T_TCPS;                 /* Trailing Cone Pressure Digital Sensor, Pressure Reading, unit="Pa" */
};

/* Block#3 on Port 20513 */
/* 26 Parameters (including Timestamp) */
struct BahamasBlock3Type
{
    double  TIMESTAMP;  /* Time and Date, unit="sec" */
    double  IGI_WSEC;   /* IGI Weekseconds - Nan = -1", unit="sec" */
    double  IGI_WEEK;   /* IGI Weeknumber, unit="-" */
    double  IGI_LAT;    /* IGI latitude position above WGS84, unit="rad" */
    double  IGI_LON;    /* IGI longitude position above WGS84, unit="rad" */
    double  IGI_ALT;    /* IGI height above WGS84 ellipsoid, unit="m" */
    double  IGI_NSV;    /* IGI north south velocity", unit="m/s" */
    double  IGI_EWV;    /* IGI east west velocity", unit="m/s" */
    double  IGI_VV;     /* IGI vertical velocity", unit="m/s" */
    double  IGI_ROLL;   /* IGI roll angle, unit="rad"; */
    double  IGI_PITCH;  /* IGI pitch angle, unit="rad"; */
    double  IGI_YAW;    /* IGI true heading, unit="rad+"; */
    double  IGI_RMSX;   /* IGI RMS value for position x, unit="m" */
    double  IGI_RMSY;   /* IGI RMS value for position y, unit="m" */
    double  IGI_RMSZ;   /* IGI RMS value for position z, unit="m" */
    double  IGI_P;      /* IGI body roll rate, unit="rad/s" */
    double  IGI_Q;      /* IGI body pitch rate, unit="rad/s" */
    double  IGI_R;      /* IGI body yaw rate, unit="rad/s" */
    double  IGI_AXB;    /* IGI body  x-axis acceleration, unit="m/s^2" */
    double  IGI_AYB;    /* IGI body  y-axis acceleration, unit="m/s^2" */
    double  IGI_AZB;    /* IGI body  z-axis acceleration, unit="m/s^2" */
    double  IGI_AZG;    /* IGI vertical acceleration, unit="m/s^2" */
    double  IGI_ATA;    /* IGI actual track angle, unit="rad+" */
    double  IGI_GS;     /* IGI ground speed, unit="m/s" */
    double  IGI_WA;     /* IGI wind angle, unit="rad+" */
    double  IGI_WS;     /* IGI wind speed, unit="m/s" */
};

/* Block#4 on Port 20514 */
/* 23 Parameters (including Timestamp) */
struct BahamasBlock4Type
{
    double  TIMESTAMP;  /* Time and Date, unit="sec" */
    double  C_R115ACA;  /* RHS: 115VAC 3 Phase Experimental Power Monitor, Phase A Voltage, unit="V" */
    double  C_R115ACB;  /* RHS: 115VAC 3 Phase Experimental Power Monitor, Phase B Voltage, unit="V" */
    double  C_R115ACC;  /* RHS: 115VAC 3 Phase Experimental Power Monitor, Phase C Voltage, unit="V" */
    double  C_R115AIA;  /* RHS: 115VAC 3 Phase Anti Ice Power Monitor, Phase A Voltage, unit="V" */
    double  C_R115AIB;  /* RHS: 115VAC 3 Phase Anti Ice Power Monitor, Phase B Voltage, unit="V" */
    double  C_R115AIC;  /* RHS: 115VAC 3 Phase Anti Ice Power Monitor, Phase C Voltage, unit="V" */
    double  C_R28DC;    /* RHS: 28VDC Experimental Power Monitor, Voltage, unit="V" */
    double  C_R230AC;   /* RHS: 230VAC Experimental Power Monitor, Voltage, unit="V" */
    double  C_L115ACA;  /* LHS: 115VAC 3 Phase Experimental Power Monitor, Phase A Voltage, unit="V" */
    double  C_L115ACB;  /* LHS: 115VAC 3 Phase Experimental Power Monitor, Phase B Voltage, unit="V" */
    double  C_L115ACC;  /* LHS: 115VAC 3 Phase Experimental Power Monitor, Phase C Voltage, unit="V" */
    double  C_L115AIA;  /* LHS: 115VAC 3 Phase Anti Ice Power Monitor, Phase A Voltage, unit="V" */
    double  C_L115AIB;  /* LHS: 115VAC 3 Phase Anti Ice Power Monitor, Phase B Voltage, unit="V" */
    double  C_L115AIC;  /* LHS: 115VAC 3 Phase Anti Ice Power Monitor, Phase C Voltage, unit="V" */
    double  C_L28DC;    /* LHS: 28VDC Experimental Power Monitor, Voltage, unit="V" */
    double  C_L230AC;   /* LHS: 230VAC Experimental Power Monitor, Voltage, unit="V" */
    double  N_5DC;      /* Nose Sensor Box Power Supply, 5V DC-Voltage, unit="V" */
    double  N_P15DC;    /* Nose Sensor Box Power Supply, +15V DC-Voltage" */
    double  N_N15DC;    /* Nose Sensor Box Power Supply, -15V DC-Voltage" */
    double  C_A1Y;      /* Acceleration cross at seat rail sensor 1 */
    double  C_A2Y;      /* Acceleration cross custom, unit="m/s^2" */
    double  C_A3Y;      /* Acceleration cross custom - pressure rake, unit="m/s^2" */
};
/* Block#5 on Port 20515 */
/* 139 Parameters (including Timestamp) */
struct BahamasBlock5Type
{
	/* IRS data below */
	double  TIMESTAMP;  /* Time and Date", unit="sec" */
	double  IRS_TIN;    /* irs3TimeinNav", unit="sec" */
	double  IRS_HHDG;   /* irs3sLbl132b29_14HybridTrueHeading, unit="rad+" */
	double  IRS_HVF;    /* irs3Lbl135b28_11HybridVerticalFOM, unit="-" */
	double  IRS_HATA;   /* irs3sLbl137b29_14HybridTrackAngle, unit="rad+" */
	double  IRS_HGS;    /* irs3Labl175b28_14HybridGroundSpeed, unit="m/s" */
	double  IRS_HLAT;   /* irs3Labl254b29_09HybridLatitude, unit="rad" */
	double  IRS_HLON;   /* irs3Labl255b29_09HybridLongitude, unit="rad" */
	double  IRS_HLATF;  /* irs3Labl257b29_18HybridLatitudeFine, unit="rad+" */
	double  IRS_HLONF;  /* irs3Labl256b29_18HybridLongitudeFine, unit="rad+" */
	double  IRS_HALT;   /* irs3Labl261b29_09HybridAltitude, unit="m" */
	double  IRS_HFPA;   /* irs3HybridFlightPathAngle, unit="rad+" */
	double  IRS_HHF;    /* irs3Labl255b29_09HybridHorizonFOM, unit="-" */
	double  IRS_HNSV;   /* irs3Labl266b29_14HybridNSVelocity, unit="m/s" */
	double  IRS_HEWV;   /* irs3Labl267b29_14HybridEWVelocity, unit="m/s" */
	double  IRS_LAT;    /* irs3aPosLatitude, unit="rad" */
	double  IRS_LON;    /* irs3aPosLongtitudes, unit="rad" */
	double  IRS_GS;     /* irs3aGroundSpeed, unit="m/s" */
	double  IRS_ATA;    /* irs3aTrackAngleTrue, unit="rad+" */
	double  IRS_HDG;    /* irs3aTrueHeading, unit="rad+" */
	double  IRS_WS;     /* irs3aWindSpeed, unit="m/s" */
	double  IRS_WA;     /* irs3aWindDirectionTrue, unit="rad+" */
	double  IRS_ATAM;   /* irs3aTrackAngleMagnetic, unit="rad+" */
	double  IRS_HDGM;   /* irs3aMagneticHeading, unit="rad+" */
	double  IRS_DA;     /* irs3aDriftAngle, unit="rad" */
	double  IRS_FPA;    /* irs3aFlightPathAngle, unit="rad+" */
	double  IRS_FPAC;   /* irs3aFlightPathAccel, unit="m/s^2" */
	double  IRS_THE;    /* irs3aPitchAngle, unit="rad" */
	double  IRS_PHI;    /* irs3aRollAngle/irs3aRollAngleStatus", unit="rad" */
	double  IRS_Q;      /* irs3aBodyPitchRate, unit="rad/s" */
	double  IRS_P;      /* irs3aBodyRollRate, unit="rad/s" */
	double	IRS_R;		/* irs3aBodyYawRate, unit="rad/s" */
	double	IRS_AXB;	/* irs3aBodyLongAccels, unit = "m/s^2" */
	double	IRS_AYB;	/* irs3aBodyLatAccel, unit = "m/s^2" */
	double	IRS_AZB;	/* irs3aBodyNormAccel, unit = "m/s^2" */
	double	IRS_PHDG;	/* irs3aPlatformHeading, unit = "rad+" */
	double	IRS_ATAR;	/* irs3TrackingAngleRate, unit = "rad/s" */
	double	IRS_QATT;	/* IRS inertial pitch rate, unit = "rad/s" */
	double	IRS_PATT;	/* IRS inertial roll rate, unit = "rad/s" */
	double	IRS_HVV;	/* irs3HybridVerticalVelocity, unit = "m/s" */
	double	IRS_CNT;	/* irs3aCycleCounter, unit = "-" */
	double	IRS_ALT;	/* irs3aInertialAltitude, unit = "m" */
	double	IRS_AATA;	/* irs3aAlongTrackAccel, unit = "m/s^2" */
	double	IRS_ACTA;	/* irs3aCrossTrackAccel, unit = "m/s^2" */
	double	IRS_AZG;	/* irs3aVertikalAccel, unit = "m/s^2" */
	double	IRS_VV;		/* irs3aInertialVertikalSpd, unit = "m/s" */
	double	IRS_NSV;	/* irs3aNsVelocity, unit = "m/s" */
	double	IRS_EWV;	/* irs3aEwVelocity, unit = "m/s" */
	double	IRS_AZBU;	/* irs3aUnbiasedNormalAccel, unit = "m/s^2" */
	double	IRS_AAHDG;	/* irs3aAlongHeadingAccel, unit = "m/s^2" */
	double	IRS_ACHDG;	/* irs3aCrossHeadingAccel, unit = "m/s^2" */

	/* FMS Data below */
	double	FMS_DTR;	/* fms2DesiredTrack, unit = "rad+" */
	double	FMS_LDEV;	/* fms2LateralDev, unit = "m" */
	double  FMS_VDEV;	/* fms2VerticalDev, unit = "m" */
	double	FMS_MVAR;	/* fms2MagVar", unit = "rad+" */
	double	FMS_HP;		/* adsPressureAltitude, unit = "m" */
	double	FMS_HPB;	/* adsBaroAltitude, unit = "m" */
	double	FMS_MC;		/* adsMach, unit = "Ma" */
	double	FMS_CAS;	/* adsCalibratedAirspeed, unit = "m/s" */
	double	FMS_MAS;	/* adsMaxOperatingAirspeed, unit = "m/s" */
	double	FMS_TAS;	/* adsTrueAirspeed, unit = "m/s" */
	double	FMS_TAT;	/* adsTotalAirTemperature, unit = "K" */
	double	FMS_HPR;	/* adsAltitudeRate, unit = "m/s" */
	double	FMS_SAT;	/* adsStaticAirTemperature, unit = "K" */
	double	FMS_DTGO;	/* fms2DistanceToGo, unit = "m" */
	double	FMS_TTGO;	/* fms2TimeToGo, unit = "sec" */
	double	FMS_TTT;	/* fms2TimeToTOC, unit = "sec" */
	double	FMS_HDEST;	/* fms2DestFieldElev, unit = "m" */
	double	FMS_LAT;	/* fms2PposLatitude, unit = "rad" */
	double	FMS_LON;	/* fms2PposLongitude, unit = "rad" */
	double	FMS_GS;		/* fms2Groundspeed, unit = "m/s" */
	double	FMS_ATA;	/* fms2Trackangle, unit = "rad+" */
	double	FMS_HDG;	/* irsTrueHeading, unit = "rad+" */
	double	FMS_WS;		/* fms2WindSpeed, unit = "m/s" */
	double	FMS_WA;		/* fms2WindAngle, unit = "rad+" */
	double	FMS_DA;		/* fms2DriftAngle, unit = "rad" */
	double	FMS_DDEST;	/* fms2DistanceToDest, unit = "m" */
	double	FMS_TDEST;	/* fms2TimeToDest, unit = "sec" */

	/* Air Data System below */
	double	ADS_HP;		/* ads2PressureAltitude, unit = "m" */
	double	ADS_HPB;	/* ads2BaroAltitude, unit = "m" */
	double	ADS_MC;		/* ads2Mach, unit = "Ma" */
	double	ADS_CAS;	/* ads2CalibratedAirspeed, unit = "m/s" */
	double	ADS_MAS;	/* ads2MaxOperatingAirspeed, unit = "m/s" */
	double	ADS_TAS;	/* ads2TrueAirspeed/ads2AirData50msec", unit = "m/s" */
	double	ADS_TAT;	/* ads2TotalAirTemperature, unit = "K" */
	double	ADS_HPR;	/* ads2AltitudeRate, unit = "m/s" */
	double	ADS_SAT;	/* ads2StaticAirTemperature, unit = "K" */
	double	ADS_QC;		/* ads2ImpactPressureMb, unit = "Pa" */
	double	ADS_PS;		/* ads2StaticAirPressure, unit = "Pa" */
	double	ADS_HPB2;	/* ads2BaroAltitude2, unit = "m" */
	double	ADS_PT;		/* ads2TotalPressureMb, unit = "Pa" */
	double	ADS_AOA1;	/* fcs1AoaNorm, unit = "-" */
	double	ADS_AOA2;	/* fcs2AoaNorm, unit = "-" */
	double	ADS_HPB1;	/* ads2BaroAltitude1, unit = "m" */

	/* GPS data below */
	double	GPS_PR;		/* gps2PseudoRange, unit = "m" */
	double	GPS_PRS;	/* gps2PseudoRangeFine, unit = "m" */
	double	GPS_RR;		/* gps2RangeRate, unit = "m/s" */
	double	GPS_DR;		/* gps2DeltaRange, unit = "m" */
	double	GPS_X;		/* gps2SvPositionX, unit = "m" */
	double	GPS_XF;		/* gps2SvPositionXFine, unit = "m" */
	double	GPS_Y;		/* gps2SvPositionY, unit = "m" */
	double	GPS_YF;		/* gps2SvPositionYFine", unit = "m" */
	double	GPS_Z;		/* gps2SvPositionZ, unit = "m" */
	double	GPS_ZF;		/* gps2SvPositionZFine, unit = "m" */
	double	GPS_UTC;	/* gps2UtcMeasureTime, unit = "sec" */
	double	GPS_ALT;	/* gps2Altitude, unit = "m" */
	double	GPS_HDOP;	/* Horizontal Dilution Of Precision, unit = "-" */
	double	GPS_VDOP;	/* Vertical Dilution Of Precision, unit = "-" */
	double	GPS_ATA;	/* gps2TrackAngle, unit = "rad+" */
	double	GPS_LATM;	/* gps2Latitude, unit="rad" */
	double	GPS_LON;	/* gps2Longitude, unit = "rad" */
	double	GPS_GS;		/* gps2GroundSpeed, unit = "m/s" */
	double	GPS_LATF;	/* gps2LatitudeFine, unit = "rad+" */
	double	GPS_LONF;	/* gps2LongitudeFine, unit = "rad+" */
	double	GPS_HIL;	/* gps2AutHorizIntegLimit, unit = "m" */
	double	GPS_VIL;	/* gps2AutVertIntegLimit, unit = "m" */
	double	GPS_VF;		/* gps2VertForm, unit = "m" */
	double	GPS_UTCF1;	/* gps2UtcFine, unit = "sec" */
	double	GPS_UTCF2;	/* gps2UtcFineFractions, unit = "sec" */
	double	GPS_AAH;	/* gps2AppAreaHil, unit = "m" */
	double	GPS_AAV;	/* gps2AppAreaVil, unit = "m" */
	double	GPS_TIME;	/* gps2UtcTIME in Seconds, Minutes and Hours, unit = "msec" */
	double	GPS_VV;		/* gps2VertVel, unit = "m/s" */
	double	GPS_NSV;	/* gps2NsVelocity, unit = "m/s" */
	double	GPS_EWV;	/* gps2EwVelocity, unit = "m/s" */
	double	GPS_HF;		/* gps2HorizFom, unit = "m" */
	double	LSS_3;		/* LSS_3, unit = "V" */

	/* TCS Data below */
	double	TCS_CPS;	/* TCS_CPS, unit = "V" */
	double	TCS_AS;		/* TCS_AS, unit = "V" */
	double	TCS_MOD;	/* TCS_MOD, unit = "V" */
	double	TCS_IR;		/* TCS_IR, unit = "V" */
	double	TCS_IA;		/* TCS_IA, unit = "V" */
	double	TCS_IB;		/* TCS_IB, unit = "V" */
	double	TCS_ALT;	/* TCS_ALT, unit = "V" */
	double	TCS_ALT2;	/* TCS_ALT3, unit = "V" */
	double	TCS_RAV;	/* TCS_RAV, unit = "V" */
	double	TCS_SL;		/* TCS_SL, unit = "V" */
	double	TCS_MTN;	/* TCS_MTN, unit = "V" */
	double	TCS_ID;		/* TCS_ID, unit = "V" */
};

struct BahamasLightType
{
	/* from Block 1 */
	double  HP;                     /* Pressure Altitude from Boom, unit="m" */
	double  TAS;                    /* True Air Speed from Boom, unit="m/s" */
	double  ALPHA;                  /* Angle of Attack from Boom, unit="rad" */
	double  BETA;                   /* Angle of Sideslip from Boom, unit="rad" */
	double  TV;                     /* Virtual Temperature, unit="K" */
	double  MIXRATIO_H;             /* Mixing Ratio from Humicap, unit="kg/kg" */
	double  ABSHUM_H;               /* Absolute Humidity from Humicap, unit="kg/m^3" */
	double  TSL;                    /* Static Temperature from TAT Left, unit="K" */
    double  RHOS;                   /* Density from Static Parameters, unit="kg/m^3" */
    double  VVI_B;                  /* Vertical Velocity Indicated from Boom", unit="m/s" */

	/* from Block 2 */
    double  B_PSA;                  /* Noseboom Static Pressure Analog Sensor, Pressure Reading, unit="Pa" */
    double  B_AX;                   /* Noseboom Accelerometer (x-direction), Acceleration, unit="m/s^2" */
    double  B_AY;                   /* Noseboom Accelerometer (y-direction), Acceleration, unit="m/s^2" */
    double  B_AZ;                   /* Noseboom Accelerometer (z-direction), Acceleration, unit="m/s^2" */
    double  C_P;                    /* Cabin Pressure Analog Sensor, Pressure Reading, unit="Pa" */
    double  C_PT;                   /* Cabin Pressure Analog Sensor, Core Temperature, unit="K" */
    double  N_TAT1;                 /* Nose TAT Probe, TAT #1 - links, unit="K" */

	/* from Block 3 */
	double  IGI_LAT;    /* IGI latitude position above WGS84, unit="rad" */
	double  IGI_LON;    /* IGI longitude position above WGS84, unit="rad" */
    double  IGI_ALT;    /* IGI height above WGS84 ellipsoid, unit="m" */
    double  IGI_NSV;    /* IGI north south velocity", unit="m/s" */
    double  IGI_EWV;    /* IGI east west velocity", unit="m/s" */
    double  IGI_VV;     /* IGI vertical velocity", unit="m/s" */
    double  IGI_ROLL;   /* IGI roll angle, unit="rad"; */
    double  IGI_PITCH;  /* IGI pitch angle, unit="rad"; */
    double  IGI_YAW;    /* IGI true heading, unit="rad+"; */

	/* from Block 4 */
	double  C_R115ACA;  /* RHS: 115VAC 3 Phase Experimental Power Monitor, Phase A Voltage, unit="V" */
	double  C_R115ACB;  /* RHS: 115VAC 3 Phase Experimental Power Monitor, Phase B Voltage, unit="V" */
	double  C_R115ACC;  /* RHS: 115VAC 3 Phase Experimental Power Monitor, Phase C Voltage, unit="V" */
	double  C_R28DC;    /* RHS: 28VDC Experimental Power Monitor, Voltage, unit="V" */
    double  C_R230AC;   /* RHS: 230VAC Experimental Power Monitor, Voltage, unit="V" */
    double  C_L115ACA;  /* LHS: 115VAC 3 Phase Experimental Power Monitor, Phase A Voltage, unit="V" */
    double  C_L115ACB;  /* LHS: 115VAC 3 Phase Experimental Power Monitor, Phase B Voltage, unit="V" */
    double  C_L115ACC;  /* LHS: 115VAC 3 Phase Experimental Power Monitor, Phase C Voltage, unit="V" */
    double  C_L28DC;    /* LHS: 28VDC Experimental Power Monitor, Voltage, unit="V" */
    double  C_L230AC;   /* LHS: 230VAC Experimental Power Monitor, Voltage, unit="V" */
};

struct LIFOHDataType {
	unsigned char switches; /*Bit 0:penray shutter open, 1:penray shutter close, 2:shroud shutter open, 3:shroud shutter close, 4:PMT HV+Preamp, 5:UV Lamp */
	unsigned char valves; /*Bit 0:UV Purge, 1:diff zero, 2:pitot reflow */
	double abs_pressure; /*in hPa */
	double diff_pressure; /*in Pa */
	double pmt_signal; /*in V */
	double pitot_temp; /*in Celsius */
	unsigned char ai_power; /*Bit 0..2: L1..L3 from Anti Ice Input, Bit3: Anti - Ice Mode(0 = serial / low power, 1 = parallel / hi power)*/
	double ohtube_temp[3]; /*Temperature OH Tube heaters*/
	double roxtube_temp[2]; /*Temperatur ROx Tube heaters*/
	double penray_temp; /*Temperature Penray Heater*/
	double pmt_temp; /*Temperature PMT heater*/
	double preamp_temp; /*Temperature preamp heater*/
	double ohtube_current[3]; /*current of OH Tube heaters in A*/
	double roxtube_current[2]; /*current of ROx Tube heaters in A*/
	double penray_current; /*current of penreay heater in A*/
	double pmt_current; /*current of PMT heater in A*/
	double preamp_current; /*current of preamp heater in A*/
	double ai_restrictor; /*Anti-Ice Current Restrictor*/
	double ai_small_nose; /*Anti-Ice Current Small Nose (at ROx Inlet)*/
	double ai_big_nose; /*Anti-Ice Current Big Nose (at OH Inlet)*/
	double ai_small_ring; /*Anti-Ice Current Small Ring (inner ring OH Inlet)*/
	double ai_big_ring; /*Anti-Ice Current Big Ring (outer ring OH Inlet)*/
};

/* unique key for shared mem access between cgi and elekIOGSB */
#define GSB_SHMKEY			(0x23072010)

enum GSBType {
	GSBTYPE_UNKNOWN, /* not defined == 0 */
    GSBTYPE_GSB_M,   /* Mainz version for NO, non-EX */
    GSBTYPE_GSB_F,   /* Juelich version, CO non-EX */
    GSBTYPE_GSB_X,   /* LOLA version, CO-EX, no MFCs */
    MAX_GSBTYPE
};

struct GSBStatusType     /* structure for the GasflaschenSicherheitsBehaelter */
{
  /* data structures for the GasflaschenSicherheitsBehaelter */
  /* a.k.a. Secondary Containment */

  struct timeval             TimeOfDayMaster;
  struct timeval             TimeOfDayGSB;

  /* for GUI it's nice to know which type of GSB is connected */
  enum GSBType				 eTypeOfGSB;

  /* LTC DACs via I2C on ARM9 module*/
  /* 24 bit ADC#0 raw data */
  /* raw flow numbers MFCs (NO variant) */
  int32_t					 iRawFlowMFC1;
  int32_t					 iRawFlowMFC2;
  int32_t					 iRawFlowMFC3;

  /* raw pressure readings (NO variant) */
  int32_t					 iRawPressureNO1;
  int32_t					 iRawPressureNO2;
  int32_t					 iRawPressureNO3;

  /* raw PT100 voltages (NO variant) */
  int32_t					 iRawPT100NO1;
  int32_t					 iRawPT100NO2;

  /* 24 bit ADC#1 raw data */
  /* raw pressure numbers (CO ex version) */
  int32_t 					 iRawPressureCO1;
  int32_t					 iRawPressureCO2;
  int32_t					 iRawPressureCO3;

  /* raw PT100 voltages (CO ex version) */
  int32_t					 iRawPT100CO1;

  /* raw internal temp sensors of the ADCs */
  int32_t					 iTempADC0;
  int32_t					 iTempADC1;

  /* spare channels */
  int32_t					 iSpare0;
  int32_t					 iSpare1;
  int32_t					 iSpare2;
  int32_t					 iSpare3;

  /* setpoints for flow controllers */
  /* 16bit DAC on I2C bus of ARM9 module */

  uint16_t					 uiSetPointMFC0;		/* 0-5V setpoint, 0 to MAXINT */
  uint16_t					 uiSetPointMFC1;
  uint16_t					 uiSetPointMFC2;
  uint16_t					 uiSetPointMFC3;

  /* valve control word, 5 lowermost bits used */
  /* controlled via ATmega644 on upper PCB */
  uint32_t					 uiAVRFirmwareRevision;	/* current AVR firmware */
  uint16_t					 uiValveControlWord;	/* bitwise used */
  uint16_t					 uiValveVoltageSwitch;	/* should be around 24V, e.g. 24000 */
  uint16_t					 uiValveVoltageHold;	/* T.B.D., ~16V */
  uint16_t					 uiValveVoltageIs;	    /* actual voltage readback from AVR */
  uint16_t					 uiValveCurrent;		/* sum of current through valves */

  /* LED interior light */
  uint16_t					 uiLEDCurrentSet;		/* setpoint for LED current */
  uint16_t					 uiLEDCurrentIs;		/* reading of LED current */
  uint16_t					 uiLEDVoltage;			/* reading of LED voltage */

/* ============================================================================ */
/* structure below here is extended with doubles to transfer the calculated     */
/* values (bar, celsius and so on) to the web gui                               */
/* may be omitted for transfer to elekStatus, as evaluation might be also done  */
/* in Matlab, but this will provide at least an overview of what's going on in  */
/* the containment                                                              */
/* ============================================================================ */

  double					 dFlowMFC1;
  double					 dFlowMFC2;
  double					 dFlowMFC3;

  /* pressure readings (NO variant) */
  double					 dPressureNO1;
  double					 dPressureNO2;
  double					 dPressureNO3;

  /* PT100 temp (NO variant) */
  double					 dPT100NO1;
  double					 dPT100NO2;

  /* pressure readings (CO ex version) */
  double 					 dPressureCO1;
  double					 dPressureCO2;
  double					 dPressureCO3;

  /* PT100 temp (CO ex version) */
  double					 dPT100CO1;

  /* internal temp of the ADCs */
  double					 dTempADC0;
  double					 dTempADC1;

  /* LED current & voltage */
  double					 dLedCurrentIs;
  double					 dLedCurrentSet;
  double					 dLedVoltage;

  /* Valve current & voltage */
  double					 dValveVoltageSwitch;	/* should be around 24V */
  double					 dValveVoltageHold;	    /* T.B.D., ~10V */
  double					 dValveVoltageIs;	    /* actual voltage readback from AVR */
  double					 dValveCurrent;			/* sum of current through valves */

};

struct receiveGSBStatusType
{

    struct timeval          TimeOfUpdate;                                   /* time of update of the data */
    struct GSBStatusType	GSBData;				/* packet structure from GSB */
	uint8_t					ucDataValid;			/* data valid flag (for timeout handling) */
};

struct receiveLIFOHDataType
{
    struct timeval          TimeOfUpdate;                                   /* time of update of the data */
	struct LIFOHDataType	Data;					/* packet structure from LIF OH Inlet */
	uint8_t					ucDataValid;			/* data valid flag (for timeout handling) */
};

struct receiveBahamasLightType
{
    struct timeval          TimeOfUpdate;                                   /* time of update of the data */
    struct BahamasLightType	Data;					/* packet structure from BAHAMAS, reduces version from elekIOAux*/
	uint8_t					ucDataValid;			/* data valid flag (for timeout handling) */
};

struct elekStatusType {                                             /* combined status information of instrument */

  /* data structures for the Master Box (HORUS / Core I7) */

  struct timeval                 TimeOfDayMaster;                                   /* time of data */
  struct CounterCardType         CounterCardMaster;                                 /* Counter Card for Reference */
  struct EtalonDataType          EtalonData;                                        /* All Etalonstepper data */
  struct FilamentCardType        FilamentCard;                                      /* filament card */
  struct ADCCardType             ADCCardMaster[MAX_ADC_CARD_HORUS];                 /* ADC Card */
  struct MFCCardType             MFCCardMaster[MAX_MFC_CARD_HORUS];                 /* MFC Card */
  struct ValveCardType           ValveCardMaster[MAX_VALVE_CARD_HORUS];             /* Valve Card */
  struct DCDC4CardType           DCDC4CardMaster[MAX_DCDC4_CARD_HORUS];             /* Valve Card */
  struct TempSensorCardType      TempSensCardMaster[MAX_TEMP_SENSOR_CARD_HORUS];    /* Temperature Sensor Card */
  struct BackplaneADCType        BackplaneADC;
  struct NO2CounterCardType      CounterCardNO2;
  struct InstrumentFlagsType     InstrumentFlags;                                   /* Instrument flags */
  struct GPSDataType             GPSData;
  struct MirrorDataType          MirrorData[MAX_MIRROR_CONTROLLER];	                /* all Mirror Stepper data */
  struct TSCDataType             TimeStampCommand;
  struct ButterflyType           Butterfly;
  struct profibusMFCStatusType   profibusMFCData;
  struct receiveLIFOHDataType    LIFOHData;                                        /* data of OH Inlet */
  struct receiveBahamasLightType BahamasLightData;
  struct greenLaserStatusType    GreenLaser;                                       /* data for Green Laser */
  struct blueLaserStatusType     BlueLaser;                                        /* data for Blue Laser */
  struct DyeLaserStatusType      DyeLaserStatus;
  struct receiveGSBStatusType    GSB;                                              /* GSB NO: BE AWARE IT'S SHARED WHEN SWITCHING VALVES AND FLOWS */

}; /* elekStatusType */

enum IDStatusSectionType { /* keep this in sync with the instrument structure*/

	STATUS_SEND_COUNTERCARDMASTER,                                 /* Counter Card for Reference */
	STATUS_SEND_ETALONDATA,                                        /* All Etalonstepper data */
	STATUS_SEND_FILAMENTCARD,                                      /* filament card */
	STATUS_SEND_ADCCARDMASTER,               /* ADC Card */
	STATUS_SEND_MFCCARDMASTER,               /* MFC Card */
	STATUS_SEND_VALVECARDMASTER,             /* Valve Card */
	STATUS_SEND_DCDC4CARDMASTER,             /* Valve Card */
	STATUS_SEND_TEMPSENSCARDMASTER,          /* Temperature Sensor Card */
	STATUS_SEND_BACKPLANEADC,
	STATUS_SEND_COUNTERCARDNO2,
	STATUS_SEND_INSTRUMENTFLAGS,             /* Instrument flags */
	STATUS_SEND_GPSDATA,
	STATUS_SEND_MIRRORDATA,	                /* all Mirror Stepper data */
	STATUS_SEND_TIMESTAMPCOMMAND,
	STATUS_SEND_BUTTERFLY,
	STATUS_SEND_PROFIBUSMFCDATA,
	STATUS_SEND_LIFOHDATA,                                        /* data of OH Inlet */
	STATUS_SEND_BAHAMASLIGHTDATA,
	STATUS_SEND_GREENLASER,                                       /* data for Green Laser */
	STATUS_SEND_BLUELASER,                                        /* data for Blue Laser */
	STATUS_SEND_DYELASER,                                        /* data for Dye Laser */
	STATUS_SEND_GSB,

	STATUS_SEND_MAX
};

#define MAX_SEND_STRUCT_LEN 8192

struct StatusSendSectionType {

	enum IDStatusSectionType	   IDStatusSection;
	struct timeval                 TimeOfDayMaster;
	uint16_t                       SizePayload;
	uint8_t                        Payload[MAX_SEND_STRUCT_LEN];

};

struct sLicor6262Type /* the LICOR can transmit a maximum of 10 parameters out of the total 22 available ones */
{
	struct timeval             TimeOfDayLicor;
	uint8_t					   ucLicorDataValid;
	float fLicorTemperature; /* internal cell temperature unit: �C*/
	float fAmbientPressure;  /* pressure unit: kPA */

	float fH2Oraw;			 /* raw H2O value, unit: millivolt */
	float fH2OAbs;			 /* H2O absolute value, unit: mmol/mol */
	float fH2ODiff ;         /* H2O differntial value, unit: mmol/mol */

	float fH2OkPaAbs;        /* H2O absolute vapor pressure (?) unit: kPa */
	float fH2OkPaDiff;       /* H2O differntial vapor pressure (?) unit: kPa */

	float fH2OMassAbs;       /* H2O absolute mass, unit: mg/g */
	float fDewPoint;         /* H2O dew point, unit: �C */
	float fH2ORef;           /* H2O absolute value, unit mmol/mol */
};

struct sLargeCalibAux
{
	enum InstrumentActionType LicorAction;	/* may be used later for indicating LICOR zero cal etc.*/
	float fPressureSensors[6]; /* for future use*/
	uint16_t usValveWord;      /* maybe we have magnetic valves in future*/
	uint16_t usSpare;
	float fSpare;
};

struct calibStatusType     /* new structure introduced for the calibrator automation process */
{
  /* data structures for the calibrator box */
  struct timeval             TimeOfDayCalib;
  struct ADCCardType         ADCCardCalib[MAX_ADC_CARD_CALIB];
  struct MFCCardType         MFCCardCalib[MAX_ADC_CARD_CALIB];
  struct SCRCardType         SCRCardCalib[MAX_SCR3XB_CALIB];
  struct TempSensorCardType  TempSensCardCalib[MAX_TEMP_SENSOR_CARD_CALIB];
  struct LicorH2OCO2Type     LicorCalib;
  struct PIDregulatorType    PIDRegulator;
};

struct largeCalibStatusType     /* 300L calibrator data structure*/
{
    /* data structures for the calibrator box */
    struct timeval					TimeOfDayLargeCalib;
    struct profibusMFCStatusType    ProfibusCalib;
    struct sLicor6262Type			LicorCalib;
    struct sLargeCalibAux			AuxDataCalib;
};

struct auxStatusType     /* structure for BAHAMAS Data */
{
  struct timeval             TimeOfDayAux;
  struct AuxDataValidType    Status;
  struct BahamasBlock1Type   BahamasBlock1Data;
  struct BahamasBlock2Type   BahamasBlock2Data;
  struct BahamasBlock3Type   BahamasBlock3Data;
  struct BahamasBlock4Type   BahamasBlock4Data;
  struct BahamasBlock5Type   BahamasBlock5Data;
};

/* structure for --- IMPORTANT --- BAHAMAS Data send to status */

struct BahamasLightStatusType
{
  struct timeval             TimeOfDayAux;
  struct BahamasLightType	 ImportantData;
};

struct spectralStatusType     /* structure for the spectrometer data*/
{
  /* data structures for the spectrometer*/
  struct timeval             TimeOfDaySpectra;                      /* timestamp when the spectrum was aquired */
  struct timeval             TimeOfDayStatus;                       /* timestamp of the matching status dataset used for on-offline calculations */
  uint16_t                   uiMinWaveLength;                       /* minimal Wavelength, to speed up plotting */
  uint16_t                   uiMaxWaveLength;                       /* maximum Wavelength, to speed up plotting */
  uint16_t                   uiLineCount;                           /* valid lines in this spectrum (may vary between HR2000 and HR4000 */
  struct SpectralLineType    SpectralData[MAXSPECTRALLINES];        /* spectral data (wavelength / count pairs) */
  union PositionType         Set;                                   /* etalon set position */
  union PositionType         Current;                               /* etalon current position */
  union PositionType         Encoder;                               /* etalon encoder position */
  enum  EtalonActionType     CurrentEtalonAction;                   /* what the etalon was doing during spectra sampling */
};



enum PLANET_SEND_ID_TYPE {
	PLANET_SEND_ASCII_COUNTS_ID,
	PLANET_SEND_ASCII_MFC_ID,
	PLANET_SEND_BIN_ID,
    PLANET_SEND_ASCII_NO2_COUNTS_ID,

	PLANET_SEND_MAX_ID
};

/* structure for the timing periods to send data to the planet server and on ground */
struct PlanetSendPeriodeType
{
	time_t PlanetSendAsciiCounts;
	time_t PlanetSendAsciiMFC;
	time_t PlanetSendBinStatus;
    time_t PlanetSendAsciiNO2;
};

extern int elkInit(void);
extern int elkExit(void);
extern int elkWriteData(uint16_t Adress, uint16_t Data);
extern int elkReadData(uint16_t Adress);
#pragma pack(pop)
#endif

