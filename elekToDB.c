/*
*
* To-Do List:
*
* Refactoring:
*
*
*	insertFromFile:
*		loadFile
*		parse File
*
*	listen:
*		notifyFileChange
*		calculate last entry
*
*	int insertSingleEntry(struct elekStatusType data)
*
*	buildDatabase
*		buildDatabase
*
*	insertFragment
*		checkDatastructureOfFragment
*		insertDatastructure
*
*	main:
*		add verbose flag
*		(optional) command line parser
*
*	Logging
		- void prepareLogging(char* message, int verbose) => calls logMessage and printMessageToScreen depending on verbose
		- void logMessage(char* message) => writes message to disk and, if logfile gets to big, tars file and renames it, calls printMessage
		- void printMessage(char* message) => prints message to screen; may do some calculations to print the message nicer and mor "static"
*
*		
* Modes:
* 	- listen (finish refactoring before implementing)
*	- insertFromFile (see refactoring)
*	- insertSingleEntry
*	- insertFragment
*	- buildDatabase
*
* compiling
* debugging/testing
*
*
* Done: 
*	- Build sqlStrings before calling modes
*		- single strings: 						(2017-01-18)
*			- struct CounterCardType         	CounterCardMaster (2017-01-18)
*			- struct EtalonDataType          	EtalonData (2017-01-18)
*			- struct FilamentCardType        	FilamentCard (2017-01-18)
*			- struct BackplaneADCType        	BackplaneADC (2017-01-18)
*			- struct NO2CounterCardType      	CounterCardNO2 (2017-01-18)
*			- struct InstrumentFlagsType     	InstrumentFlags (2017-01-18)
*			- struct GPSDataType             	GPSData (2017-01-18)
*			- struct TSCDataType             	TimeStampCommand (2017-01-18)
*			- struct ButterflyType           	Butterfly (2017-01-18)
*			- struct profibusMFCStatusType   	profibusMFCData (2017-01-18)
*			- struct receiveLIFOHDataType    	LIFOHData (2017-01-18)
*			- struct receiveBahamasLightType 	BahamasLightData (2017-01-18)
*		 	- struct greenLaserStatusType    	GreenLaser (2017-01-18)
*			- struct blueLaserStatusType     	BlueLaser (2017-01-18)
*			- struct DyeLaserStatusType      	DyeLaserStatus (2017-01-18)
*			- struct receiveGSBStatusType 		GSB (2017-01-18)
*
*		- string-arrays:						(2017-01-19)
*			- struct ADCCardType             	ADCCardMaster[MAX_ADC_CARD_HORUS] (2017-01-19)
*			- struct MFCCardType             	MFCCardMaster[MAX_MFC_CARD_HORUS] (2017-01-19)
*			- struct ValveCardType           	ValveCardMaster[MAX_VALVE_CARD_HORUS] (2017-01-19)
*			- struct DCDC4CardType           	DCDC4CardMaster[MAX_DCDC4_CARD_HORUS] (2017-01-19)
*			- struct TempSensorCardType      	TempSensCardMaster[MAX_TEMP_SENSOR_CARD_HORUS] (2017-01-19)
*			- struct MirrorDataType          	MirrorData[MAX_MIRROR_CONTROLLER] (2017-01-19)
*
*	Design logging of messages					(2017-01-19)
*
*	- Build functions to insert data from a single structure into a single table (2017-01-19 - 2017-02-09)
*	  (signature: int insertStructIntoTable(const char* sqlString, struct STRUCT data))
*		- struct CounterCardType         	CounterCardMaster (2017-01-19)
*		- struct EtalonDataType          	EtalonData (2017-01-19)
*		- struct FilamentCardType        	FilamentCard (2017 -01-26)
*		- struct BackplaneADCType        	BackplaneADC (2017-01-26)
*		- struct NO2CounterCardType      	CounterCardNO2 (2017-02-01)
*		- struct InstrumentFlagsType     	InstrumentFlags (2017-02-01)
*		- struct GPSDataType             	GPSData (2017-02-08)
*		- struct TSCDataType             	TimeStampCommand (2017-02-08)
*		- struct ButterflyType           	Butterfly (2017-02-08)
*		- struct profibusMFCStatusType   	profibusMFCData (2017-02-08)
*		- struct receiveLIFOHDataType    	LIFOHData (2017-02-09)
*		- struct receiveBahamasLightType 	BahamasLightData (2017-02-09)
*		- struct greenLaserStatusType    	GreenLaser (2017-02-09)
*		- struct blueLaserStatusType     	BlueLaser (2017-02-09)
*		- struct DyeLaserStatusType      	DyeLaserStatus (2017-02-09)
*		- struct receiveGSBStatusType 		GSB (2017-02-09)
*
*	- Build functions to insert data from a single structure into a single table (2017-02-22)
*	  (signature: int insertStructIntoTable(const char* sqlString, struct STRUCT data))
*		- struct ADCCardType             	ADCCardMaster[MAX_ADC_CARD_HORUS] (2017-02-22)
*		- struct MFCCardType             	MFCCardMaster[MAX_MFC_CARD_HORUS] (2017-02-22)
*		- struct ValveCardType           	ValveCardMaster[MAX_VALVE_CARD_HORUS] (2017-02-22)
*		- struct DCDC4CardType           	DCDC4CardMaster[MAX_DCDC4_CARD_HORUS] (2017-02-22)
*		- struct TempSensorCardType      	TempSensCardMaster[MAX_TEMP_SENSOR_CARD_HORUS] (2017-02-22)
*		- struct MirrorDataType          	MirrorData[MAX_MIRROR_CONTROLLER] (2017-02-22)
*
*	- Compile current build
*	  2017-02-22 - compiled on 2017-02-23
*
*	- enum for single-structs (2017-03-15)
*	- enum for array-structs (2017-03-15)
*
*	Testing insertion of structures:
*		- struct CounterCardType         	CounterCardMaster (2017-03-16)
*		- struct EtalonDataType          	EtalonData (2017-03-16)
*		- struct FilamentCardType        	FilamentCard (2017 -03-16)
*		- struct BackplaneADCType        	BackplaneADC  (2017-04-19)
		- struct NO2CounterCardType      	CounterCardNO2  (2017-04-19)
*		- struct InstrumentFlagsType     	InstrumentFlags  (2017-04-19)
*		- struct GPSDataType             	GPSData  (2017-04-19)
*		- struct TSCDataType             	TimeStampCommand  (2017-04-19)
*		- struct ButterflyType           	Butterfly  (2017-04-19)
*		- struct receiveLIFOHDataType    	LIFOHData  (2017-04-19)
*		- struct receiveBahamasLightType 	BahamasLightData  (2017-04-19)
*		- struct greenLaserStatusType    	GreenLaser  (2017-04-19)
*		- struct blueLaserStatusType     	BlueLaser  (2017-04-19)
*		- struct DyeLaserStatusType      	DyeLaserStatus  (2017-04-19)
*		- struct receiveGSBStatusType 		GSB (2017-04-20)
*		- struct profibusMFCStatusType   	profibusMFCData (2017-04-26)
*
*	sqlCommandsForArrayStructs 	(2017-04-26)
*/

/*
* -------------------------------------------
* Includes
* -------------------------------------------
*/
#include "elekToDB.h"

/*
* -------------------------------------------
* Helper functions - Parsing datatypes to chars
* -------------------------------------------
*/
char* concatenateChars(char* s1, char* s2){

	size_t len1 = strlen(s1);
    size_t len2 = strlen(s2);

    char *result = malloc(len1+len2+1);//+1 for the zero-terminator
    if(result){

    	strcpy(result, s1);
    	strcat(result, s2);

	    return result;

    }else{

    	printf("Error allocating memory for strings: %s and %s\n", s1, s2);
    	exit(2);

    }

}

char* parseTimeval(struct timeval tv){
	
	time_t currentTime;
	struct tm *currentTM;

	char *result = malloc(TIME_BUFFER_SIZE);
	char *tmbuf = malloc(TIME_BUFFER_SIZE);

	//fixes the case for negative seconds, i.e. a time before 1970. Since the campaign will always be at a later date than 1970
	//it is safe to assume that every negative number will be a positive number. This happend with the GPSDataType and its TimeOfUpdate
	if(tv.tv_usec < 0){

		tv.tv_usec = tv.tv_usec * -1;

	}

	if(tv.tv_sec < 0){

		tv.tv_sec = tv.tv_sec * -1;

	}

	currentTime = tv.tv_sec; //getting the seconds
	currentTM = localtime(&currentTime);

	strftime(tmbuf, TIME_BUFFER_SIZE, "%Y-%m-%d %H:%M:%S", currentTM);
	snprintf(result, TIME_BUFFER_SIZE, "%s.%06ld", tmbuf, tv.tv_usec);

	free(tmbuf);

	return result;

}

int insertCharIntoCharArray(char* data, char** array, int position){

	char* buffer = malloc(strlen(data));
	strcpy(buffer, data);
	*(array + position) = buffer;

	if(*(array+position) != NULL){

		return 1;

	}else{

		return 0;

	}

}

int insertUnsignedCharIntoCharArray(unsigned char* data, char** array, int position){

	char* buffer = malloc(strlen(data));
	strcpy(buffer, data);
	*(array + position) = buffer;

	if(*(array+position) != NULL){

		return 1;

	}else{

		return 0;

	}

}

int insertIntIntoCharArray(int data, char** array, int position){

	char* buffer = (char*) malloc(INT_BUFFER_SIZE);
	snprintf(buffer, INT_BUFFER_SIZE, "%d", data);
	*(array + position) = buffer;

	if(*(array+position) != NULL){

		return 1;

	}else{

		return 0;

	}

}

int insertUintIntoCharArray(unsigned int data, char** array, int position){

	char* buffer = (char*) malloc(INT_BUFFER_SIZE);
	snprintf(buffer, INT_BUFFER_SIZE, "%d", data);
	*(array + position) = buffer;

	if(*(array+position) != NULL){

		return 1;

	}else{

		return 0;

	}

}

int insertuint8_tIntoCharArray(uint8_t data, char** array, int position){

	char* buffer;
	int i;
	buffer = malloc(9);
	if(!buffer){

		return 0;

	}

	buffer[8] = 0;

	for(i = 0; i <= 7; i++){

		buffer[7-i] = (((data) >> i) & (0x01)) + '0';

	}

	//puts(buffer);

	*(array + position) = buffer;

	if(*(array+position)[0] == '\0'){

		printf("String is empty!\n");

	}

	if(*(array+position) != NULL){

		return 1;

	}else{

		return 0;

	}

}

int insertint16_tIntoCharArray(int16_t data, char** array, int position){

	char* buffer = (char*) malloc(INT_BUFFER_SIZE);
	snprintf(buffer, INT_BUFFER_SIZE, "%d", data);
	*(array + position) = buffer;

	if(*(array+position) != NULL){

		return 1;

	}else{

		return 0;

	}

}

int insertuint16_tIntoCharArray(uint16_t data, char** array, int position){

	char* buffer = (char*) malloc(INT_BUFFER_SIZE);
	snprintf(buffer, INT_BUFFER_SIZE, "%d", data);
	*(array + position) = buffer;

	if(*(array+position) != NULL){

		return 1;

	}else{

		return 0;

	}

}

int insertint32_tIntoCharArray(int32_t data, char** array, int position){

	char* buffer = (char*) malloc(INT_BUFFER_SIZE);
	snprintf(buffer, INT_BUFFER_SIZE, "%d", data);
	*(array + position) = buffer;

	if(*(array+position) != NULL){

		return 1;

	}else{

		return 0;

	}

}

int insertuint32_tIntoCharArray(uint32_t data, char** array, int position){

	char* buffer = (char*) malloc(INT_BUFFER_SIZE);
	snprintf(buffer, INT_BUFFER_SIZE, "%u", data);
	*(array + position) = buffer;

	if(*(array+position) != NULL){

		return 1;

	}else{

		return 0;

	}

}

int insertuint64_tIntoCharArray(uint64_t data, char** array, int position){

	char* buffer = (char*) malloc(INT_BUFFER_SIZE);
	snprintf(buffer, INT_BUFFER_SIZE, "%llu", data);
	*(array + position) = buffer;

	if(*(array+position) != NULL){

		return 1;

	}else{

		return 0;

	}

}

int insertDoubleIntoCharArray(double data, char** array, int position){

	char* buffer = (char*) malloc(INT_BUFFER_SIZE);

	printf("Double: %f\n", data);

	if(isnan(data)){

		*(array+position) = NULL;

	}else{

		snprintf(buffer, INT_BUFFER_SIZE, "%f", data);
		*(array + position) = buffer;

		if(*(array+position) != NULL){

			return 1;

		}else{

			return 0;

		}

	}
	

	return 1;


}

int insertFloatIntoCharArray(float data, char** array, int position){

	char* buffer = (char*) malloc(INT_BUFFER_SIZE);
	snprintf(buffer, INT_BUFFER_SIZE, "%f", data);
	*(array + position) = buffer;

	if(*(array+position) != NULL){

		return 1;

	}else{

		return 0;

	}

}

/*
* -------------------------------------------
* helper functions - File Handler
* -------------------------------------------
*/

int openFileForReading(char* fileName, FILE** pFile){

	*pFile = fopen(fileName, "rb");

	if(!pFile){

		printf("failed! Reason: Filename is not correct. Exiting!\n");//log message
		return 0;

	}
	printf("done!\n");

	//Test to see if the the file size is correct, i.e. that a certain number of structs fit in the file without anything missing
	fseek(*pFile, 0L, SEEK_END);
	long sz = ftell(*pFile);
	rewind(*pFile);

	printf("Checking File %s...", fileName);
	if((sz%(sizeof(struct elekStatusType)))){

		printf("failed! Reason: Calculated number of structs (size: %u) in file (size: %ld) leaves rest (size: %ld) Exiting\n", sizeof(struct elekStatusType), sz, sz%(sizeof(struct elekStatusType)));//log message
		return 0;

	}

	printf("done!\n");
	return 1;

}

/*
* -------------------------------------------
* Error handling and log functions
* -------------------------------------------
*/
void handler(int sig) {
  void *array[25];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 25);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

void exit_nicely(PGconn *conn){

	PQfinish(conn);
	printf("Exiting with Error 2\n");
	exit(1);

}

void exit_nicely_with_res(PGconn *conn, PGresult *res){

	PQclear(res);
	PQfinish(conn);
	printf("Exiting with Error 1\n");
	exit(1);

}

void exit_nicely_with_res_and_error_code(PGconn *conn, PGresult *res, int errorCode, char *message){

	PQclear(res);
	PQfinish(conn);
	printf("Exiting with PSQL-Error %d\n%s",errorCode, message);
	exit(errorCode);

}

/*
* -------------------------------------------
* Database functions
* -------------------------------------------
*/
PGconn* createConnection(char* dbname, char* user, char* password){

	char* connectionString = (char *) malloc(1);
	strcpy(connectionString, " ");
	connectionString = concatenateChars(connectionString, "user=");
	connectionString = concatenateChars(connectionString, user);
	connectionString = concatenateChars(connectionString, " password=");
	connectionString = concatenateChars(connectionString, password);
	connectionString = concatenateChars(connectionString, " dbname=");
	connectionString = concatenateChars(connectionString, dbname);

	PGconn *conn;
	conn = PQconnectdb(connectionString);

	//printf("charlength: %lu, sizeofarray: %lu", strlen(connectionString), sizeof(connectionString)); //some string tests

	if(PQstatus(conn) != CONNECTION_OK){

		fprintf(stderr, "Connection to database failed: %s\n", PQerrorMessage(conn));
		exit_nicely(conn);

	}else{

		return conn;

	}

}

void executeSQLCommand(PGconn *conn, char* sqlString){

	PGresult *res = PQexec(conn, sqlString);

	if(PQresultStatus(res) != PGRES_COMMAND_OK){

		exit_nicely_with_res(conn, res);

	}

}

void executePreparedStatement(PGconn *conn, char* sqlString, int numberOfParams, const char** values, int lengths[], int binary[]){

	
		printf("SQLString:%s, NumberOfParameters: %d\n", sqlString, numberOfParams);

		int i = 0;
		for(i = 0; i < numberOfParams;i++){

			printf("Value at %d:%s, length at %d:%d, Binary at %d:%d\n", i, values[i], i, lengths[i], i, binary[i]);

		}
	

	PGresult *res = PQexecParams(conn, sqlString, numberOfParams, NULL, values, lengths, binary, 0);

	if(PQresultStatus(res) != PGRES_COMMAND_OK){

		exit_nicely_with_res_and_error_code(conn, res, PQresultStatus(res), PQresultErrorMessage(res));

	}

}

/*
* -------------------------------------------
* SQl-String creation functions
* -------------------------------------------
*/
int buildSQLCommandsAndNumberOfParametersForInsertionForStructs(char** sqlArray, int* numberOfParameters){

	int sqlCounter = 0, i = 0, j = 0, k = 0;
	int parameterCounter = 0;

	char* hString = malloc(16);
	char* iString = malloc(16);
	char* jString = malloc(16);

	int counterCardTypeCounter = 0;
	char* countercardtype = "INSERT INTO countercardtype (timeval,";

	for(i = 0; i < ADC_CHANNEL_COUNTER_CARD; i++){

		snprintf(iString, 16, "%d", i);

		countercardtype = concatenateChars(countercardtype, " adcdata_");
		countercardtype = concatenateChars(countercardtype, iString);
		countercardtype = concatenateChars(countercardtype, ",");

		counterCardTypeCounter++;

	}

	for(i = 0; i < MAX_COUNTER_CHANNEL; i++){

		snprintf(iString, 16, "%d", i);		

		countercardtype = concatenateChars(countercardtype, " shiftdelay_");
		countercardtype = concatenateChars(countercardtype, iString);
		countercardtype = concatenateChars(countercardtype, ",");

		countercardtype = concatenateChars(countercardtype, " gatedelaydelay_");
		countercardtype = concatenateChars(countercardtype, iString);
		countercardtype = concatenateChars(countercardtype, ",");

		countercardtype = concatenateChars(countercardtype, " gatewidth_");
		countercardtype = concatenateChars(countercardtype, iString);
		countercardtype = concatenateChars(countercardtype, ",");

		countercardtype = concatenateChars(countercardtype, " counts_");
		countercardtype = concatenateChars(countercardtype, iString);
		countercardtype = concatenateChars(countercardtype, ",");

		countercardtype = concatenateChars(countercardtype, " pulses_");
		countercardtype = concatenateChars(countercardtype, iString);
		countercardtype = concatenateChars(countercardtype, ",");
	
		int j = 0;
		for(j = 0; j < MAX_COUNTER_TIMESLOT; j++){

			snprintf(jString, 16, "%d", j);

			countercardtype = concatenateChars(countercardtype, " data_");
			countercardtype = concatenateChars(countercardtype, iString);
			countercardtype = concatenateChars(countercardtype, "_");
			countercardtype = concatenateChars(countercardtype, jString);
			countercardtype = concatenateChars(countercardtype, ",");			

		}

		for(j = 0; j < COUNTER_MASK_WIDTH; j++){

			char* jString = malloc(16);
			snprintf(jString, 16, "%d", j);

			countercardtype = concatenateChars(countercardtype, " mask_");
			countercardtype = concatenateChars(countercardtype, iString);
			countercardtype = concatenateChars(countercardtype, "_");
			countercardtype = concatenateChars(countercardtype, jString);
			countercardtype = concatenateChars(countercardtype, ",");

		}

		counterCardTypeCounter += 5 + MAX_COUNTER_TIMESLOT + COUNTER_MASK_WIDTH;

	}

	countercardtype = concatenateChars(countercardtype, "masterdelay) VALUES ($1,");
	counterCardTypeCounter++; //for timeval

	//countercardtype: building values parameters
	for(i = 1; i < counterCardTypeCounter; i++){

		k = i + 1;
		snprintf(iString, 16, "%d", k);
		countercardtype = concatenateChars(countercardtype, "$");
		countercardtype = concatenateChars(countercardtype, iString);
		countercardtype = concatenateChars(countercardtype, ",");


	}

	counterCardTypeCounter++; //for masterdelay
	snprintf(iString, 16, "%d", counterCardTypeCounter);
	countercardtype = concatenateChars(countercardtype, "$");
	countercardtype = concatenateChars(countercardtype, iString);
	countercardtype = concatenateChars(countercardtype, ");");


	*(sqlArray + sqlCounter) = countercardtype;
	*(numberOfParameters + parameterCounter) = counterCardTypeCounter;
	sqlCounter++;
	parameterCounter++;

	char* etalondatatype = "INSERT INTO etalondatatype(timeval,scanstepwidth,ditherstepwidth,offlinestepleft,offlinestepright,curspeed,setspeed,setaccl,statusword,refchannel,unused1,endswitchright,endswitchleft,unused2,set_low,set_high,set_position,current_low,current_high,current_position,encoder_low,encoder_high,encoder_position,index_low,index_high,index_position,online_low,online_high,online_position,scanstart_low,scanstart_high,scanstart_position,scanstop_low,scanstop_high,scanstop_position) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22, $23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35);";
	int etalondataTypeCounter = ETALON_DATA_TYPE_COUNTER;

	*(sqlArray + sqlCounter) = etalondatatype;
	*(numberOfParameters + parameterCounter) = etalondataTypeCounter;
	sqlCounter++;
	parameterCounter++;

	//filamentcardtype
	char* filamentcardtype = "INSERT INTO filamentcardtype(timeval,usdummy,usemissionvoltage,usemissioncurrent,uccommstatus, ucerrcount,usoscfinecount,actioncnt,usemissionuread,";
	int filamentcardTypeCounter = 9; //based on base parameters below

	//magic numbers: 32, 8, 7, 14 => based on the definition of FilamentCardType in elekIO.h (same magic numbers there)
	for(i = 0; i < 32; i++){

		snprintf(iString, 16, "%d", i);

		filamentcardtype = concatenateChars(filamentcardtype, "rawarray_");
		filamentcardtype = concatenateChars(filamentcardtype, iString);
		filamentcardtype = concatenateChars(filamentcardtype, ",");

		filamentcardTypeCounter++;

		if(i < 8){

			filamentcardtype = concatenateChars(filamentcardtype, "asignature_");
			filamentcardtype = concatenateChars(filamentcardtype, iString);
			filamentcardtype = concatenateChars(filamentcardtype, ",");
			filamentcardTypeCounter++;

		}

		if(i < 7){

			filamentcardtype = concatenateChars(filamentcardtype, "amainfill_");
			filamentcardtype = concatenateChars(filamentcardtype, iString);
			filamentcardtype = concatenateChars(filamentcardtype, ",");
			filamentcardTypeCounter++;

		}

		if(i < 14){

			filamentcardtype = concatenateChars(filamentcardtype, "wordarray_");
			filamentcardtype = concatenateChars(filamentcardtype, iString);
			filamentcardtype = concatenateChars(filamentcardtype, ",");
			filamentcardTypeCounter++;

		}
	}

	filamentcardtype = concatenateChars(filamentcardtype, " usemissioniread) VALUES (");
	filamentcardTypeCounter++;
	for(i = 0; i < filamentcardTypeCounter-1; i++){

		k = i + 1;
		snprintf(iString, 16, "%d", k);
		filamentcardtype = concatenateChars(filamentcardtype, "$");
		filamentcardtype = concatenateChars(filamentcardtype, iString);
		filamentcardtype = concatenateChars(filamentcardtype, ",");

	}
	snprintf(iString, 16, "%d", filamentcardTypeCounter);
	filamentcardtype = concatenateChars(filamentcardtype, "$");
	filamentcardtype = concatenateChars(filamentcardtype, iString);
	filamentcardtype = concatenateChars(filamentcardtype, ");");

	*(sqlArray + sqlCounter) = filamentcardtype;
	*(numberOfParameters + parameterCounter) = filamentcardTypeCounter;
	sqlCounter++;
	parameterCounter++;

	//backplaneadctype
	char* backplaneadctype = "INSERT INTO backplaneadctype(";
	int backplaneadctypeCounter = 0;

	for(i = 0; i < 32; i++){

		snprintf(iString, 16, "%d", i);

		backplaneadctype = concatenateChars(backplaneadctype, "rawarray_");
		backplaneadctype = concatenateChars(backplaneadctype, iString);
		backplaneadctype = concatenateChars(backplaneadctype, ",");
		backplaneadctypeCounter++;

		if(i < 8){

			backplaneadctype = concatenateChars(backplaneadctype, "dc_currents_a_");
			backplaneadctype = concatenateChars(backplaneadctype, iString);
			backplaneadctype = concatenateChars(backplaneadctype, ",");
			backplaneadctypeCounter++;			

			backplaneadctype = concatenateChars(backplaneadctype, "dc_currents_b_");
			backplaneadctype = concatenateChars(backplaneadctype, iString);
			backplaneadctype = concatenateChars(backplaneadctype, ",");
			backplaneadctypeCounter++;

			backplaneadctype = concatenateChars(backplaneadctype, "dcvoltages_");
			backplaneadctype = concatenateChars(backplaneadctype, iString);
			backplaneadctype = concatenateChars(backplaneadctype, ",");
			backplaneadctypeCounter++;

			backplaneadctype = concatenateChars(backplaneadctype, "truerms_reserved_");
			backplaneadctype = concatenateChars(backplaneadctype, iString);
			backplaneadctype = concatenateChars(backplaneadctype, ",");
			backplaneadctypeCounter++;

		}

	}

	backplaneadctype = concatenateChars(backplaneadctype, "timeval) VALUES(");
	backplaneadctypeCounter++;

	for(i = 0; i < backplaneadctypeCounter-1; i++){

		k = i + 1;
		snprintf(iString, 16, "%d", k);
		backplaneadctype = concatenateChars(backplaneadctype, "$");
		backplaneadctype = concatenateChars(backplaneadctype, iString);
		backplaneadctype = concatenateChars(backplaneadctype, ",");

	}
	snprintf(iString, 16, "%d", backplaneadctypeCounter);
	backplaneadctype = concatenateChars(backplaneadctype, "$");
	backplaneadctype = concatenateChars(backplaneadctype, iString);
	backplaneadctype = concatenateChars(backplaneadctype, ");");

	*(sqlArray + sqlCounter) = backplaneadctype;
	*(numberOfParameters + parameterCounter) = backplaneadctypeCounter;
	sqlCounter++;
	parameterCounter++;

	//no2countercardtype
	char* no2countercardtype = "INSERT INTO no2countercardtype(timeval,cardsignature,cardrevision,cardspare1,cardspare2,delayregister,pulsewidth,rawregister,bit_reset,reserved,bit_modaux,bit_modmain,bit_modenable,bit_testmode,bit_evenmorereserved,bit_startcopy,";
	int no2countercardtypeCounter = 16; //based on number of parameters above

	for(i = 0; i < 224; i++){

		snprintf(iString, 16, "%d", i);

		if(i < 8){

			no2countercardtype = concatenateChars(no2countercardtype, "adcchannels_");
			no2countercardtype = concatenateChars(no2countercardtype, iString);
			no2countercardtype = concatenateChars(no2countercardtype, ",");
			no2countercardtypeCounter++;

		}

		if(i < 112){

			no2countercardtype = concatenateChars(no2countercardtype, "counterarray_");
			no2countercardtype = concatenateChars(no2countercardtype, iString);
			no2countercardtype = concatenateChars(no2countercardtype, ",");
			no2countercardtypeCounter++;

		}
		
			no2countercardtype = concatenateChars(no2countercardtype, "rawarray_");
			no2countercardtype = concatenateChars(no2countercardtype, iString);
			no2countercardtype = concatenateChars(no2countercardtype, ",");
			no2countercardtypeCounter++;

	}

	no2countercardtype = concatenateChars(no2countercardtype, "pausewidth) VALUES(");
	no2countercardtypeCounter++;

	for(i = 0; i < no2countercardtypeCounter-1; i++){

		k = i + 1;
		snprintf(iString, 16, "%d", k);
		no2countercardtype = concatenateChars(no2countercardtype, "$");
		no2countercardtype = concatenateChars(no2countercardtype, iString);
		no2countercardtype = concatenateChars(no2countercardtype, ",");

	}
	snprintf(iString, 16, "%d", no2countercardtypeCounter);
	no2countercardtype = concatenateChars(no2countercardtype, "$");
	no2countercardtype = concatenateChars(no2countercardtype, iString);
	no2countercardtype = concatenateChars(no2countercardtype, ");");

	*(sqlArray + sqlCounter) = no2countercardtype;
	*(numberOfParameters + parameterCounter) = no2countercardtypeCounter;
	sqlCounter++;
	parameterCounter++;

	//instrumentflagstype
	char* instrumentflagstype = "INSERT INTO instrumentflagstype(timeval,statussave,statusquery,etalonaction,instrumentaction) VALUES ($1,$2,$3,$4,$5);";
	int instrumentflagstypeCounter = 5;

	*(sqlArray + sqlCounter) = instrumentflagstype;
	*(numberOfParameters + parameterCounter) = instrumentflagstypeCounter;
	sqlCounter++;
	parameterCounter++;


	//gpsdatatype
	char* gpsdatatype = "INSERT INTO gpsdatatype(timeval,ucutchours,ucutcmins,ucutcseconds,dlongitude,dlatitude,faltitude,fhdop, ucnumberofsatellites,uclastvaliddata,uigroundspeed,uiheading) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12);";
	int gpsdatatypeCounter = 12;

	*(sqlArray + sqlCounter) = gpsdatatype;
	*(numberOfParameters + parameterCounter) = gpsdatatypeCounter;
	sqlCounter++;
	parameterCounter++;

	//tscdatatype
	char* tscdatatype = "INSERT INTO tscdatatype(timeval,tscreceived_tscword_0,tscreceived_tscword_1,tscreceived_tscword_2, tscreceived_tscword_3, tscprocessed_tscword_0,tscprocessed_tscword_1,tscprocessed_tscword_2,tscprocessed_tscword_3) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9);";
	int tscdatatypeCounter = 9;

	*(sqlArray + sqlCounter) = tscdatatype;
	*(numberOfParameters + parameterCounter) = tscdatatypeCounter;
	sqlCounter++;
	parameterCounter++;

	//butterflytype
	char* butterflytype = "INSERT INTO butterflytype(timeval,positionvalid,currentposition,targetpositiongot,targetpositionset,butterflystatusword, cpufield_unused,cpufield_jtrf,cpufield_wdrf,cpufield_borf,cpufield_extrf,cpufield_porf,butterflycpuword,motorfield_oca,motorfield_ocb, motorfield_ola,motorfield_olb,motorfield_ochs,motorfield_uv,motorfield_otpw,motorfield_ot,motorfield_one,motorfield_load,motorfield_unused) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24);";
	int butterflytypeCounter = 24;

	*(sqlArray + sqlCounter) = butterflytype;
	*(numberOfParameters + parameterCounter) = butterflytypeCounter;
	sqlCounter++;
	parameterCounter++;

	//profibusmfcstatustype
	char* profibusmfcstatustype = "INSERT INTO profibusmfcstatustype(timeval,uscomstatus,";
	int profibusmfcstatustypeCounter = 2; //based on above parameter-count

	for(i = 0; i < MAX_MFC_PROFIBUS*36; i++){

		snprintf(iString, 16, "%d", i);

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "ucrawpayload_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

	}

	for(i = 0; i < MAX_MFC_PROFIBUS; i++){

		snprintf(iString, 16, "%d", i);

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "usflow_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "ussetpointread_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "ucpadding_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

		int j = 0;
		for(j = 0; j < 20; j++){

			snprintf(jString, 16, "%d", j);

			if(j < 7){

				profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "ucunit_");
				profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
				profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "_");
				profibusmfcstatustype = concatenateChars(profibusmfcstatustype, jString);
				profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
				profibusmfcstatustypeCounter++;

			}

			profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "ucserialnumber_");
			profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
			profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "_");
			profibusmfcstatustype = concatenateChars(profibusmfcstatustype, jString);
			profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
			profibusmfcstatustypeCounter++;

		}

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "maxflow_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "low_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "high_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;
		//***End profibus mfctype_normal

	}

	for(i = 0; i < 6; i++){

		snprintf(iString, 16, "%d", i);

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "usflow_dummy_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "ussetpointread_dummy_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "ucpadding_dummy_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

		int j = 0;
		for(j = 0; j < 20; j++){

			snprintf(jString, 16, "%d", j);

			if(j < 7){

				profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "ucunit_dummy_");
				profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
				profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "_");
				profibusmfcstatustype = concatenateChars(profibusmfcstatustype, jString);
				profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
				profibusmfcstatustypeCounter++;

			}

			profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "ucserialnumber_dummy_");
			profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
			profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "_");
			profibusmfcstatustype = concatenateChars(profibusmfcstatustype, jString);
			profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
			profibusmfcstatustypeCounter++;

		}

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "maxflow_dummy_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "low_dummy_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "high_dummy_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;
		//***End profibus mfctype_dummy

	}

	//**sReadJumoPID
	profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "ucinterfacemode,factualreading,fsetpointreading,fcontroloutput1,fcontroloutput2, fanalogueinput1,fanalogueinput2,sdigitalmarker,fanaloguemarker,fsetpoint1,srelayoutputs0021,srelayoutputs0023, scontrolcontacts0025, sbinarysignals0026,");
	profibusmfcstatustypeCounter += 14; //based on above paramter-count

	for(i = 0; i < 8; i++){

		snprintf(iString, 16, "%d", i);

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "aacyclicdatawrite_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;


	}

	//**smodbustunnel
	profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "jobok, jobbroken, jobtoggle2, jobtoggle1, reserved, modbusaccessmode, modbusaddress, integerdata, floatdata,");
	profibusmfcstatustypeCounter += 9; //based on above paramter-count


	//**swritejumopid
	for(i = 0; i < 8; i++){

		snprintf(iString, 16, "%d", i);

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "aacyclicdatawrite_write_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

	}

	profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "jobok_write, jobbroken_write, jobtoggle2_write, jobtoggle1_write, reserved_write, modbusaccessmode_write, modbusaddress_write, integerdata_write, floatdata_write,");
	profibusmfcstatustypeCounter += 9; //based on above paramter-count

	//ucpadding
	for(i = 0; i < 85; i++){

		snprintf(iString, 16, "%d", i);

		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "ucPadding_jumo_");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");
		profibusmfcstatustypeCounter++;

	}

	profibusmfcstatustype = concatenateChars(profibusmfcstatustype, " uidatavalid) VALUES(");
	profibusmfcstatustypeCounter++;

	for(i = 0; i < profibusmfcstatustypeCounter-1; i++){

		k = i + 1;
		snprintf(iString, 16, "%d", k);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "$");
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
		profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ",");

	}
	snprintf(iString, 16, "%d", profibusmfcstatustypeCounter);
	profibusmfcstatustype = concatenateChars(profibusmfcstatustype, "$");
	profibusmfcstatustype = concatenateChars(profibusmfcstatustype, iString);
	profibusmfcstatustype = concatenateChars(profibusmfcstatustype, ");");


	*(sqlArray + sqlCounter) = profibusmfcstatustype;
	*(numberOfParameters + parameterCounter) = profibusmfcstatustypeCounter;
	sqlCounter++;
	parameterCounter++;

	//receivelifohdatatype
	char* receivelifohdatatype =  "INSERT INTO receivelifohdatatype(timeval,ucdatavalid,switches, valves, abs_pressure, diff_pressure,pmt_signal ,pitot_temp,ai_power, ohtube_temp_0, ohtube_temp_1,ohtube_temp_2, roxtube_temp_0,roxtube_temp_1,penray_temp,pmt_temp,preamp_temp, ohtube_current_0,ohtube_current_1,ohtube_current_2,roxtube_current_0,roxtube_current_1,penray_current,pmt_current,preamp_current,ai_restrictor, ai_small_nose ,ai_big_nose,ai_small_ring,ai_big_ring) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25, $26,$27,$28,$29,$30);";
	int receivelifohdatatypeCounter = 30; //based on above parameter count

	*(sqlArray + sqlCounter) = receivelifohdatatype;
	*(numberOfParameters + parameterCounter) = receivelifohdatatypeCounter;
	sqlCounter++;
	parameterCounter++;

	//receivebahamaslighttype
	char* receivebahamaslighttype = "INSERT INTO receivebahamaslighttype(timeval,ucdatavalid,hp,tas,alpha,beta,tv,mixratio_h,abshum_h,tsl, rhos,wi_b, b_psa,b_ax,b_ay,b_az,c_p,c_pt,n_tat1,igi_lat,igi_lon,igi_alt,igi_nsv,igi_ewv,igi_vv,igi_roll,igi_pitch,igi_yaw,c_r115aca,c_r115acb, c_r115acc ,v_r28dc,c_r230ac,c_l115aca,c_l115acb,c_l115acc,c_l28dc,c_l230ac) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19, $20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,$36,$37,$38);";
	int receivebahamaslighttypeCounter = 38; //based on above parameter count

	*(sqlArray + sqlCounter) = receivebahamaslighttype;
	*(numberOfParameters + parameterCounter) = receivebahamaslighttypeCounter;
	sqlCounter++;
	parameterCounter++;

	//greenlaserstatustype
	char* greenlaserstatustype = "INSERT INTO greenlaserstatustype(timeval,usflags,userrorcode,fdiodecurrentreading,fdiodecurrentsetpoint, fheatsinktemperature_0,fheatsinktemperature_1,fheatsinktemperature_2,fheatsinktemperature_3,firpower,fqswitchfrequency,fthgttemp,fshgttemp, fspare1,fspare2) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15);";
	int greenlaserstatustypeCounter = 15; //based on above parameter count

	*(sqlArray + sqlCounter) = greenlaserstatustype;
	*(numberOfParameters + parameterCounter) = greenlaserstatustypeCounter;
	sqlCounter++;
	parameterCounter++;

	//bluelaserstatustype
	char* bluelaserstatustype = "INSERT INTO bluelaserstatustype(timeval,uspeakpower,usavgpower,usactualbias,usmodulatedbiaslevel,usmodlevel, usdiodetemperature,usctrlvoltage,ustriggervoltage,digitalmodulationmode,pwrstatus) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11);";
	int bluelaserstatustypeCounter = 11; //based on above parameter count

	*(sqlArray + sqlCounter) = bluelaserstatustype;
	*(numberOfParameters + parameterCounter) = bluelaserstatustypeCounter;
	sqlCounter++;
	parameterCounter++;

	//dyelaserstatustype
	char* dyelaserstatustype = "INSERT INTO dyelaserstatustype(timeval,fsetpressure,fispressure,fsettempdyelaser,fsistempdyelaser, fsettemplaserplate,fsistemplaserplate,usflags) VALUES ($1,$2,$3,$4,$5,$6,$7,$8);";
	int dyelaserstatustypeCounter = 8; //based on above parameter count

	*(sqlArray + sqlCounter) = dyelaserstatustype;
	*(numberOfParameters + parameterCounter) = dyelaserstatustypeCounter;
	sqlCounter++;
	parameterCounter++;

	//receiveGSBStatusType
	char* receivegsbstatustype = "INSERT INTO receivegsbstatustype(timeval,ucdatavalid,timeofdaymaster,timeofdaygsb,etypeofgsb,irawflowmfc1, irawflowmfc2,irawflowmfc3,irawpressureno1,irawpressureno2,irawpressureno3,irawpt100no1,irawpt100no2,irawpressureco1,irawpressureco2, irawpressureco3,irawpt100co1,itempadc0,itempadc1,ispare0,ispare1,ispare2,ispare3,uisetpointmfc0,uisetpointmfc1,uisetpointmfc2,uisetpointmfc3 ,uiavrfirmwarerevision,uivalvecontrolword,uivalvevoltageswitch,uivalvevoltagehold,uivalvevoltageis,uivalvevurrent,uiledcurrentset, uiledcurrentis,uiledvoltage,dflowmfc1,dflowmfc2,dflowmfc3,dpressureno1,dpressureno2,dpressureno3,dpt100no1,dpt100no2,dpressureco1, dpressureco2,dpressureco3,dpt100co1,dtempadc0,dtempadc1,dledcurrentis,dledcurrentset,dledvoltage,dvalvevoltageswitch,dvalvevoltagehold, dvalvevoltageis,dvalvecurrent) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27, $28,$29,$30,$31,$32,$33,$34,$35,$36::NUMERIC,$37,$38,$39,$40,$41,$42,$43,$44,$45,$46,$47,$48,$49,$50,$51,$52,$53,$54,$55,$56,$57);";
	int receivegsbstatustypeCounter = 57;

	*(sqlArray + sqlCounter) = receivegsbstatustype;
	*(numberOfParameters + parameterCounter) = receivegsbstatustypeCounter;
	sqlCounter++;
	parameterCounter++;


	//checking if something is null; checked for every char* in sqlArray and numberOfParameters
	for(i = 0; i < NUMBER_OF_STRUCTS; i++){

		if((sqlArray + i) == NULL || (numberOfParameters + i) == NULL){

			return 0; //error occured; better: log error in error log

		}


	}

	//all went well
	return 1;

}

int buildSQLCommandsAndNumberOfParametersForInsertionForArraystructs(char*** sqlArray, int* numberOfParameters){

	int i = 0;
	int j = 0;
	int h = 0;

	char* iString = malloc(16);
	char* jString = malloc(16);
	char* hString = malloc(16);


	int arrayCounter = 0;

	//Array of Tables => adccardtype
	char* adccardtypeArray[MAX_ADC_CARD_HORUS];
	int adccardtypeCounter = 2 + 9*MAX_ADC_CHANNEL_PER_CARD; // 2 => timeval and numsamples; 9 => Entries in MAX_ADC_CHANNEL_PER_CARD-array
	for(i = 0; i < MAX_ADC_CARD_HORUS; i++){

		char* sqlCommand = "INSERT INTO adccardtype_";
		snprintf(iString, 16, "%d", i);

		//sqlCommand = concatenateChars(sqlCommand, "INSERT INTO adccardtype_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, "(timeval,");

		//adding a number of structures
		for(j = 0; j < MAX_ADC_CHANNEL_PER_CARD; j++){

			snprintf(jString, 16, "%d", j);

			sqlCommand = concatenateChars(sqlCommand, "adcdata_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "sumdat_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "sumsqr_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "adcchannelconfig_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "muxchannel_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "bridge_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "gain_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, "l,");

			sqlCommand = concatenateChars(sqlCommand, "offset_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "unused_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

		}

		sqlCommand = concatenateChars(sqlCommand, "numsamples) VALUES(");

		for(h= 1; h < adccardtypeCounter; h++){

			snprintf(hString, 16, "%d", h);

			sqlCommand = concatenateChars(sqlCommand, "$");
			sqlCommand = concatenateChars(sqlCommand, hString);
			sqlCommand = concatenateChars(sqlCommand, ",");

		}

		snprintf(hString, 16, "%d", adccardtypeCounter);
		sqlCommand = concatenateChars(sqlCommand, "$");
		sqlCommand = concatenateChars(sqlCommand, hString);
		sqlCommand = concatenateChars(sqlCommand, ");");

		insertCharIntoCharArray(sqlCommand, adccardtypeArray, i);
		free(sqlCommand);

	}

	*(sqlArray + arrayCounter) = adccardtypeArray;
	*(numberOfParameters + arrayCounter) = adccardtypeCounter;
	arrayCounter++;

	//Array of Tables => mfccardtype
	char* mfccardtypeArray[MAX_MFC_CARD_HORUS];
	int mfccardtypeCounter = 2 + 11*MAX_MFC_CHANNEL_PER_CARD; //2 = timeval and nullsample
	for(i = 0; i < MAX_MFC_CARD_HORUS; i++){

		char* sqlCommand = (char*)malloc(1);
		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "INSERT INTO mfccardtype_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, "(timeval,");

		//adding a number of structures
		int j = 0;
		for(j = 0; j < MAX_MFC_CHANNEL_PER_CARD; j++){

			snprintf(jString, 16, "%d", j);

			sqlCommand = concatenateChars(sqlCommand, "flow_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "sumdat_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "setflow_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "sumsqr_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "mfcchannelconfig_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "muxchannel_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "unused_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "ch0_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "ch1_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "ch2_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "ch3_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

		}

		sqlCommand = concatenateChars(sqlCommand, "numsamples) VALUES (");

		for(h= 1; h < mfccardtypeCounter; h++){

			snprintf(hString, 16, "%d", h);

			sqlCommand = concatenateChars(sqlCommand, "$");
			sqlCommand = concatenateChars(sqlCommand, hString);
			sqlCommand = concatenateChars(sqlCommand, ",");

		}

		snprintf(hString, 16, "%d", mfccardtypeCounter);
		sqlCommand = concatenateChars(sqlCommand, "$");
		sqlCommand = concatenateChars(sqlCommand, hString);
		sqlCommand = concatenateChars(sqlCommand, ");");

		insertCharIntoCharArray(sqlCommand, mfccardtypeArray, i);
		free(sqlCommand);

	}

	*(sqlArray + arrayCounter) = mfccardtypeArray;
	*(numberOfParameters + arrayCounter) = mfccardtypeCounter;
	arrayCounter++;

	char* valvecardtypeArray[MAX_VALVE_CARD_HORUS];
	int valvecardtypeCounter = 4; //4 = timeval,valvevolt,valve,voltredcnt

	//Array of Tables => valvecardtype
	for(i = 0; i < MAX_VALVE_CARD_HORUS; i++){

		char* sqlCommand = (char*)malloc(1);
		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "INSERT INTO valvecardtype_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, "(timeval,valvevolt,valve,voltredcnt) VALUES (");

		for(h= 1; h < valvecardtypeCounter; h++){

			snprintf(hString, 16, "%d", h);
			sqlCommand = concatenateChars(sqlCommand, "$");
			sqlCommand = concatenateChars(sqlCommand, hString);
			sqlCommand = concatenateChars(sqlCommand, ",");

		}

		snprintf(hString, 16, "%d", valvecardtypeCounter);
		sqlCommand = concatenateChars(sqlCommand, "$");
		sqlCommand = concatenateChars(sqlCommand, hString);
		sqlCommand = concatenateChars(sqlCommand, ");");

		insertCharIntoCharArray(sqlCommand, valvecardtypeArray, i);
		free(sqlCommand);

	}

	*(sqlArray + arrayCounter) = valvecardtypeArray;
	*(numberOfParameters + arrayCounter) = valvecardtypeCounter;
	arrayCounter++;

	char* dcdc4cardtypeArray[MAX_DCDC4_CARD_HORUS];
	int dcdc4cardtypeCounter = 1 + MAX_DCDC4_CHANNEL_PER_CARD; //1 = timeval
	//Array of Tables => dcdc4cardtype
	for(i = 0; i < MAX_DCDC4_CARD_HORUS; i++){

		char* sqlCommand = (char*)malloc(1);
		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "INSERT INTO dcdc4cardtype_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, "(");

		//adding a number of structures
		int j = 0;
		for(j = 0; j < MAX_DCDC4_CHANNEL_PER_CARD; j++){

			snprintf(jString, 16, "%d", j);

			sqlCommand = concatenateChars(sqlCommand, "channel_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

		}

		for(h= 1; h <dcdc4cardtypeCounter; h++){

			snprintf(hString, 16, "%d", h);
			sqlCommand = concatenateChars(sqlCommand, "$");
			sqlCommand = concatenateChars(sqlCommand, hString);
			sqlCommand = concatenateChars(sqlCommand, ",");

		}

		snprintf(hString, 16, "%d", dcdc4cardtypeCounter);
		sqlCommand = concatenateChars(sqlCommand, "$");
		sqlCommand = concatenateChars(sqlCommand, hString);
		sqlCommand = concatenateChars(sqlCommand, ");");

		insertCharIntoCharArray(sqlCommand, dcdc4cardtypeArray, i);
		free(sqlCommand);

	}

	*(sqlArray + arrayCounter) = dcdc4cardtypeArray;
	*(numberOfParameters + arrayCounter) = dcdc4cardtypeCounter;
	arrayCounter++;

	char* tempsensorcardtypeArray[MAX_TEMP_SENSOR_CARD_HORUS];
	int tempsensorcardtypeCounter = 10 + 14*MAX_TEMP_SENSOR; //10 = timeval,numerrcrc,numerrnoresponse,word,busy,update,scan,unused,reset, numsensor
	//Array of Tables => tempsensorcardtype_; 14 variables per MAX_TEMP_SENSOR
	for(i = 0; i < MAX_TEMP_SENSOR_CARD_HORUS; i++){

		char* sqlCommand = (char*)malloc(1);
		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "CREATE TABLE tempsensorcardtype_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, "(timeval,numerrcrc,numerrnoresponse,word,busy,update,scan,unused,reset,");

		//adding a number of structures
		int j = 0;
		for(j = 0; j < MAX_TEMP_SENSOR; j++){

			snprintf(jString, 16, "%d", j);

			sqlCommand = concatenateChars(sqlCommand, "wordtemp_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "wordlimit_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "wordid_0_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "wordid_1_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "word_id_2_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "tempfrac_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "tempmain_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "bvalid_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "bcrcerror_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "bnoresponse_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "balarmflag_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "aromcode_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "ctemplimitmax_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");

			sqlCommand = concatenateChars(sqlCommand, "ctemplimitmin_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, ",");


		}
		sqlCommand = concatenateChars(sqlCommand, "numsensor) VALUES (");

		for(h= 1; h <tempsensorcardtypeCounter; h++){

			snprintf(hString, 16, "%d", h);
			sqlCommand = concatenateChars(sqlCommand, "$");
			sqlCommand = concatenateChars(sqlCommand, hString);
			sqlCommand = concatenateChars(sqlCommand, ",");

		}

		snprintf(hString, 16, "%d", tempsensorcardtypeCounter);
		sqlCommand = concatenateChars(sqlCommand, "$");
		sqlCommand = concatenateChars(sqlCommand, hString);
		sqlCommand = concatenateChars(sqlCommand, ");");

		insertCharIntoCharArray(sqlCommand, tempsensorcardtypeArray, i);
		free(sqlCommand);

	}

	*(sqlArray + arrayCounter) = tempsensorcardtypeArray;
	*(numberOfParameters + arrayCounter) = tempsensorcardtypeCounter;
	arrayCounter++;

	char* mirrordatatypeArray[MAX_MIRROR_CONTROLLER];
	int mirrordatatypeCounter = 7 + MAX_MIRROR*MAX_MIRROR_AXIS; //6 = timeval,minuvdiffcts,word,movingflagbyte,realigning,unused,realignminutes
	//Array of Tables => mirrordatatype
	for(i = 0; i < MAX_MIRROR_CONTROLLER; i++){

		char* sqlCommand = (char*)malloc(1);
		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "INSERT INTO mirrordatatype_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, "(timeval,minuvdiffcts,word,movingflagbyte,realigning,unused,");

		//adding a number of structures
		j = 0;
		for(j = 0; j < MAX_MIRROR; j++){

			snprintf(jString, 16, "%d", j);

			h = 0;
			for(h = 0; h < MAX_MIRROR_AXIS; h++){

				snprintf(hString, 16, "%d", h);

				sqlCommand = concatenateChars(sqlCommand, "position_");
				sqlCommand = concatenateChars(sqlCommand, jString);
				sqlCommand = concatenateChars(sqlCommand, "_");
				sqlCommand = concatenateChars(sqlCommand, hString);
				sqlCommand = concatenateChars(sqlCommand, " integer null,");

			}

		}

		for(h= 1; h < mirrordatatypeCounter; h++){

			snprintf(hString, 16, "%d", h);
			sqlCommand = concatenateChars(sqlCommand, "$");
			sqlCommand = concatenateChars(sqlCommand, hString);
			sqlCommand = concatenateChars(sqlCommand, ",");

		}

		snprintf(hString, 16, "%d", mirrordatatypeCounter);
		sqlCommand = concatenateChars(sqlCommand, "$");
		sqlCommand = concatenateChars(sqlCommand, hString);
		sqlCommand = concatenateChars(sqlCommand, ");");

		insertCharIntoCharArray(sqlCommand, mirrordatatypeArray, i);
		free(sqlCommand);

	}

	*(sqlArray + arrayCounter) = mirrordatatypeArray;
	*(numberOfParameters + arrayCounter) = mirrordatatypeCounter;

	//checking if something is null; checked for every char* in sqlArray and numberOfParameters
	for(i = 0; i < NUMBER_OF_ARRAYSTRUCTS; i++){

		if((sqlArray + i) == NULL || (numberOfParameters + i) == NULL){

			return 0; //error occured; better: log error in error log

		}


	}

	//all went well
	return 1;

}

/*
* -------------------------------------------
* Insert structures into database functions
* -------------------------------------------
*/
int insertCounterCardTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct CounterCardType data, PGconn *conn){

	//building const char array for countercardtype
	char *dataValues[numberOfEntries];
	int dataLength[numberOfEntries];
	int dataBinary[numberOfEntries];

	int i = 0;

	dataValues[0] = parseTimeval(data.TimeOfUpdate);
	dataLength[0] = strlen(dataValues[0])+1;
	dataBinary[0] = 0;

	for(i = 0; i < ADC_CHANNEL_COUNTER_CARD; i++){

		insertuint16_tIntoCharArray(data.ADCData[i], dataValues, i+1);
		dataLength[i+1] = sizeof(uint16_t);
		dataBinary[i+1] = 0;
		
	}

	for(i = 0; i < MAX_COUNTER_CHANNEL; i++){

		int counterOffset = i*MAX_COUNTER_TIMESLOT+i*COUNTER_MASK_WIDTH+i*4+ADC_CHANNEL_COUNTER_CARD;

		insertuint16_tIntoCharArray(data.Channel[i].ShiftDelay, dataValues, i+1+counterOffset);
		dataLength[i+1+counterOffset] = sizeof(uint16_t);
		dataBinary[i+1+counterOffset] = 0;

		insertuint16_tIntoCharArray(data.Channel[i].GateDelay, dataValues, i+2+counterOffset);
		dataLength[i+2+counterOffset] = sizeof(uint16_t);
		dataBinary[i+2+counterOffset] = 0;

		insertuint16_tIntoCharArray(data.Channel[i].GateWidth, dataValues, i+3+counterOffset);
		dataLength[i+3+counterOffset] = sizeof(uint16_t);
		dataBinary[i+3+counterOffset] = 0;

		insertuint16_tIntoCharArray(data.Channel[i].Counts, dataValues, i+4+counterOffset);
		dataLength[i+4+counterOffset] = sizeof(uint16_t);
		dataBinary[i+4+counterOffset] = 0;

		insertuint16_tIntoCharArray(data.Channel[i].Pulses, dataValues, i+5+counterOffset);
		dataLength[i+5+counterOffset] = sizeof(uint16_t);
		dataBinary[i+5+counterOffset] = 0;

		
		int j = 0;
		for(j = 0; j < MAX_COUNTER_TIMESLOT; j++){

			insertuint16_tIntoCharArray(data.Channel[i].Data[j], dataValues, i+j+6+counterOffset);
			dataLength[i+j+6+counterOffset] = sizeof(uint16_t);
			dataBinary[i+j+6+counterOffset] = 0;

		}


		for(j = 0; j < COUNTER_MASK_WIDTH; j++){


			insertuint16_tIntoCharArray(data.Channel[i].Mask[j], dataValues, i+j+6+counterOffset+MAX_COUNTER_TIMESLOT);
			dataLength[i+j+6+counterOffset+MAX_COUNTER_TIMESLOT] = sizeof(uint16_t);
			dataBinary[i+j+6+counterOffset+MAX_COUNTER_TIMESLOT] = 0;

		}
		
	}

	insertuint16_tIntoCharArray(data.MasterDelay, dataValues, numberOfEntries-1);
	dataLength[numberOfEntries-1] = sizeof(uint16_t);
	dataBinary[numberOfEntries-1] = 0;

	const char** constArray = (const char**) dataValues;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, dataLength, dataBinary);

	return 1; //return a success; error, if occured, is caught in executePreparedStatement

}

int insertEtalonDataTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct EtalonDataType data, PGconn *conn){

	//preapring arrays for etalon and inserting into database
	//building arrays for etalondatatype
	char *etalondataTypeValues[numberOfEntries];
	int etalondataTypeLength[numberOfEntries];
	int etalondataTypeBinary[numberOfEntries];
	int i = 0;

	//	printf("Time of update %s, counter %d", parseTimeval(data.TimeOfUpdate), timeCounter);


	etalondataTypeValues[0] = parseTimeval(data.TimeOfUpdate);
	etalondataTypeLength[0] = strlen(etalondataTypeValues[0]);

	insertuint16_tIntoCharArray(data.ScanStepWidth, etalondataTypeValues, 1);
	etalondataTypeLength[1] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.DitherStepWidth, etalondataTypeValues, 2);
	etalondataTypeLength[2] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.OfflineStepLeft, etalondataTypeValues, 3);
	etalondataTypeLength[3] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.OfflineStepRight, etalondataTypeValues, 4);
	etalondataTypeLength[4] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.CurSpeed, etalondataTypeValues, 5);
	etalondataTypeLength[5] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.SetSpeed, etalondataTypeValues, 6);
	etalondataTypeLength[6] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.SetAccl, etalondataTypeValues, 7);
	etalondataTypeLength[7] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Status.StatusWord, etalondataTypeValues, 8);
	etalondataTypeLength[8] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Status.StatusField.RefChannel, etalondataTypeValues, 9);
	etalondataTypeLength[9] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Status.StatusField.Unused1, etalondataTypeValues, 10);
	etalondataTypeLength[10] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Status.StatusField.EndswitchRight, etalondataTypeValues, 11);
	etalondataTypeLength[11] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Status.StatusField.EndswitchLeft, etalondataTypeValues, 12);
	etalondataTypeLength[12] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Status.StatusField.Unused2, etalondataTypeValues, 13);
	etalondataTypeLength[13] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Set.PositionWord.Low, etalondataTypeValues, 14);
	etalondataTypeLength[14] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Set.PositionWord.High, etalondataTypeValues, 15);
	etalondataTypeLength[15] = sizeof(uint16_t);
	insertuint32_tIntoCharArray(data.Set.Position, etalondataTypeValues, 16);
	etalondataTypeLength[16] = sizeof(uint32_t);
	insertuint16_tIntoCharArray(data.Current.PositionWord.Low, etalondataTypeValues, 17);
	etalondataTypeLength[17] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Current.PositionWord.High, etalondataTypeValues, 18);
	etalondataTypeLength[18] = sizeof(uint16_t);
	insertuint32_tIntoCharArray(data.Current.Position, etalondataTypeValues, 19);
	etalondataTypeLength[19] = sizeof(uint32_t);
	insertuint16_tIntoCharArray(data.Encoder.PositionWord.Low, etalondataTypeValues, 20);
	etalondataTypeLength[20] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Encoder.PositionWord.High, etalondataTypeValues, 21);
	etalondataTypeLength[21] = sizeof(uint16_t);
	insertuint32_tIntoCharArray(data.Encoder.Position, etalondataTypeValues, 22);
	etalondataTypeLength[22] = sizeof(uint32_t);
	insertuint16_tIntoCharArray(data.Index.PositionWord.Low, etalondataTypeValues, 23);
	etalondataTypeLength[23] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Index.PositionWord.High, etalondataTypeValues, 24);
	etalondataTypeLength[24] = sizeof(uint16_t);
	insertuint32_tIntoCharArray(data.Index.Position, etalondataTypeValues, 25);
	etalondataTypeLength[25] = sizeof(uint32_t);
	insertuint16_tIntoCharArray(data.Online.PositionWord.Low, etalondataTypeValues, 26);
	etalondataTypeLength[26] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Online.PositionWord.High, etalondataTypeValues, 27);
	etalondataTypeLength[27] = sizeof(uint16_t);
	insertuint32_tIntoCharArray(data.Online.Position, etalondataTypeValues, 28);
	etalondataTypeLength[28] = sizeof(uint32_t);
	insertuint16_tIntoCharArray(data.ScanStart.PositionWord.Low, etalondataTypeValues, 29);
	etalondataTypeLength[29] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.ScanStart.PositionWord.High, etalondataTypeValues, 30);
	etalondataTypeLength[30] = sizeof(uint16_t);
	insertuint32_tIntoCharArray(data.ScanStart.Position, etalondataTypeValues, 31);
	etalondataTypeLength[31] = sizeof(uint32_t);
	insertuint16_tIntoCharArray(data.ScanStop.PositionWord.Low, etalondataTypeValues, 32);
	etalondataTypeLength[32] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.ScanStop.PositionWord.High, etalondataTypeValues, 33);
	etalondataTypeLength[33] = sizeof(uint16_t);
	insertuint32_tIntoCharArray(data.ScanStop.Position, etalondataTypeValues, 34);
	etalondataTypeLength[34] = sizeof(uint32_t);

	for(i = 0; i < numberOfEntries; i++){

		etalondataTypeBinary[i] = 0;

	}

	const char** constTempArray = (const char**) etalondataTypeValues;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constTempArray, etalondataTypeLength, etalondataTypeBinary);
	return 1; //implement better Error logging

}

int insertFilamentCardTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct FilamentCardType data, PGconn *conn){

	char *filamentCardTypeValues[numberOfEntries];
	int filamentCardTypeLength[numberOfEntries];
	int filamentCardTypeBinary[numberOfEntries];
	int i = 0;

	filamentCardTypeValues[0] = parseTimeval(data.TimeOfUpdate);
	filamentCardTypeLength[0] = strlen(filamentCardTypeValues[0]);

	insertuint16_tIntoCharArray(data.Filament.Registers.usDummy, filamentCardTypeValues, 1);
	filamentCardTypeLength[1] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Filament.Registers.usEmissionVoltage, filamentCardTypeValues, 2);
	filamentCardTypeLength[2] = sizeof(uint16_t);
	insertuint16_tIntoCharArray(data.Filament.Registers.usEmissionCurrent, filamentCardTypeValues, 3);
	filamentCardTypeLength[3] = sizeof(uint16_t);
	insertuint8_tIntoCharArray(data.Filament.Registers.ucCommStatus, filamentCardTypeValues, 4);
	filamentCardTypeLength[4] = sizeof(uint8_t);
	insertuint8_tIntoCharArray(data.Filament.Registers.ucErrCount, filamentCardTypeValues, 5);
	filamentCardTypeLength[5] = sizeof(uint8_t);
	insertuint8_tIntoCharArray(data.Filament.Registers.ucOscFineCount, filamentCardTypeValues, 6);
	filamentCardTypeLength[6] = sizeof(uint8_t);
	insertuint8_tIntoCharArray(data.Filament.Registers.ActionCnt, filamentCardTypeValues, 7);
	filamentCardTypeLength[7] = sizeof(uint8_t);
	insertuint16_tIntoCharArray(data.Filament.Registers.usEmissionURead, filamentCardTypeValues, 8);
	filamentCardTypeLength[8] = sizeof(uint16_t);

	int arrayPosition = 9; //9: 9 parameters beforehand
	for(i = 0; i < 32; i++){

		
		insertuint16_tIntoCharArray(data.Filament.RawArray[i], filamentCardTypeValues, arrayPosition);
		filamentCardTypeLength[arrayPosition] = sizeof(uint16_t);
		arrayPosition++;

		if(i < 8){

			insertuint8_tIntoCharArray(data.Filament.Registers.aSignature[i], filamentCardTypeValues, arrayPosition);
			filamentCardTypeLength[arrayPosition] = sizeof(uint8_t);
			arrayPosition++;

		}

		if(i < 7){

			insertuint16_tIntoCharArray(data.Filament.Registers.aMainFill[i], filamentCardTypeValues, arrayPosition);
			filamentCardTypeLength[arrayPosition] = sizeof(uint16_t);
			arrayPosition++;

		}

		if(i < 14){

			insertuint16_tIntoCharArray(data.Filament.Registers.WordArray[i], filamentCardTypeValues, arrayPosition);
			filamentCardTypeLength[arrayPosition] = sizeof(uint16_t);
			arrayPosition++;

		}
	}

	insertuint16_tIntoCharArray(data.Filament.Registers.usEmissionIRead, filamentCardTypeValues, arrayPosition);
	filamentCardTypeLength[arrayPosition] = sizeof(uint16_t);

	for(i = 0; i < numberOfEntries; i++){

		filamentCardTypeBinary[i] = 0;

	}

	const char** constTempArray = (const char**) filamentCardTypeValues;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constTempArray, filamentCardTypeLength, filamentCardTypeBinary);

	return 1; //implement better Error logging

}

int insertBackplaneADCTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct BackplaneADCType data, PGconn *conn){

	char *backplaneADCTypeValues[numberOfEntries];
	int backplaneADCTypeLength[numberOfEntries];
	int backplaneADCTypeBinary[numberOfEntries];

	backplaneADCTypeValues[numberOfEntries-1] = parseTimeval(data.TimeOfUpdate);
	backplaneADCTypeLength[numberOfEntries-1] = strlen(backplaneADCTypeValues[numberOfEntries-1]);
	backplaneADCTypeBinary[numberOfEntries-1] = 0;

	int arrayPosition = 0;
	int i = 0;
	for(i = 0; i < 32; i++){

		insertuint16_tIntoCharArray(data.ADC.RawArray[i], backplaneADCTypeValues, arrayPosition);
		backplaneADCTypeLength[arrayPosition] = sizeof(uint16_t);
		backplaneADCTypeBinary[arrayPosition] = 0;
		arrayPosition++;

		if(i < 8){

			insertuint16_tIntoCharArray(data.ADC.Channel[i].DCCurrents_A, backplaneADCTypeValues, arrayPosition);
			backplaneADCTypeLength[arrayPosition] = sizeof(uint16_t);
			backplaneADCTypeBinary[arrayPosition] = 0;
			arrayPosition++;

			insertuint16_tIntoCharArray(data.ADC.Channel[i].DCCurrents_B, backplaneADCTypeValues, arrayPosition);
			backplaneADCTypeLength[arrayPosition] = sizeof(uint16_t);
			backplaneADCTypeBinary[arrayPosition] = 0;
			arrayPosition++;

			insertuint16_tIntoCharArray(data.ADC.Channel[i].DCVoltages, backplaneADCTypeValues, arrayPosition);
			backplaneADCTypeLength[arrayPosition] = sizeof(uint16_t);
			backplaneADCTypeBinary[arrayPosition] = 0;
			arrayPosition++;

			insertuint16_tIntoCharArray(data.ADC.Channel[i].TrueRMS_Reserved, backplaneADCTypeValues, arrayPosition);
			backplaneADCTypeLength[arrayPosition] = sizeof(uint16_t);
			backplaneADCTypeBinary[arrayPosition] = 0;
			arrayPosition++;

		}

	}

	const char** constArray = (const char**) backplaneADCTypeValues;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, backplaneADCTypeLength, backplaneADCTypeBinary);

	return 1;

}

int insertNO2CounterCardTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct NO2CounterCardType data, PGconn *conn){


	char *no2CounterCardTypeValues[numberOfEntries];
	int no2CounterCardTypeLength[numberOfEntries];
	int no2CounterCardTypeBinary[numberOfEntries];

	no2CounterCardTypeValues[0] = parseTimeval(data.TimeOfUpdate);
	no2CounterCardTypeLength[0] = strlen(no2CounterCardTypeValues[0]);
	no2CounterCardTypeBinary[0] = 0;

	insertuint16_tIntoCharArray(data.Controls.CardSignature, no2CounterCardTypeValues, 1);
	no2CounterCardTypeLength[1] = sizeof(uint16_t);
	no2CounterCardTypeBinary[1] = 0;
	insertuint16_tIntoCharArray(data.Controls.CardRevision, no2CounterCardTypeValues, 2);
	no2CounterCardTypeLength[2] = sizeof(uint16_t);
	no2CounterCardTypeBinary[2] = 0;
	insertuint16_tIntoCharArray(data.Controls.CardSpare1, no2CounterCardTypeValues, 3);
	no2CounterCardTypeLength[3] = sizeof(uint16_t);
	no2CounterCardTypeBinary[3] = 0;
	insertuint8_tIntoCharArray(data.Controls.CardSpare2, no2CounterCardTypeValues, 4);
	no2CounterCardTypeLength[4] = sizeof(uint16_t);
	no2CounterCardTypeBinary[4] = 0;
	insertuint8_tIntoCharArray(data.Controls.DelayRegister, no2CounterCardTypeValues, 5);
	no2CounterCardTypeLength[5] = sizeof(uint16_t);
	no2CounterCardTypeBinary[5] = 0;
	insertuint8_tIntoCharArray(data.Controls.PulseWidth, no2CounterCardTypeValues, 6);
	no2CounterCardTypeLength[6] = sizeof(uint16_t);
	no2CounterCardTypeBinary[6] = 0;
	insertuint8_tIntoCharArray(data.Controls.CardControl.RawRegister, no2CounterCardTypeValues, 7);
	no2CounterCardTypeLength[7] = sizeof(uint16_t);
	no2CounterCardTypeBinary[7] = 0;
	insertuint16_tIntoCharArray(data.Controls.CardControl.Bit_Reset, no2CounterCardTypeValues, 8);
	no2CounterCardTypeLength[8] = sizeof(uint16_t);
	no2CounterCardTypeBinary[8] = 0;
	insertuint16_tIntoCharArray(data.Controls.CardControl.Reserved, no2CounterCardTypeValues, 9);
	no2CounterCardTypeLength[9] = sizeof(uint16_t);
	no2CounterCardTypeBinary[9] = 0;
	insertuint16_tIntoCharArray(data.Controls.CardControl.Bit_ModAux, no2CounterCardTypeValues, 10);
	no2CounterCardTypeLength[10] = sizeof(uint16_t);
	no2CounterCardTypeBinary[10] = 0;
	insertuint16_tIntoCharArray(data.Controls.CardControl.Bit_ModMain, no2CounterCardTypeValues, 11);
	no2CounterCardTypeLength[11] = sizeof(uint16_t);
	no2CounterCardTypeBinary[11] = 0;
	insertuint8_tIntoCharArray(data.Controls.CardControl.Bit_ModEnable, no2CounterCardTypeValues, 12);
	no2CounterCardTypeLength[12] = sizeof(uint16_t);
	no2CounterCardTypeBinary[12] = 0;
	insertuint8_tIntoCharArray(data.Controls.CardControl.Bit_TestMode, no2CounterCardTypeValues, 13);
	no2CounterCardTypeLength[13] = sizeof(uint16_t);
	no2CounterCardTypeBinary[13] = 0;
	insertuint8_tIntoCharArray(data.Controls.CardControl.Bit_EvenMoreReserved, no2CounterCardTypeValues, 14);
	no2CounterCardTypeLength[14] = sizeof(uint16_t);
	no2CounterCardTypeBinary[14] = 0;
	insertuint8_tIntoCharArray(data.Controls.CardControl.Bit_StartCopy, no2CounterCardTypeValues, 15);
	no2CounterCardTypeLength[15] = sizeof(uint16_t);
	no2CounterCardTypeBinary[15] = 0;

	int arrayPosition = 16;
	int i = 0;
	for(i = 0; i < 224; i++){

		if(i < 8){

			insertuint16_tIntoCharArray(data.Controls.ADCChannels[i], no2CounterCardTypeValues, arrayPosition);
			no2CounterCardTypeLength[arrayPosition] = sizeof(uint16_t);
			no2CounterCardTypeBinary[arrayPosition] = 0;
			arrayPosition++;

		}

		if(i < 112){

			insertuint32_tIntoCharArray(data.Counters.CounterArray[i], no2CounterCardTypeValues, arrayPosition);
			no2CounterCardTypeLength[arrayPosition] = sizeof(uint32_t);
			no2CounterCardTypeBinary[arrayPosition] = 0;
			arrayPosition++;

		}

		insertuint16_tIntoCharArray(data.Counters.RawArray[i], no2CounterCardTypeValues, arrayPosition);
		no2CounterCardTypeLength[arrayPosition] = sizeof(uint16_t);
		no2CounterCardTypeBinary[arrayPosition] = 0;
		arrayPosition++;

	}


	insertuint16_tIntoCharArray(data.Controls.PauseWidth, no2CounterCardTypeValues, arrayPosition);
	no2CounterCardTypeLength[arrayPosition] = sizeof(uint16_t);
	no2CounterCardTypeBinary[arrayPosition] = 0;

	const char** constArray = (const char**) no2CounterCardTypeValues;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, no2CounterCardTypeLength, no2CounterCardTypeBinary);

	return 1;

}

int insertInstrumentFlagsTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct InstrumentFlagsType data, PGconn *conn){

	char *instrumentFlagsTypeValues[numberOfEntries];
	int instrumentFlagsTypeLength[numberOfEntries];
	int instrumentFlagsTypeBinary[numberOfEntries];

	instrumentFlagsTypeValues[0] = parseTimeval(data.TimeOfUpdate);
	instrumentFlagsTypeLength[0] = strlen(instrumentFlagsTypeValues[0]);
	instrumentFlagsTypeBinary[0] = 0;

	insertUintIntoCharArray(data.StatusSave, instrumentFlagsTypeValues, 1);
	instrumentFlagsTypeLength[1] = sizeof(unsigned int);
	instrumentFlagsTypeBinary[1] = 0;
	insertUintIntoCharArray(data.StatusQuery, instrumentFlagsTypeValues, 2);
	instrumentFlagsTypeLength[2] = sizeof(unsigned int);
	instrumentFlagsTypeBinary[2] = 0;
	insertIntIntoCharArray(data.EtalonAction, instrumentFlagsTypeValues, 3);
	instrumentFlagsTypeLength[3] = sizeof(int);
	instrumentFlagsTypeBinary[3] = 0;
	insertIntIntoCharArray(data.InstrumentAction, instrumentFlagsTypeValues, 4);
	instrumentFlagsTypeLength[4] = sizeof(int);
	instrumentFlagsTypeBinary[4] = 0;

	const char** constArray = (const char**) instrumentFlagsTypeValues;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, instrumentFlagsTypeLength, instrumentFlagsTypeBinary);

	return 1;

}

int insertGPSDataTypeIntoDatabatse(char* sqlCommand, int numberOfEntries, struct GPSDataType data, PGconn *conn){

	char *gpsDataTypeValue[numberOfEntries];
	int gpsDataTypeLength[numberOfEntries];
	int gpsDataTypeBinary[numberOfEntries];

	gpsDataTypeValue[0] = parseTimeval(data.TimeOfUpdate);
	gpsDataTypeLength[0] = strlen(gpsDataTypeValue[0]);
	gpsDataTypeBinary[0] = 0;

	insertUintIntoCharArray(data.ucUTCHours, gpsDataTypeValue, 1);
	gpsDataTypeLength[1] = sizeof(unsigned int);
	gpsDataTypeBinary[1] = 0;
	insertUintIntoCharArray(data.ucUTCMins, gpsDataTypeValue, 2);
	gpsDataTypeLength[2] = sizeof(unsigned int);
	gpsDataTypeBinary[2] = 0;
	insertUintIntoCharArray(data.ucUTCSeconds, gpsDataTypeValue, 3);
	gpsDataTypeLength[3] = sizeof(unsigned int);
	gpsDataTypeBinary[3] = 0;
	insertDoubleIntoCharArray(data.dLongitude, gpsDataTypeValue, 4);
	gpsDataTypeLength[4] = sizeof(double);
	gpsDataTypeBinary[4] = 0;
	insertDoubleIntoCharArray(data.dLatitude, gpsDataTypeValue, 5);
	gpsDataTypeLength[5] = sizeof(double);
	gpsDataTypeBinary[5] = 0;
	insertFloatIntoCharArray(data.fAltitude, gpsDataTypeValue, 6);
	gpsDataTypeLength[6] = sizeof(float);
	gpsDataTypeBinary[6] = 0;
	insertFloatIntoCharArray(data.fHDOP, gpsDataTypeValue, 7);
	gpsDataTypeLength[7] = sizeof(float);
	gpsDataTypeBinary[7] = 0;
	insertUintIntoCharArray(data.ucNumberOfSatellites, gpsDataTypeValue, 8);
	gpsDataTypeLength[8] = sizeof(unsigned int);
	gpsDataTypeBinary[8] = 0;
	insertUintIntoCharArray(data.ucLastValidData, gpsDataTypeValue, 9);
	gpsDataTypeLength[9] = sizeof(unsigned int);
	gpsDataTypeBinary[9] = 0;
	insertuint16_tIntoCharArray(data.uiGroundSpeed, gpsDataTypeValue, 10);
	gpsDataTypeLength[10] = sizeof(uint16_t);
	gpsDataTypeBinary[10] = 0;
	insertuint16_tIntoCharArray(data.uiHeading, gpsDataTypeValue, 11);
	gpsDataTypeLength[11] = sizeof(uint16_t);
	gpsDataTypeBinary[11] = 0;

	const char** constArray = (const char**) gpsDataTypeValue;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, gpsDataTypeLength, gpsDataTypeBinary);

	return 1;

}

int insertTSCDataTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct TSCDataType data, PGconn *conn){

	char *tscDataTypeValue[numberOfEntries];
	int tscDataTypeLength[numberOfEntries];
	int tscDataTypeBinary[numberOfEntries];

	tscDataTypeValue[0] = parseTimeval(data.TimeOfUpdate);
	tscDataTypeLength[0] = strlen(tscDataTypeValue[0]);
	tscDataTypeBinary[0] = 0;

	insertuint16_tIntoCharArray(data.TSCReceived.TSCSplit.TSCWord0, tscDataTypeValue, 1);
	tscDataTypeLength[1] = sizeof(uint16_t);
	tscDataTypeBinary[1] = 0;
	insertuint16_tIntoCharArray(data.TSCReceived.TSCSplit.TSCWord1, tscDataTypeValue, 2);
	tscDataTypeLength[2] = sizeof(uint16_t);
	tscDataTypeBinary[2] = 0;
	insertuint16_tIntoCharArray(data.TSCReceived.TSCSplit.TSCWord2, tscDataTypeValue, 3);
	tscDataTypeLength[3] = sizeof(uint16_t);
	tscDataTypeBinary[3] = 0;
	insertuint16_tIntoCharArray(data.TSCReceived.TSCSplit.TSCWord3, tscDataTypeValue, 4);
	tscDataTypeLength[4] = sizeof(uint16_t);
	tscDataTypeBinary[4] = 0;
	insertuint64_tIntoCharArray(data.TSCReceived.TSCValue, tscDataTypeValue, 5);
	tscDataTypeLength[5] = sizeof(uint64_t);
	tscDataTypeBinary[5] = 0;
	insertuint16_tIntoCharArray(data.TSCProcessed.TSCSplit.TSCWord0, tscDataTypeValue, 6);
	tscDataTypeLength[6] = sizeof(uint16_t);
	tscDataTypeBinary[6] = 0;
	insertuint16_tIntoCharArray(data.TSCProcessed.TSCSplit.TSCWord1, tscDataTypeValue, 7);
	tscDataTypeLength[7] = sizeof(uint16_t);
	tscDataTypeBinary[7] = 0;
	insertuint16_tIntoCharArray(data.TSCProcessed.TSCSplit.TSCWord2, tscDataTypeValue, 8);
	tscDataTypeLength[8] = sizeof(uint16_t);
	tscDataTypeBinary[8] = 0;
	insertuint16_tIntoCharArray(data.TSCProcessed.TSCSplit.TSCWord3, tscDataTypeValue, 9);
	tscDataTypeLength[9] = sizeof(uint16_t);
	tscDataTypeBinary[9] = 0;
	insertuint64_tIntoCharArray(data.TSCProcessed.TSCValue, tscDataTypeValue, 10);
	tscDataTypeLength[10] = sizeof(uint64_t);
	tscDataTypeBinary[10] = 0;

	const char** constArray = (const char**) tscDataTypeValue;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, tscDataTypeLength, tscDataTypeBinary);

	return 1;

}

int insertButterflyTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct ButterflyType data, PGconn *conn){

	char *butterflyTypeValue[numberOfEntries];
	int butterflyTypeLength[numberOfEntries];
	int butterflyTypeBinary[numberOfEntries];

	butterflyTypeValue[0] = parseTimeval(data.TimeOfUpdate);
	butterflyTypeLength[0] = strlen(butterflyTypeValue[0]);
	butterflyTypeBinary[0] = 0;
	insertuint16_tIntoCharArray(data.PositionValid, butterflyTypeValue, 1);
	butterflyTypeLength[1] = sizeof(uint16_t);
	butterflyTypeBinary[1] = 0;
	insertuint16_tIntoCharArray(data.CurrentPosition, butterflyTypeValue, 2);
	butterflyTypeLength[2] = sizeof(uint16_t);
	butterflyTypeBinary[2] = 0;
	insertuint16_tIntoCharArray(data.TargetPositionGot, butterflyTypeValue, 3);
	butterflyTypeLength[3] = sizeof(uint16_t);
	butterflyTypeBinary[3] = 0;
	insertuint16_tIntoCharArray(data.TargetPositionSet, butterflyTypeValue, 4);
	butterflyTypeLength[4] = sizeof(uint16_t);
	butterflyTypeBinary[4] = 0;
	insertuint16_tIntoCharArray(data.CPUStatus.StatusField.UNUSED, butterflyTypeValue, 5);
	butterflyTypeLength[5] = sizeof(uint16_t);
	butterflyTypeBinary[5] = 0;
	insertuint16_tIntoCharArray(data.CPUStatus.StatusField.JTRF, butterflyTypeValue, 6);
	butterflyTypeLength[6] = sizeof(uint16_t);
	butterflyTypeBinary[6] = 0;
	insertuint16_tIntoCharArray(data.CPUStatus.StatusField.WDRF, butterflyTypeValue, 7);
	butterflyTypeLength[7] = sizeof(uint16_t);
	butterflyTypeBinary[7] = 0;
	insertuint16_tIntoCharArray(data.CPUStatus.StatusField.BORF, butterflyTypeValue, 8);
	butterflyTypeLength[8] = sizeof(uint16_t);
	butterflyTypeBinary[8] = 0;
	insertuint16_tIntoCharArray(data.CPUStatus.StatusField.EXTRF, butterflyTypeValue, 9);
	butterflyTypeLength[9] = sizeof(uint16_t);
	butterflyTypeBinary[9] = 0;
	insertuint16_tIntoCharArray(data.CPUStatus.StatusField.PORF, butterflyTypeValue, 10);
	butterflyTypeLength[10] = sizeof(uint16_t);
	butterflyTypeBinary[10] = 0;
	insertuint16_tIntoCharArray(data.CPUStatus.ButterflyCPUWord, butterflyTypeValue, 11);
	butterflyTypeLength[11] = sizeof(uint16_t);
	butterflyTypeBinary[11] = 0;
	insertuint16_tIntoCharArray(data.MotorStatus.StatusField.OCA, butterflyTypeValue, 12);
	butterflyTypeLength[12] = sizeof(uint16_t);
	butterflyTypeBinary[12] = 0;
	insertuint16_tIntoCharArray(data.MotorStatus.StatusField.OCB, butterflyTypeValue, 13);
	butterflyTypeLength[13] = sizeof(uint16_t);
	butterflyTypeBinary[13] = 0;
	insertuint16_tIntoCharArray(data.MotorStatus.StatusField.OLA, butterflyTypeValue, 14);
	butterflyTypeLength[14] = sizeof(uint16_t);
	butterflyTypeBinary[14] = 0;
	insertuint16_tIntoCharArray(data.MotorStatus.StatusField.OLB, butterflyTypeValue, 15);
	butterflyTypeLength[15] = sizeof(uint16_t);
	butterflyTypeBinary[15] = 0;
	insertuint16_tIntoCharArray(data.MotorStatus.StatusField.OCHS, butterflyTypeValue, 16);
	butterflyTypeLength[16] = sizeof(uint16_t);
	butterflyTypeBinary[16] = 0;
	insertuint16_tIntoCharArray(data.MotorStatus.StatusField.UV, butterflyTypeValue, 17);
	butterflyTypeLength[17] = sizeof(uint16_t);
	butterflyTypeBinary[17] = 0;
	insertuint16_tIntoCharArray(data.MotorStatus.StatusField.OTPW, butterflyTypeValue, 18);
	butterflyTypeLength[18] = sizeof(uint16_t);
	butterflyTypeBinary[18] = 0;
	insertuint16_tIntoCharArray(data.MotorStatus.StatusField.OT, butterflyTypeValue, 19);
	butterflyTypeLength[19] = sizeof(uint16_t);
	butterflyTypeBinary[19] = 0;
	insertuint16_tIntoCharArray(data.MotorStatus.StatusField.ONE, butterflyTypeValue, 20);
	butterflyTypeLength[20] = sizeof(uint16_t);
	butterflyTypeBinary[20] = 0;
	insertuint16_tIntoCharArray(data.MotorStatus.StatusField.LOAD, butterflyTypeValue, 21);
	butterflyTypeLength[21] = sizeof(uint16_t);
	butterflyTypeBinary[21] = 0;
	insertuint16_tIntoCharArray(data.MotorStatus.StatusField.unused, butterflyTypeValue, 22);
	butterflyTypeLength[22] = sizeof(uint16_t);
	butterflyTypeBinary[22] = 0;
	insertuint16_tIntoCharArray(data.MotorStatus.ButterflyStatusWord, butterflyTypeValue, 23);
	butterflyTypeLength[23] = sizeof(uint16_t);
	butterflyTypeBinary[23] = 0;

	const char** constArray = (const char**) butterflyTypeValue;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, butterflyTypeLength, butterflyTypeBinary);

	return 1;

}


int insertProfibusMFCStatusTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct profibusMFCStatusType data, PGconn *conn){

	char *profibusMFCStatusTypeValue[numberOfEntries];
	int profibusMFCStatusTypeLength[numberOfEntries];
	int profibusMFCStatusTypeBinary[numberOfEntries];

	profibusMFCStatusTypeValue[0] = parseTimeval(data.TimeOfUpdate);
	profibusMFCStatusTypeLength[0] = strlen(profibusMFCStatusTypeValue[0]);
	profibusMFCStatusTypeBinary[0] = 0;

	insertuint16_tIntoCharArray(data.MFCData.usComStatus, profibusMFCStatusTypeValue, 1);
	profibusMFCStatusTypeLength[1] = sizeof(uint16_t);
	profibusMFCStatusTypeBinary[1] = 0;

	int currentArrayPosition = 2; //2 entries, timeofupdate and uscomstatus
	int i = 0;

	for(i = 0; i < MAX_MFC_PROFIBUS*36; i++){

		insertuint8_tIntoCharArray(data.MFCData.Payload.ucRawPayload[i], profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

	}

	for(i = 0; i < MAX_MFC_PROFIBUS; i++){

		insertuint16_tIntoCharArray(data.MFCData.Payload.sMFCData[i].usFlow, profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

		insertuint16_tIntoCharArray(data.MFCData.Payload.sMFCData[i].usSetPointRead, profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

		insertuint8_tIntoCharArray(data.MFCData.Payload.sMFCData[i].ucPadding, profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

		int j = 0;
		for(j = 0; j < 20; j++){

			if(j < 7){

				insertuint8_tIntoCharArray(data.MFCData.Payload.sMFCData[i].ucUnit[j], profibusMFCStatusTypeValue, currentArrayPosition);
				profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
				profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
				currentArrayPosition++;

			}

			insertuint8_tIntoCharArray(data.MFCData.Payload.sMFCData[i].ucSerialNumber[j], profibusMFCStatusTypeValue, currentArrayPosition);
			profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
			profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
			currentArrayPosition++;

		}

		insertFloatIntoCharArray(data.MFCData.Payload.sMFCData[i].unFullScale.fMaxFlow, profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(float);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

		insertuint16_tIntoCharArray(data.MFCData.Payload.sMFCData[i].unFullScale.FullScaleWord.Low, profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

		insertuint16_tIntoCharArray(data.MFCData.Payload.sMFCData[i].unFullScale.FullScaleWord.High, profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

	}

	for(i = 0; i < 6; i++){

		insertuint16_tIntoCharArray(data.MFCData.Payload.sJUMO.sDummyMFCs[i].usFlow, profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

		insertuint16_tIntoCharArray(data.MFCData.Payload.sJUMO.sDummyMFCs[i].usSetPointRead, profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

		insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sDummyMFCs[i].ucPadding, profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

		int j = 0;
		for(j = 0; j < 20; j++){

			if(j < 7){

				insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sDummyMFCs[i].ucUnit[j], profibusMFCStatusTypeValue, currentArrayPosition);
				profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
				profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
				currentArrayPosition++;

			}

			insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sDummyMFCs[i].ucSerialNumber[j], profibusMFCStatusTypeValue, currentArrayPosition);
			profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
			profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
			currentArrayPosition++;


		}

		insertFloatIntoCharArray(data.MFCData.Payload.sJUMO.sDummyMFCs[i].unFullScale.fMaxFlow, profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(float);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

		insertuint16_tIntoCharArray(data.MFCData.Payload.sJUMO.sDummyMFCs[i].unFullScale.FullScaleWord.Low, profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

		insertuint16_tIntoCharArray(data.MFCData.Payload.sJUMO.sDummyMFCs[i].unFullScale.FullScaleWord.High, profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

	}

	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.ucInterfaceMode, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertFloatIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.fActualReading, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(float);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertFloatIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.fSetPointReading, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(float);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertFloatIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.fControlOutput1, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(float);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertFloatIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.fControlOutput2, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(float);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertFloatIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.fAnalogueInput1, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(float);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertFloatIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.fAnalogueInput2, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(float);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint16_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.sDigitalMarker, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertFloatIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.fAnalogueMarker, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(float);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertFloatIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.fSetPoint1, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(float);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint16_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.sRelayOutputs0021, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint16_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.sBinaryInputs0023, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint16_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.sControlContacts0025, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint16_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.sBinarySignals0026, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	for(i = 0; i < 8; i++){

		insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.ReadModBus.aAcyclicDataWrite[i], profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

	}

	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.ReadModBus.ModBusStruct.JobOK, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.ReadModBus.ModBusStruct.JobBroken, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.ReadModBus.ModBusStruct.JobToggle2, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.ReadModBus.ModBusStruct.JobToggle1, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.ReadModBus.ModBusStruct.reserved, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.ReadModBus.ModBusStruct.ModBusAccessMode, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint16_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.ReadModBus.ModBusStruct.ModBusAddress, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint32_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.ReadModBus.ModBusStruct.Data.IntegerData, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint32_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertFloatIntoCharArray(data.MFCData.Payload.sJUMO.sJumoReadStruct.ReadModBus.ModBusStruct.Data.IntegerData, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(float);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	//**swritejumopid
	for(i = 0; i < 8; i++){

		insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoWriteStruct.WriteModBus.aAcyclicDataWrite[i], profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

	}

	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoWriteStruct.WriteModBus.ModBusStruct.JobOK, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoWriteStruct.WriteModBus.ModBusStruct.JobBroken, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;


	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoWriteStruct.WriteModBus.ModBusStruct.JobToggle2, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;


	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoWriteStruct.WriteModBus.ModBusStruct.JobToggle1, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;


	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoWriteStruct.WriteModBus.ModBusStruct.reserved, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;


	insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoWriteStruct.WriteModBus.ModBusStruct.ModBusAccessMode, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;


	insertuint16_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoWriteStruct.WriteModBus.ModBusStruct.ModBusAddress, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint16_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;


	insertuint32_tIntoCharArray(data.MFCData.Payload.sJUMO.sJumoWriteStruct.WriteModBus.ModBusStruct.Data.IntegerData, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint32_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;


	insertFloatIntoCharArray(data.MFCData.Payload.sJUMO.sJumoWriteStruct.WriteModBus.ModBusStruct.Data.FloatData, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(float);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
	currentArrayPosition++;

	//ucpadding
	for(i = 0; i < 85; i++){ //85: Magic Number derived from elekIO.h, line 1122

		insertuint8_tIntoCharArray(data.MFCData.Payload.sJUMO.ucPadding[i], profibusMFCStatusTypeValue, currentArrayPosition);
		profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
		profibusMFCStatusTypeBinary[currentArrayPosition] = 0;
		currentArrayPosition++;

	}

	insertuint8_tIntoCharArray(data.uiDataValid, profibusMFCStatusTypeValue, currentArrayPosition);
	profibusMFCStatusTypeLength[currentArrayPosition] = sizeof(uint8_t);
	profibusMFCStatusTypeBinary[currentArrayPosition] = 0;

	const char** constArray = (const char**) profibusMFCStatusTypeValue;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, profibusMFCStatusTypeLength, profibusMFCStatusTypeBinary);

	return 1;

}

int insertRecieveLIFOHDataTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct receiveLIFOHDataType data, PGconn *conn){

	char *receiveLIFOHDataTypeValues[numberOfEntries];
	int receiveLIFOHDataTypeLength[numberOfEntries];
	int receiveLIFOHDataTypeBinary[numberOfEntries];

	receiveLIFOHDataTypeValues[0] = parseTimeval(data.TimeOfUpdate);
	receiveLIFOHDataTypeLength[0] = strlen(receiveLIFOHDataTypeValues[0]);
	receiveLIFOHDataTypeBinary[0] = 0;

	insertuint8_tIntoCharArray(data.ucDataValid, receiveLIFOHDataTypeValues, 1);
	receiveLIFOHDataTypeLength[1] = sizeof(uint8_t);
	receiveLIFOHDataTypeBinary[1] = 0;
	insertUintIntoCharArray(data.Data.switches, receiveLIFOHDataTypeValues, 2);
	receiveLIFOHDataTypeLength[2] = sizeof(unsigned int);
	receiveLIFOHDataTypeBinary[2] = 0;
	insertUintIntoCharArray(data.Data.valves, receiveLIFOHDataTypeValues, 3);
	receiveLIFOHDataTypeLength[3] = sizeof(unsigned int);
	receiveLIFOHDataTypeBinary[3] = 0;
	insertDoubleIntoCharArray(data.Data.abs_pressure, receiveLIFOHDataTypeValues, 4);
	receiveLIFOHDataTypeLength[4] = sizeof(double);
	receiveLIFOHDataTypeBinary[4] = 0;
	insertDoubleIntoCharArray(data.Data.diff_pressure, receiveLIFOHDataTypeValues, 5);
	receiveLIFOHDataTypeLength[5] = sizeof(double);
	receiveLIFOHDataTypeBinary[5] = 0;
	insertDoubleIntoCharArray(data.Data.pmt_signal, receiveLIFOHDataTypeValues, 6);
	receiveLIFOHDataTypeLength[6] = sizeof(double);
	receiveLIFOHDataTypeBinary[6] = 0;
	insertDoubleIntoCharArray(data.Data.pitot_temp, receiveLIFOHDataTypeValues, 7);
	receiveLIFOHDataTypeLength[7] = sizeof(double);
	receiveLIFOHDataTypeBinary[7] = 0;
	insertUintIntoCharArray(data.Data.ai_power, receiveLIFOHDataTypeValues, 8);
	receiveLIFOHDataTypeLength[8] = sizeof(unsigned int);
	receiveLIFOHDataTypeBinary[8] = 0;
	insertDoubleIntoCharArray(data.Data.ohtube_current[0], receiveLIFOHDataTypeValues, 9);
	receiveLIFOHDataTypeLength[9] = sizeof(double);
	receiveLIFOHDataTypeBinary[9] = 0;
	insertDoubleIntoCharArray(data.Data.ohtube_current[1], receiveLIFOHDataTypeValues, 10);
	receiveLIFOHDataTypeLength[10] = sizeof(double);
	receiveLIFOHDataTypeBinary[10] = 0;
	insertDoubleIntoCharArray(data.Data.ohtube_current[2], receiveLIFOHDataTypeValues, 11);
	receiveLIFOHDataTypeLength[11] = sizeof(double);
	receiveLIFOHDataTypeBinary[11] = 0;
	insertDoubleIntoCharArray(data.Data.roxtube_temp[0], receiveLIFOHDataTypeValues, 12);
	receiveLIFOHDataTypeLength[12] = sizeof(double);
	receiveLIFOHDataTypeBinary[12] = 0;
	insertDoubleIntoCharArray(data.Data.roxtube_temp[1], receiveLIFOHDataTypeValues, 13);
	receiveLIFOHDataTypeLength[13] = sizeof(double);
	receiveLIFOHDataTypeBinary[13] = 0;
	insertDoubleIntoCharArray(data.Data.penray_temp, receiveLIFOHDataTypeValues, 14);
	receiveLIFOHDataTypeLength[14] = sizeof(double);
	receiveLIFOHDataTypeBinary[14] = 0;
	insertDoubleIntoCharArray(data.Data.pmt_temp, receiveLIFOHDataTypeValues, 15);
	receiveLIFOHDataTypeLength[15] = sizeof(double);
	receiveLIFOHDataTypeBinary[15] = 0;
	insertDoubleIntoCharArray(data.Data.preamp_temp, receiveLIFOHDataTypeValues, 16);
	receiveLIFOHDataTypeLength[16] = sizeof(double);
	receiveLIFOHDataTypeBinary[16] = 0;
	insertDoubleIntoCharArray(data.Data.ohtube_current[0], receiveLIFOHDataTypeValues, 17);
	receiveLIFOHDataTypeLength[17] = sizeof(double);
	receiveLIFOHDataTypeBinary[17] = 0;
	insertDoubleIntoCharArray(data.Data.ohtube_current[1], receiveLIFOHDataTypeValues, 18);
	receiveLIFOHDataTypeLength[18] = sizeof(double);
	receiveLIFOHDataTypeBinary[18] = 0;
	insertDoubleIntoCharArray(data.Data.ohtube_current[2], receiveLIFOHDataTypeValues, 19);
	receiveLIFOHDataTypeLength[19] = sizeof(double);
	receiveLIFOHDataTypeBinary[19] = 0;
	insertDoubleIntoCharArray(data.Data.roxtube_current[0], receiveLIFOHDataTypeValues, 20);
	receiveLIFOHDataTypeLength[20] = sizeof(double);
	receiveLIFOHDataTypeBinary[20] = 0;
	insertDoubleIntoCharArray(data.Data.roxtube_current[1], receiveLIFOHDataTypeValues, 21);
	receiveLIFOHDataTypeLength[21] = sizeof(double);
	receiveLIFOHDataTypeBinary[21] = 0;
	insertDoubleIntoCharArray(data.Data.penray_current, receiveLIFOHDataTypeValues, 22);
	receiveLIFOHDataTypeLength[22] = sizeof(double);
	receiveLIFOHDataTypeBinary[22] = 0;
	insertDoubleIntoCharArray(data.Data.pmt_current, receiveLIFOHDataTypeValues, 23);
	receiveLIFOHDataTypeLength[23] = sizeof(double);
	receiveLIFOHDataTypeBinary[23] = 0;
	insertDoubleIntoCharArray(data.Data.preamp_current, receiveLIFOHDataTypeValues, 24);
	receiveLIFOHDataTypeLength[24] = sizeof(double);
	receiveLIFOHDataTypeBinary[24] = 0;
	insertDoubleIntoCharArray(data.Data.ai_restrictor, receiveLIFOHDataTypeValues, 25);
	receiveLIFOHDataTypeLength[25] = sizeof(double);
	receiveLIFOHDataTypeBinary[25] = 0;
	insertDoubleIntoCharArray(data.Data.ai_small_nose, receiveLIFOHDataTypeValues, 26);
	receiveLIFOHDataTypeLength[26] = sizeof(double);
	receiveLIFOHDataTypeBinary[26] = 0;
	insertDoubleIntoCharArray(data.Data.ai_big_nose, receiveLIFOHDataTypeValues, 27);
	receiveLIFOHDataTypeLength[27] = sizeof(double);
	receiveLIFOHDataTypeBinary[27] = 0;
	insertDoubleIntoCharArray(data.Data.ai_small_ring, receiveLIFOHDataTypeValues, 28);
	receiveLIFOHDataTypeLength[28] = sizeof(double);
	receiveLIFOHDataTypeBinary[28] = 0;
	insertDoubleIntoCharArray(data.Data.ai_big_ring, receiveLIFOHDataTypeValues, 29);
	receiveLIFOHDataTypeLength[29] = sizeof(double);
	receiveLIFOHDataTypeBinary[29] = 0;

	const char** constArray = (const char**) receiveLIFOHDataTypeValues;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, receiveLIFOHDataTypeLength, receiveLIFOHDataTypeBinary);

	return 1;

}

int insertReceiveBahamasLightTypeintoDatabase(char* sqlCommand, int numberOfEntries, struct receiveBahamasLightType data, PGconn *conn){

	char *receiveBahamasLightTypeValues[numberOfEntries];
	int receiveBahamasLightTypeLength[numberOfEntries];
	int receiveBahamasLightTypeBinary[numberOfEntries];

	receiveBahamasLightTypeValues[0] = parseTimeval(data.TimeOfUpdate);
	receiveBahamasLightTypeLength[0] = strlen(receiveBahamasLightTypeValues[0]);
	receiveBahamasLightTypeBinary[0] = 0;

	insertuint8_tIntoCharArray(data.ucDataValid, receiveBahamasLightTypeValues, 1);
	receiveBahamasLightTypeLength[1] = sizeof(uint8_t);
	receiveBahamasLightTypeBinary[1] = 0;
	insertDoubleIntoCharArray(data.Data.HP, receiveBahamasLightTypeValues, 2);
	receiveBahamasLightTypeLength[2] = sizeof(double);
	receiveBahamasLightTypeBinary[2] = 0;
	insertDoubleIntoCharArray(data.Data.TAS, receiveBahamasLightTypeValues, 3);
	receiveBahamasLightTypeLength[3] = sizeof(double);
	receiveBahamasLightTypeBinary[3] = 0;
	insertDoubleIntoCharArray(data.Data.ALPHA, receiveBahamasLightTypeValues, 4);
	receiveBahamasLightTypeLength[4] = sizeof(double);
	receiveBahamasLightTypeBinary[4] = 0;
	insertDoubleIntoCharArray(data.Data.BETA, receiveBahamasLightTypeValues, 5);
	receiveBahamasLightTypeLength[5] = sizeof(double);
	receiveBahamasLightTypeBinary[5] = 0;
	insertDoubleIntoCharArray(data.Data.TV, receiveBahamasLightTypeValues, 6);
	receiveBahamasLightTypeLength[6] = sizeof(double);
	receiveBahamasLightTypeBinary[6] = 0;
	insertDoubleIntoCharArray(data.Data.MIXRATIO_H, receiveBahamasLightTypeValues, 7);
	receiveBahamasLightTypeLength[7] = sizeof(double);
	receiveBahamasLightTypeBinary[7] = 0;
	insertDoubleIntoCharArray(data.Data.ABSHUM_H, receiveBahamasLightTypeValues, 8);
	receiveBahamasLightTypeLength[8] = sizeof(double);
	receiveBahamasLightTypeBinary[8] = 0;
	insertDoubleIntoCharArray(data.Data.TSL, receiveBahamasLightTypeValues, 9);
	receiveBahamasLightTypeLength[9] = sizeof(double);
	receiveBahamasLightTypeBinary[9] = 0;
	insertDoubleIntoCharArray(data.Data.RHOS, receiveBahamasLightTypeValues, 10);
	receiveBahamasLightTypeLength[10] = sizeof(double);
	receiveBahamasLightTypeBinary[10] = 0;
	insertDoubleIntoCharArray(data.Data.VVI_B, receiveBahamasLightTypeValues, 11);
	receiveBahamasLightTypeLength[11] = sizeof(double);
	receiveBahamasLightTypeBinary[11] = 0;
	insertDoubleIntoCharArray(data.Data.B_PSA, receiveBahamasLightTypeValues, 12);
	receiveBahamasLightTypeLength[12] = sizeof(double);
	receiveBahamasLightTypeBinary[12] = 0;
	insertDoubleIntoCharArray(data.Data.B_AX, receiveBahamasLightTypeValues, 13);
	receiveBahamasLightTypeLength[13] = sizeof(double);
	receiveBahamasLightTypeBinary[13] = 0;
	insertDoubleIntoCharArray(data.Data.B_AY, receiveBahamasLightTypeValues, 14);
	receiveBahamasLightTypeLength[14] = sizeof(double);
	receiveBahamasLightTypeBinary[14] = 0;
	insertDoubleIntoCharArray(data.Data.B_AZ, receiveBahamasLightTypeValues, 15);
	receiveBahamasLightTypeLength[15] = sizeof(double);
	receiveBahamasLightTypeBinary[15] = 0;
	insertDoubleIntoCharArray(data.Data.C_P, receiveBahamasLightTypeValues, 16);
	receiveBahamasLightTypeLength[16] = sizeof(double);
	receiveBahamasLightTypeBinary[16] = 0;
	insertDoubleIntoCharArray(data.Data.C_PT, receiveBahamasLightTypeValues, 17);
	receiveBahamasLightTypeLength[17] = sizeof(double);
	receiveBahamasLightTypeBinary[17] = 0;
	insertDoubleIntoCharArray(data.Data.N_TAT1, receiveBahamasLightTypeValues, 18);
	receiveBahamasLightTypeLength[18] = sizeof(double);
	receiveBahamasLightTypeBinary[18] = 0;
	insertDoubleIntoCharArray(data.Data.IGI_LAT, receiveBahamasLightTypeValues, 19);
	receiveBahamasLightTypeLength[19] = sizeof(double);
	receiveBahamasLightTypeBinary[19] = 0;
	insertDoubleIntoCharArray(data.Data.IGI_LON, receiveBahamasLightTypeValues, 20);
	receiveBahamasLightTypeLength[20] = sizeof(double);
	receiveBahamasLightTypeBinary[20] = 0;
	insertDoubleIntoCharArray(data.Data.IGI_ALT, receiveBahamasLightTypeValues, 21);
	receiveBahamasLightTypeLength[21] = sizeof(double);
	receiveBahamasLightTypeBinary[21] = 0;
	insertDoubleIntoCharArray(data.Data.IGI_NSV, receiveBahamasLightTypeValues, 22);
	receiveBahamasLightTypeLength[22] = sizeof(double);
	receiveBahamasLightTypeBinary[22] = 0;
	insertDoubleIntoCharArray(data.Data.IGI_EWV, receiveBahamasLightTypeValues, 23);
	receiveBahamasLightTypeLength[23] = sizeof(double);
	receiveBahamasLightTypeBinary[23] = 0;
	insertDoubleIntoCharArray(data.Data.IGI_VV, receiveBahamasLightTypeValues, 24);
	receiveBahamasLightTypeLength[24] = sizeof(double);
	receiveBahamasLightTypeBinary[24] = 0;
	insertDoubleIntoCharArray(data.Data.IGI_ROLL, receiveBahamasLightTypeValues, 25);
	receiveBahamasLightTypeLength[25] = sizeof(double);
	receiveBahamasLightTypeBinary[25] = 0;
	insertDoubleIntoCharArray(data.Data.IGI_PITCH, receiveBahamasLightTypeValues, 26);
	receiveBahamasLightTypeLength[26] = sizeof(double);
	receiveBahamasLightTypeBinary[26] = 0;
	insertDoubleIntoCharArray(data.Data.IGI_YAW, receiveBahamasLightTypeValues, 27);
	receiveBahamasLightTypeLength[27] = sizeof(double);
	receiveBahamasLightTypeBinary[27] = 0;
	insertDoubleIntoCharArray(data.Data.C_R115ACA, receiveBahamasLightTypeValues, 28);
	receiveBahamasLightTypeLength[28] = sizeof(double);
	receiveBahamasLightTypeBinary[28] = 0;
	insertDoubleIntoCharArray(data.Data.C_R115ACB, receiveBahamasLightTypeValues, 29);
	receiveBahamasLightTypeLength[29] = sizeof(double);
	receiveBahamasLightTypeBinary[29] = 0;
	insertDoubleIntoCharArray(data.Data.C_R115ACC, receiveBahamasLightTypeValues, 30);
	receiveBahamasLightTypeLength[30] = sizeof(double);
	receiveBahamasLightTypeBinary[30] = 0;
	insertDoubleIntoCharArray(data.Data.C_R28DC, receiveBahamasLightTypeValues, 31);
	receiveBahamasLightTypeLength[31] = sizeof(double);
	receiveBahamasLightTypeBinary[31] = 0;
	insertDoubleIntoCharArray(data.Data.C_R230AC, receiveBahamasLightTypeValues, 32);
	receiveBahamasLightTypeLength[32] = sizeof(double);
	receiveBahamasLightTypeBinary[32] = 0;
	insertDoubleIntoCharArray(data.Data.C_L115ACA, receiveBahamasLightTypeValues, 33);
	receiveBahamasLightTypeLength[33] = sizeof(double);
	receiveBahamasLightTypeBinary[33] = 0;
	insertDoubleIntoCharArray(data.Data.C_L115ACB, receiveBahamasLightTypeValues, 34);
	receiveBahamasLightTypeLength[34] = sizeof(double);
	receiveBahamasLightTypeBinary[34] = 0;
	insertDoubleIntoCharArray(data.Data.C_L115ACC, receiveBahamasLightTypeValues, 35);
	receiveBahamasLightTypeLength[35] = sizeof(double);
	receiveBahamasLightTypeBinary[35] = 0;
	insertDoubleIntoCharArray(data.Data.C_L28DC, receiveBahamasLightTypeValues, 36);
	receiveBahamasLightTypeLength[36] = sizeof(double);
	receiveBahamasLightTypeBinary[36] = 0;
	insertDoubleIntoCharArray(data.Data.C_L230AC, receiveBahamasLightTypeValues, 37);
	receiveBahamasLightTypeLength[37] = sizeof(double);
	receiveBahamasLightTypeBinary[37] = 0;

	const char** constArray = (const char**) receiveBahamasLightTypeValues;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, receiveBahamasLightTypeLength, receiveBahamasLightTypeBinary);

	return 1;

}

int insertGreenLaserStatusTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct greenLaserStatusType data, PGconn *conn){

	char *greenLaserStatusTypeValues[numberOfEntries];
	int greenLaserStatusTypeLength[numberOfEntries];
	int greenLaserStatusTypeBinary[numberOfEntries];

	greenLaserStatusTypeValues[0] = parseTimeval(data.TimeOfUpdate);
	greenLaserStatusTypeLength[0] = strlen(greenLaserStatusTypeValues[0]);
	greenLaserStatusTypeBinary[0] = 0;

	insertuint16_tIntoCharArray(data.usFlags, greenLaserStatusTypeValues, 1);
	greenLaserStatusTypeLength[1] = sizeof(uint16_t);
	greenLaserStatusTypeBinary[1] = 0;
	insertuint16_tIntoCharArray(data.usErrorCode, greenLaserStatusTypeValues, 2);
	greenLaserStatusTypeLength[2] = sizeof(uint16_t);
	greenLaserStatusTypeBinary[2] = 0;
	insertFloatIntoCharArray(data.fDiodeCurrentReading, greenLaserStatusTypeValues, 3);
	greenLaserStatusTypeLength[3] = sizeof(float);
	greenLaserStatusTypeBinary[3] = 0;
	insertFloatIntoCharArray(data.fDiodeCurrentSetpoint, greenLaserStatusTypeValues, 4);
	greenLaserStatusTypeLength[4] = sizeof(float);
	greenLaserStatusTypeBinary[4] = 0;
	insertFloatIntoCharArray(data.fHeatsinkTemperature[0], greenLaserStatusTypeValues, 5);
	greenLaserStatusTypeLength[5] = sizeof(float);
	greenLaserStatusTypeBinary[5] = 0;
	insertFloatIntoCharArray(data.fHeatsinkTemperature[1], greenLaserStatusTypeValues, 6);
	greenLaserStatusTypeLength[6] = sizeof(float);
	greenLaserStatusTypeBinary[6] = 0;
	insertFloatIntoCharArray(data.fHeatsinkTemperature[2], greenLaserStatusTypeValues, 7);
	greenLaserStatusTypeLength[7] = sizeof(float);
	greenLaserStatusTypeBinary[7] = 0;
	insertFloatIntoCharArray(data.fHeatsinkTemperature[3], greenLaserStatusTypeValues, 8);
	greenLaserStatusTypeLength[8] = sizeof(float);
	greenLaserStatusTypeBinary[8] = 0;
	insertFloatIntoCharArray(data.fIRPower, greenLaserStatusTypeValues, 9);
	greenLaserStatusTypeLength[9] = sizeof(float);
	greenLaserStatusTypeBinary[9] = 0;
	insertFloatIntoCharArray(data.fQSwitchFrequency, greenLaserStatusTypeValues, 10);
	greenLaserStatusTypeLength[10] = sizeof(float);
	greenLaserStatusTypeBinary[10] = 0;
	insertFloatIntoCharArray(data.fTHGTemp, greenLaserStatusTypeValues, 11);
	greenLaserStatusTypeLength[11] = sizeof(float);
	greenLaserStatusTypeBinary[11] = 0;
	insertFloatIntoCharArray(data.fSHGTemp, greenLaserStatusTypeValues, 12);
	greenLaserStatusTypeLength[12] = sizeof(float);
	greenLaserStatusTypeBinary[12] = 0;
	insertFloatIntoCharArray(data.fSpare1, greenLaserStatusTypeValues, 13);
	greenLaserStatusTypeLength[13] = sizeof(float);
	greenLaserStatusTypeBinary[13] = 0;
	insertFloatIntoCharArray(data.fSpare2, greenLaserStatusTypeValues, 14);
	greenLaserStatusTypeLength[14] = sizeof(float);
	greenLaserStatusTypeBinary[14] = 0;


	const char** constArray = (const char**) greenLaserStatusTypeValues;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, greenLaserStatusTypeLength, greenLaserStatusTypeBinary);

	return 1;

}

int insertBlueLaserStatusTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct blueLaserStatusType data, PGconn *conn){

	char *blueLaserStatusTypeValues[numberOfEntries];
	int blueLaserStatusTypeLength[numberOfEntries];
	int blueLaserStatusTypeBinary[numberOfEntries];

	blueLaserStatusTypeValues[0] = parseTimeval(data.TimeOfUpdate);
	blueLaserStatusTypeLength[0] = strlen(blueLaserStatusTypeValues[0]);
	blueLaserStatusTypeBinary[0] = 0;

	insertuint16_tIntoCharArray(data.usPeakPower, blueLaserStatusTypeValues, 1);
	blueLaserStatusTypeLength[1] = sizeof(uint16_t);
	blueLaserStatusTypeBinary[1] = 0;
	insertuint16_tIntoCharArray(data.usAvgPower, blueLaserStatusTypeValues, 2);
	blueLaserStatusTypeLength[2] = sizeof(uint16_t);
	blueLaserStatusTypeBinary[2] = 0;
	insertuint16_tIntoCharArray(data.usActualBias, blueLaserStatusTypeValues, 3);
	blueLaserStatusTypeLength[3] = sizeof(uint16_t);
	blueLaserStatusTypeBinary[3] = 0;
	insertuint16_tIntoCharArray(data.usModulatedBiasLevel, blueLaserStatusTypeValues, 4);
	blueLaserStatusTypeLength[4] = sizeof(uint16_t);
	blueLaserStatusTypeBinary[4] = 0;
	insertuint16_tIntoCharArray(data.usModLevel, blueLaserStatusTypeValues, 5);
	blueLaserStatusTypeLength[5] = sizeof(uint16_t);
	blueLaserStatusTypeBinary[5] = 0;
	insertuint16_tIntoCharArray(data.usDiodeTemperature, blueLaserStatusTypeValues, 6);
	blueLaserStatusTypeLength[6] = sizeof(uint16_t);
	blueLaserStatusTypeBinary[6] = 0;
	insertuint16_tIntoCharArray(data.usCtrlVoltage, blueLaserStatusTypeValues, 7);
	blueLaserStatusTypeLength[7] = sizeof(uint16_t);
	blueLaserStatusTypeBinary[7] = 0;
	insertuint16_tIntoCharArray(data.usTriggerVoltage, blueLaserStatusTypeValues, 8);
	blueLaserStatusTypeLength[8] = sizeof(uint16_t);
	blueLaserStatusTypeBinary[8] = 0;
	insertIntIntoCharArray(data.DigitalModulationMode, blueLaserStatusTypeValues, 9);
	blueLaserStatusTypeLength[9] = sizeof(unsigned int);
	blueLaserStatusTypeBinary[9] = 0;
	insertIntIntoCharArray(data.PwrStatus, blueLaserStatusTypeValues, 10);
	blueLaserStatusTypeLength[10] = sizeof(unsigned int);
	blueLaserStatusTypeBinary[10] = 0;

	const char** constArray = (const char**) blueLaserStatusTypeValues;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, blueLaserStatusTypeLength, blueLaserStatusTypeBinary);

	return 1;

}

int insertDyeLaserStatusTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct DyeLaserStatusType data, PGconn *conn){

	char *dyeLaserStatusTypeValues[numberOfEntries];
	int dyeLaserStatusTypeLength[numberOfEntries];
	int dyeLaserStatusTypeBinary[numberOfEntries];

	dyeLaserStatusTypeValues[0] = parseTimeval(data.TimeOfUpdate);
	dyeLaserStatusTypeLength[0] = strlen(dyeLaserStatusTypeValues[0]);
	dyeLaserStatusTypeBinary[0] = 0;

	insertFloatIntoCharArray(data.fSetPressure, dyeLaserStatusTypeValues, 1);
	dyeLaserStatusTypeLength[1] = sizeof(float);
	dyeLaserStatusTypeBinary[1] = 0;
	insertFloatIntoCharArray(data.fIsPressure, dyeLaserStatusTypeValues, 2);
	dyeLaserStatusTypeLength[2] = sizeof(float);
	dyeLaserStatusTypeBinary[2] = 0;
	insertFloatIntoCharArray(data.fSetTempDyeLaser, dyeLaserStatusTypeValues, 3);
	dyeLaserStatusTypeLength[3] = sizeof(float);
	dyeLaserStatusTypeBinary[3] = 0;
	insertFloatIntoCharArray(data.fSIsTempDyeLaser, dyeLaserStatusTypeValues, 4);
	dyeLaserStatusTypeLength[4] = sizeof(float);
	dyeLaserStatusTypeBinary[4] = 0;
	insertFloatIntoCharArray(data.fSetTempLaserPlate, dyeLaserStatusTypeValues, 5);
	dyeLaserStatusTypeLength[5] = sizeof(float);
	dyeLaserStatusTypeBinary[5] = 0;
	insertFloatIntoCharArray(data.fSIsTempLaserPlate, dyeLaserStatusTypeValues, 6);
	dyeLaserStatusTypeLength[6] = sizeof(float);
	dyeLaserStatusTypeBinary[6] = 0;
	insertuint16_tIntoCharArray(data.usFlags, dyeLaserStatusTypeValues, 7);
	dyeLaserStatusTypeLength[7] = sizeof(uint16_t);
	dyeLaserStatusTypeBinary[7] = 0;

	const char** constArray = (const char**) dyeLaserStatusTypeValues;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, dyeLaserStatusTypeLength, dyeLaserStatusTypeBinary);

	return 1;

}

int insertReceiveGSBStatusTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct receiveGSBStatusType data, PGconn *conn){

	char *receiveGSBStatusTypeValues[numberOfEntries];
	int receiveGSBStatusTypeLength[numberOfEntries];
	int receiveGSBStatusTypeBinary[numberOfEntries];

	receiveGSBStatusTypeValues[0] = parseTimeval(data.TimeOfUpdate);
	receiveGSBStatusTypeLength[0] = strlen(receiveGSBStatusTypeValues[0]);
	receiveGSBStatusTypeBinary[0] = 0;

	insertuint8_tIntoCharArray(data.ucDataValid, receiveGSBStatusTypeValues, 1);
	receiveGSBStatusTypeLength[1] = sizeof(uint8_t);
	receiveGSBStatusTypeBinary[1] = 0;
	
	receiveGSBStatusTypeValues[2] = parseTimeval(data.GSBData.TimeOfDayMaster);
	receiveGSBStatusTypeLength[2] = strlen(receiveGSBStatusTypeValues[2]);
	receiveGSBStatusTypeBinary[2] = 0;
	receiveGSBStatusTypeValues[3] = parseTimeval(data.GSBData.TimeOfDayGSB);
	receiveGSBStatusTypeLength[3] = strlen(receiveGSBStatusTypeValues[3]);
	receiveGSBStatusTypeBinary[3] = 0;

	insertint32_tIntoCharArray(data.GSBData.eTypeOfGSB, receiveGSBStatusTypeValues, 4);
	receiveGSBStatusTypeLength[4] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[4] = 0;

	insertint32_tIntoCharArray(data.GSBData.iRawFlowMFC1, receiveGSBStatusTypeValues, 5);
	receiveGSBStatusTypeLength[5] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[5] = 0;
	insertint32_tIntoCharArray(data.GSBData.iRawFlowMFC2, receiveGSBStatusTypeValues, 6);
	receiveGSBStatusTypeLength[6] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[6] = 0;
	insertint32_tIntoCharArray(data.GSBData.iRawFlowMFC3, receiveGSBStatusTypeValues, 7);
	receiveGSBStatusTypeLength[7] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[7] = 0;
	insertint32_tIntoCharArray(data.GSBData.iRawPressureNO1, receiveGSBStatusTypeValues, 8);
	receiveGSBStatusTypeLength[8] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[8] = 0;
	insertint32_tIntoCharArray(data.GSBData.iRawPressureNO2, receiveGSBStatusTypeValues, 9);
	receiveGSBStatusTypeLength[9] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[9] = 0;
	insertint32_tIntoCharArray(data.GSBData.iRawPressureNO3, receiveGSBStatusTypeValues, 10);
	receiveGSBStatusTypeLength[10] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[10] = 0;
	insertint32_tIntoCharArray(data.GSBData.iRawPT100NO1, receiveGSBStatusTypeValues, 11);
	receiveGSBStatusTypeLength[11] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[11] = 0;
	insertint32_tIntoCharArray(data.GSBData.iRawPT100NO2, receiveGSBStatusTypeValues, 12);
	receiveGSBStatusTypeLength[12] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[12] = 0;
	insertint32_tIntoCharArray(data.GSBData.iRawPressureCO1, receiveGSBStatusTypeValues, 13);
	receiveGSBStatusTypeLength[13] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[13] = 0;
	insertint32_tIntoCharArray(data.GSBData.iRawPressureCO2, receiveGSBStatusTypeValues, 14);
	receiveGSBStatusTypeLength[14] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[14] = 0;
	insertint32_tIntoCharArray(data.GSBData.iRawPressureCO3, receiveGSBStatusTypeValues, 15);
	receiveGSBStatusTypeLength[15] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[15] = 0;
	insertint32_tIntoCharArray(data.GSBData.iRawPT100CO1, receiveGSBStatusTypeValues, 16);
	receiveGSBStatusTypeLength[16] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[16] = 0;
	insertint32_tIntoCharArray(data.GSBData.iTempADC0, receiveGSBStatusTypeValues, 17);
	receiveGSBStatusTypeLength[17] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[17] = 0;
	insertint32_tIntoCharArray(data.GSBData.iTempADC1, receiveGSBStatusTypeValues, 18);
	receiveGSBStatusTypeLength[18] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[18] = 0;
	insertint32_tIntoCharArray(data.GSBData.iSpare0, receiveGSBStatusTypeValues, 19);
	receiveGSBStatusTypeLength[19] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[19] = 0;
	insertint32_tIntoCharArray(data.GSBData.iSpare1, receiveGSBStatusTypeValues, 20);
	receiveGSBStatusTypeLength[20] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[20] = 0;
	insertint32_tIntoCharArray(data.GSBData.iSpare2, receiveGSBStatusTypeValues, 21);
	receiveGSBStatusTypeLength[21] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[21] = 0;
	insertint32_tIntoCharArray(data.GSBData.iSpare3, receiveGSBStatusTypeValues, 22);
	receiveGSBStatusTypeLength[22] = sizeof(int32_t);
	receiveGSBStatusTypeBinary[22] = 0;
	insertint16_tIntoCharArray(data.GSBData.uiSetPointMFC0, receiveGSBStatusTypeValues, 23);
	receiveGSBStatusTypeLength[23] = sizeof(int16_t);
	receiveGSBStatusTypeBinary[23] = 0;
	insertint16_tIntoCharArray(data.GSBData.uiSetPointMFC1, receiveGSBStatusTypeValues, 24);
	receiveGSBStatusTypeLength[24] = sizeof(int16_t);
	receiveGSBStatusTypeBinary[24] = 0;
	insertuint16_tIntoCharArray(data.GSBData.uiSetPointMFC2, receiveGSBStatusTypeValues, 25);
	receiveGSBStatusTypeLength[25] = sizeof(uint16_t);
	receiveGSBStatusTypeBinary[25] = 0;
	insertuint16_tIntoCharArray(data.GSBData.uiSetPointMFC3, receiveGSBStatusTypeValues, 26);
	receiveGSBStatusTypeLength[26] = sizeof(uint16_t);
	receiveGSBStatusTypeBinary[26] = 0;
	insertuint32_tIntoCharArray(data.GSBData.uiAVRFirmwareRevision, receiveGSBStatusTypeValues, 27);
	receiveGSBStatusTypeLength[27] = sizeof(uint32_t);
	receiveGSBStatusTypeBinary[27] = 0;
	insertuint16_tIntoCharArray(data.GSBData.uiValveControlWord, receiveGSBStatusTypeValues, 28);
	receiveGSBStatusTypeLength[28] = sizeof(uint16_t);
	receiveGSBStatusTypeBinary[28] = 0;
	insertuint16_tIntoCharArray(data.GSBData.uiValveVoltageSwitch, receiveGSBStatusTypeValues, 29);
	receiveGSBStatusTypeLength[29] = sizeof(uint16_t);
	receiveGSBStatusTypeBinary[29] = 0;
	insertuint16_tIntoCharArray(data.GSBData.uiValveVoltageHold, receiveGSBStatusTypeValues, 30);
	receiveGSBStatusTypeLength[30] = sizeof(uint16_t);
	receiveGSBStatusTypeBinary[30] = 0;
	insertuint16_tIntoCharArray(data.GSBData.uiValveVoltageIs, receiveGSBStatusTypeValues, 31);
	receiveGSBStatusTypeLength[31] = sizeof(uint16_t);
	receiveGSBStatusTypeBinary[31] = 0;
	insertuint16_tIntoCharArray(data.GSBData.uiValveCurrent, receiveGSBStatusTypeValues, 32);
	receiveGSBStatusTypeLength[32] = sizeof(uint16_t);
	receiveGSBStatusTypeBinary[32] = 0;
	insertuint16_tIntoCharArray(data.GSBData.uiLEDCurrentSet, receiveGSBStatusTypeValues, 33);
	receiveGSBStatusTypeLength[33] = sizeof(uint16_t);
	receiveGSBStatusTypeBinary[33] = 0;
	insertuint16_tIntoCharArray(data.GSBData.uiLEDCurrentIs, receiveGSBStatusTypeValues, 34);
	receiveGSBStatusTypeLength[34] = sizeof(uint16_t);
	receiveGSBStatusTypeBinary[34] = 0;
	insertuint16_tIntoCharArray(data.GSBData.uiLEDVoltage, receiveGSBStatusTypeValues, 35);
	receiveGSBStatusTypeLength[35] = sizeof(uint16_t);
	receiveGSBStatusTypeBinary[35] = 0;
	insertDoubleIntoCharArray(data.GSBData.dFlowMFC1, receiveGSBStatusTypeValues, 36);
	receiveGSBStatusTypeLength[36] = sizeof(double);
	receiveGSBStatusTypeBinary[36] = 0;
	insertDoubleIntoCharArray(data.GSBData.dFlowMFC2, receiveGSBStatusTypeValues, 37);
	receiveGSBStatusTypeLength[37] = sizeof(double);
	receiveGSBStatusTypeBinary[37] = 0;
	insertDoubleIntoCharArray(data.GSBData.dFlowMFC3, receiveGSBStatusTypeValues, 38);
	receiveGSBStatusTypeLength[38] = sizeof(double);
	receiveGSBStatusTypeBinary[38] = 0;
	insertDoubleIntoCharArray(data.GSBData.dPressureNO1, receiveGSBStatusTypeValues, 39);
	receiveGSBStatusTypeLength[39] = sizeof(double);
	receiveGSBStatusTypeBinary[39] = 0;
	insertDoubleIntoCharArray(data.GSBData.dPressureNO2, receiveGSBStatusTypeValues, 40);
	receiveGSBStatusTypeLength[40] = sizeof(double);
	receiveGSBStatusTypeBinary[40] = 0;
	insertDoubleIntoCharArray(data.GSBData.dPressureNO3, receiveGSBStatusTypeValues, 41);
	receiveGSBStatusTypeLength[41] = sizeof(double);
	receiveGSBStatusTypeBinary[41] = 0;
	insertDoubleIntoCharArray(data.GSBData.dPT100NO1, receiveGSBStatusTypeValues, 42);
	receiveGSBStatusTypeLength[42] = sizeof(double);
	receiveGSBStatusTypeBinary[42] = 0;
	insertDoubleIntoCharArray(data.GSBData.dPT100NO2, receiveGSBStatusTypeValues, 43);
	receiveGSBStatusTypeLength[43] = sizeof(double);
	receiveGSBStatusTypeBinary[43] = 0;
	insertDoubleIntoCharArray(data.GSBData.dPressureCO1, receiveGSBStatusTypeValues, 44);
	receiveGSBStatusTypeLength[44] = sizeof(double);
	receiveGSBStatusTypeBinary[44] = 0;
	insertDoubleIntoCharArray(data.GSBData.dPressureCO2, receiveGSBStatusTypeValues, 45);
	receiveGSBStatusTypeLength[45] = sizeof(double);
	receiveGSBStatusTypeBinary[45] = 0;
	insertDoubleIntoCharArray(data.GSBData.dPressureCO3, receiveGSBStatusTypeValues, 46);
	receiveGSBStatusTypeLength[46] = sizeof(double);
	receiveGSBStatusTypeBinary[46] = 0;
	insertDoubleIntoCharArray(data.GSBData.dPT100CO1, receiveGSBStatusTypeValues, 47);
	receiveGSBStatusTypeLength[47] = sizeof(double);
	receiveGSBStatusTypeBinary[47] = 0;
	insertDoubleIntoCharArray(data.GSBData.dTempADC0, receiveGSBStatusTypeValues, 48);
	receiveGSBStatusTypeLength[48] = sizeof(double);
	receiveGSBStatusTypeBinary[48] = 0;
	insertDoubleIntoCharArray(data.GSBData.dTempADC1, receiveGSBStatusTypeValues, 49);
	receiveGSBStatusTypeLength[49] = sizeof(double);
	receiveGSBStatusTypeBinary[49] = 0;
	insertDoubleIntoCharArray(data.GSBData.dLedCurrentIs, receiveGSBStatusTypeValues, 50);
	receiveGSBStatusTypeLength[50] = sizeof(double);
	receiveGSBStatusTypeBinary[50] = 0;
	insertDoubleIntoCharArray(data.GSBData.dLedCurrentSet, receiveGSBStatusTypeValues, 51);
	receiveGSBStatusTypeLength[51] = sizeof(double);
	receiveGSBStatusTypeBinary[51] = 0;
	insertDoubleIntoCharArray(data.GSBData.dLedVoltage, receiveGSBStatusTypeValues, 52);
	receiveGSBStatusTypeLength[52] = sizeof(double);
	receiveGSBStatusTypeBinary[52] = 0;
	insertDoubleIntoCharArray(data.GSBData.dValveVoltageSwitch, receiveGSBStatusTypeValues, 53);
	receiveGSBStatusTypeLength[53] = sizeof(double);
	receiveGSBStatusTypeBinary[53] = 0;
	insertDoubleIntoCharArray(data.GSBData.dValveVoltageHold, receiveGSBStatusTypeValues, 54);
	receiveGSBStatusTypeLength[54] = sizeof(double);
	receiveGSBStatusTypeBinary[54] = 0;
	insertDoubleIntoCharArray(data.GSBData.dValveVoltageIs, receiveGSBStatusTypeValues, 55);
	receiveGSBStatusTypeLength[55] = sizeof(double);
	receiveGSBStatusTypeBinary[55] = 0;
	insertDoubleIntoCharArray(data.GSBData.dValveCurrent, receiveGSBStatusTypeValues, 56);
	receiveGSBStatusTypeLength[56] = sizeof(double);
	receiveGSBStatusTypeBinary[56] = 0;

	const char** constArray = (const char**) receiveGSBStatusTypeValues;
	executePreparedStatement(conn, sqlCommand, numberOfEntries, constArray, receiveGSBStatusTypeLength, receiveGSBStatusTypeBinary);

	return 1;

}


int insertADCCardTypesIntoDatabase(char** sqlCommands, int numberOfEntries, struct elekStatusType data, PGconn *conn){

	int i = 0, j = 0;
	char *adcCardTypeValues[numberOfEntries];
	int adcCardTypeLength[numberOfEntries];
	int adcCardTypeBinary[numberOfEntries];

	for(i = 0; i < MAX_ADC_CARD_HORUS; i++){

		int arrayPositon = 1;

		adcCardTypeValues[0] = parseTimeval(data.ADCCardMaster[i].TimeOfUpdate);
		adcCardTypeLength[0] = strlen(adcCardTypeValues[0]);
		adcCardTypeBinary[0] = 0;

		for(j = 0; j < MAX_ADC_CHANNEL_PER_CARD; j++){

			insertuint16_tIntoCharArray(data.ADCCardMaster[i].ADCChannelData[j].ADCData, adcCardTypeValues, arrayPositon);
			adcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			adcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint32_tIntoCharArray(data.ADCCardMaster[i].ADCChannelData[j].SumDat, adcCardTypeValues, arrayPositon);
			adcCardTypeLength[arrayPositon] = sizeof(uint32_t);
			adcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint32_tIntoCharArray(data.ADCCardMaster[i].ADCChannelData[j].SumSqr, adcCardTypeValues, arrayPositon);
			adcCardTypeLength[arrayPositon] = sizeof(uint32_t);
			adcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.ADCCardMaster[i].ADCChannelConfig[j].ADCChannelConfig, adcCardTypeValues, arrayPositon);
			adcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			adcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.ADCCardMaster[i].ADCChannelConfig[j].ADCChannelConfigBit.MuxChannel, adcCardTypeValues, arrayPositon);
			adcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			adcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.ADCCardMaster[i].ADCChannelConfig[j].ADCChannelConfigBit.Bridge, adcCardTypeValues, arrayPositon);
			adcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			adcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.ADCCardMaster[i].ADCChannelConfig[j].ADCChannelConfigBit.Gain, adcCardTypeValues, arrayPositon);
			adcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			adcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.ADCCardMaster[i].ADCChannelConfig[j].ADCChannelConfigBit.Offset, adcCardTypeValues, arrayPositon);
			adcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			adcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.ADCCardMaster[i].ADCChannelConfig[j].ADCChannelConfigBit.Unused, adcCardTypeValues, arrayPositon);
			adcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			adcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;

		}

		insertuint16_tIntoCharArray(data.ADCCardMaster[i].NumSamples, adcCardTypeValues, arrayPositon);
		adcCardTypeLength[arrayPositon] = sizeof(uint16_t);
		adcCardTypeBinary[arrayPositon] = 0;

		const char** constArray = (const char**) adcCardTypeValues;
		executePreparedStatement(conn, sqlCommands[i], numberOfEntries, constArray, adcCardTypeLength, adcCardTypeBinary);

	}

	return 1;

}


int insertMFCCardTypeIntoDatabase(char** sqlCommands, int numberOfEntries, struct elekStatusType data, PGconn *conn){


	int i = 0, j = 0;
	char *mfcCardTypeValues[numberOfEntries];
	int mfcCardTypeLength[numberOfEntries];
	int mfcCardTypeBinary[numberOfEntries];

	for(i = 0; i < MAX_MFC_CARD_HORUS; i++){

		int arrayPositon = 1;

		mfcCardTypeValues[0] = parseTimeval(data.MFCCardMaster[i].TimeOfUpdate);
		mfcCardTypeLength[0] = strlen(mfcCardTypeValues[0]);
		mfcCardTypeBinary[0] = 0;

		for(j = 0; j< MAX_MFC_CHANNEL_PER_CARD; j++){

			insertuint16_tIntoCharArray(data.MFCCardMaster[i].MFCChannelData[j].SetFlow, mfcCardTypeValues, arrayPositon);
			mfcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			mfcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.MFCCardMaster[i].MFCChannelData[j].Flow, mfcCardTypeValues, arrayPositon);
			mfcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			mfcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint32_tIntoCharArray(data.MFCCardMaster[i].MFCChannelData[j].SumDat, mfcCardTypeValues, arrayPositon);
			mfcCardTypeLength[arrayPositon] = sizeof(uint32_t);
			mfcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint32_tIntoCharArray(data.MFCCardMaster[i].MFCChannelData[j].SumSqr, mfcCardTypeValues, arrayPositon);
			mfcCardTypeLength[arrayPositon] = sizeof(uint32_t);
			mfcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.MFCCardMaster[i].MFCChannelConfig[j].MFCChannelConfig, mfcCardTypeValues, arrayPositon);
			mfcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			mfcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.MFCCardMaster[i].MFCChannelConfig[j].MFCChannelConfigBit.MuxChannel, mfcCardTypeValues, arrayPositon);
			mfcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			mfcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.MFCCardMaster[i].MFCChannelConfig[j].MFCChannelConfigBit.Unused, mfcCardTypeValues, arrayPositon);
			mfcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			mfcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.MFCCardMaster[i].MFCChannelConfig[j].MFCChannelConfigBit.Ch0, mfcCardTypeValues, arrayPositon);
			mfcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			mfcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.MFCCardMaster[i].MFCChannelConfig[j].MFCChannelConfigBit.Ch1, mfcCardTypeValues, arrayPositon);
			mfcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			mfcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.MFCCardMaster[i].MFCChannelConfig[j].MFCChannelConfigBit.Ch2, mfcCardTypeValues, arrayPositon);
			mfcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			mfcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;
			insertuint16_tIntoCharArray(data.MFCCardMaster[i].MFCChannelConfig[j].MFCChannelConfigBit.Ch3, mfcCardTypeValues, arrayPositon);
			mfcCardTypeLength[arrayPositon] = sizeof(uint16_t);
			mfcCardTypeBinary[arrayPositon] = 0;
			arrayPositon++;

		}

		insertuint16_tIntoCharArray(data.MFCCardMaster[i].NumSamples, mfcCardTypeValues, arrayPositon);
		mfcCardTypeLength[arrayPositon] = sizeof(uint16_t);
		mfcCardTypeBinary[arrayPositon] = 0;

		const char** constArray = (const char**) mfcCardTypeValues;
		executePreparedStatement(conn, sqlCommands[i], numberOfEntries, constArray, mfcCardTypeLength, mfcCardTypeBinary);

	}

	return 1;

}


int insertValveCardTypeIntoDatabase(char** sqlCommands, int numberOfEntries, struct elekStatusType data, PGconn *conn){

	int i = 0, j = 0;
	char *valveCardTypeValues[numberOfEntries];
	int valveCardTypeLength[numberOfEntries];
	int valveCardTypeBinary[numberOfEntries];

	for(i = 0; i < MAX_VALVE_CARD_HORUS; i++){


		valveCardTypeValues[0] = parseTimeval(data.ValveCardMaster[i].TimeOfUpdate);
		valveCardTypeLength[0] = strlen(valveCardTypeValues[0]);
		valveCardTypeBinary[0] = 0;

		insertuint16_tIntoCharArray(data.ValveCardMaster[i].ValveVolt, valveCardTypeValues, 1);
		valveCardTypeLength[1] = sizeof(uint16_t);
		valveCardTypeBinary[1] = 0;
		insertuint16_tIntoCharArray(data.ValveCardMaster[i].Valve, valveCardTypeValues, 2);
		valveCardTypeLength[2] = sizeof(uint16_t);
		valveCardTypeBinary[2] = 0;
		insertuint16_tIntoCharArray(data.ValveCardMaster[i].VoltRedCnt, valveCardTypeValues, 3);
		valveCardTypeLength[3] = sizeof(uint16_t);
		valveCardTypeBinary[3] = 0;

		const char** constArray = (const char**) valveCardTypeValues;
		executePreparedStatement(conn, sqlCommands[i], numberOfEntries, constArray, valveCardTypeLength, valveCardTypeBinary);


	}

	return 1;

}

int insertDCDC4CardTypeIntoDatabase(char** sqlCommands, int numberOfEntries, struct elekStatusType data, PGconn *conn){


	int i = 0, j = 0;
	char *dcdc4CardTypeValues[numberOfEntries];
	int dcdc4CardTypeLength[numberOfEntries];
	int dcdc4CardTypeBinary[numberOfEntries];

	for(i = 0; i < MAX_DCDC4_CARD_HORUS; i++){

		int arrayPosition = 1;

		dcdc4CardTypeValues[0] = parseTimeval(data.DCDC4CardMaster[i].TimeOfUpdate);
		dcdc4CardTypeLength[0] = strlen(dcdc4CardTypeValues[0]);
		dcdc4CardTypeBinary[0] = 0;

		for(j = 0; j < MAX_DCDC4_CHANNEL_PER_CARD; j++){

			insertuint16_tIntoCharArray(data.DCDC4CardMaster[i].Channel[j], dcdc4CardTypeValues, arrayPosition);
			dcdc4CardTypeLength[arrayPosition] = sizeof(uint16_t);
			dcdc4CardTypeBinary[arrayPosition] = 0;
			arrayPosition++;

		}

		const char** constArray = (const char**) dcdc4CardTypeValues;
		executePreparedStatement(conn, sqlCommands[i], numberOfEntries, constArray, dcdc4CardTypeLength, dcdc4CardTypeBinary);

	}

	return 1;

}

int insertTempSensorCardTypeIntoDatabase(char** sqlCommands, int numberOfEntries, struct elekStatusType data, PGconn *conn){

	int i = 0, j = 0;
	char *tempSensorCardTypeValues[numberOfEntries];
	int tempSensorCardTypeLength[numberOfEntries];
	int tempSensorCardTypeBinary[numberOfEntries];

	for(i = 0; i < MAX_TEMP_SENSOR_CARD_HORUS; i++){

		int arrayPositon = 9;

		tempSensorCardTypeValues[0] = parseTimeval(data.TempSensCardMaster[i].TimeOfUpdate);
		tempSensorCardTypeLength[0] = strlen(tempSensorCardTypeValues[0]);
		tempSensorCardTypeBinary[0] = 0;		

		insertuint16_tIntoCharArray(data.TempSensCardMaster[i].NumErrCRC, tempSensorCardTypeValues, 1);
		tempSensorCardTypeLength[1] = sizeof(uint16_t);
		tempSensorCardTypeBinary[1] = 0;
		insertuint16_tIntoCharArray(data.TempSensCardMaster[i].NumErrNoResponse, tempSensorCardTypeValues, 2);
		tempSensorCardTypeLength[2] = sizeof(uint16_t);
		tempSensorCardTypeBinary[2] = 0;
		insertuint16_tIntoCharArray(data.TempSensCardMaster[i].Control.Word, tempSensorCardTypeValues, 3);
		tempSensorCardTypeLength[3] = sizeof(uint16_t);
		tempSensorCardTypeBinary[3] = 0;
		insertuint16_tIntoCharArray(data.TempSensCardMaster[i].Control.Field.Busy, tempSensorCardTypeValues, 4);
		tempSensorCardTypeLength[4] = sizeof(uint16_t);
		tempSensorCardTypeBinary[4] = 0;
		insertuint16_tIntoCharArray(data.TempSensCardMaster[i].Control.Field.Update, tempSensorCardTypeValues, 5);
		tempSensorCardTypeLength[5] = sizeof(uint16_t);
		tempSensorCardTypeBinary[5] = 0;
		insertuint16_tIntoCharArray(data.TempSensCardMaster[i].Control.Field.Scan, tempSensorCardTypeValues, 6);
		tempSensorCardTypeLength[6] = sizeof(uint16_t);
		tempSensorCardTypeBinary[6] = 0;
		insertuint16_tIntoCharArray(data.TempSensCardMaster[i].Control.Field.Unused, tempSensorCardTypeValues, 7);
		tempSensorCardTypeLength[7] = sizeof(uint16_t);
		tempSensorCardTypeBinary[7] = 0;
		insertuint16_tIntoCharArray(data.TempSensCardMaster[i].Control.Field.Reset, tempSensorCardTypeValues, 8);
		tempSensorCardTypeLength[8] = sizeof(uint16_t);
		tempSensorCardTypeBinary[8] = 0;

		for(j = 0; j < MAX_TEMP_SENSOR; j++){

			insertuint16_tIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Word.WordTemp, tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(uint16_t);
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertuint16_tIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Word.WordID[0], tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(uint16_t);
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertuint16_tIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Word.WordID[1], tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(uint16_t);
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertuint16_tIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Word.WordID[2], tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(uint16_t);
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertuint16_tIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Word.WordLimit, tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(uint16_t);
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertuint16_tIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Field.TempFrac, tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(uint16_t);
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertint16_tIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Field.TempMain, tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(int16_t);
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertuint16_tIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Field.bValid, tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(uint16_t);
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertuint16_tIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Field.bCRCError, tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(uint16_t);
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertuint16_tIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Field.bNoResponse, tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(uint16_t);
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertuint16_tIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Field.bAlarmFlag, tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(uint16_t);
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertUnsignedCharIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Field.aROMCode, tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = 6;
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertIntIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Field.cTempLimitMax, tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(int);
			tempSensorCardTypeBinary[arrayPositon] = 0;
			insertIntIntoCharArray(data.TempSensCardMaster[i].TempSensor[j].Field.cTempLimitMin, tempSensorCardTypeValues, arrayPositon);
			tempSensorCardTypeLength[arrayPositon] = sizeof(int);
			tempSensorCardTypeBinary[arrayPositon] = 0;

		}

		const char** constArray = (const char**) tempSensorCardTypeValues;
		executePreparedStatement(conn, sqlCommands[i], numberOfEntries, constArray, tempSensorCardTypeLength, tempSensorCardTypeBinary);
		
	}

	return 1;

}

int insertMirrorDataTypeIntoDatabase(char** sqlCommands, int numberOfEntries, struct elekStatusType data, PGconn *conn){

	int i = 0, j = 0, k = 0;
	char *mirrorDataTypeValues[numberOfEntries];
	int mirrorDataTypeLength[numberOfEntries];
	int mirrorDataTypeBinary[numberOfEntries];

	for(i = 0; i < MAX_MIRROR_CONTROLLER; i++){

		int arrayPosition = 6;
		mirrorDataTypeValues[0] = parseTimeval(data.MirrorData[i].TimeOfUpdate);
		mirrorDataTypeLength[0] = strlen(mirrorDataTypeValues[0]);
		mirrorDataTypeBinary[0] = 0;
		insertuint16_tIntoCharArray(data.MirrorData[i].MinUVDiffCts, mirrorDataTypeValues, 1);
		mirrorDataTypeLength[1] = sizeof(uint16_t);
		mirrorDataTypeBinary[1] = 0;
		insertuint16_tIntoCharArray(data.MirrorData[i].MovingFlag.Word, mirrorDataTypeValues, 2);
		mirrorDataTypeLength[2] = sizeof(uint16_t);
		mirrorDataTypeBinary[2] = 0;
		insertuint16_tIntoCharArray(data.MirrorData[i].MovingFlag.Field.MovingFlagByte, mirrorDataTypeValues, 3);
		mirrorDataTypeLength[3] = sizeof(uint16_t);
		mirrorDataTypeBinary[3] = 0;
		insertuint16_tIntoCharArray(data.MirrorData[i].MovingFlag.Field.Realigning, mirrorDataTypeValues, 4);
		mirrorDataTypeLength[4] = sizeof(uint16_t);
		mirrorDataTypeBinary[4] = 0;
		insertuint16_tIntoCharArray(data.MirrorData[i].MovingFlag.Field.unused, mirrorDataTypeValues, 5);
		mirrorDataTypeLength[5] = sizeof(uint16_t);
		mirrorDataTypeBinary[5] = 0;

		for(j = 0; j < MAX_MIRROR; j++){

			for(k = 0; k < MAX_MIRROR_AXIS; k++){

				insertint32_tIntoCharArray(data.MirrorData[i].Mirror[j].Axis[k].Position, mirrorDataTypeValues, arrayPosition);
				mirrorDataTypeLength[arrayPosition] = sizeof(int32_t);
				mirrorDataTypeBinary[arrayPosition] = 0;
				arrayPosition++;

			}

		}

		insertuint16_tIntoCharArray(data.MirrorData[i].RealignMinutes, mirrorDataTypeValues, arrayPosition);
		mirrorDataTypeLength[arrayPosition] = sizeof(uint16_t);
		mirrorDataTypeBinary[arrayPosition] = 0;

		const char** constArray = (const char**) mirrorDataTypeValues;
		executePreparedStatement(conn, sqlCommands[i], numberOfEntries, constArray, mirrorDataTypeLength, mirrorDataTypeBinary);


	}

	return 1;

}
/*
*
* Builds a database for filling in elekStatusType-structs
*
*/
void buildDatabase(char* dbname, char* user, char* password){

	printf("Connecting to database...\n");
	PGconn *conn = createConnection(dbname, user, password);
	printf("done!\n");


	//to be able to pares an integer to a string
	char* hString = malloc(16);
	char* iString = malloc(16);
	char* jString = malloc(16);

	int i = 0;
	char* sqlCommand = malloc(32);

	printf("Dropping tables to clean-up database...");

	//dropping the tables, to generate a clean database
	executeSQLCommand(conn, "DROP TABLE IF EXISTS countercardtype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS etalondatatype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS filamentcardtype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS backplaneadctype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS no2countercardtype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS instrumentflagstype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS gpsdatatype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS tscdatatype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS butterflytype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS profibusmfcstatustype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS receivelifohdatatype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS receivebahamaslighttype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS greenlaserstatustype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS bluelaserstatustype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS dyelaserstatustype;");
	executeSQLCommand(conn, "DROP TABLE IF EXISTS receivegsbstatustype;");
	
	
	//deleting an arry of tables => ADCCards
	for(i = 0; i < MAX_ADC_CARD_HORUS; i++){

		snprintf(iString, 16, "%d", i);
		
		sqlCommand = "DROP TABLE IF EXISTS adccardtype_";
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, ";");

		executeSQLCommand(conn, sqlCommand);
		free(sqlCommand);

	}

	//deleting an arry of tables => MFCCards
	for(i = 0; i < MAX_MFC_CARD_HORUS; i++){

		snprintf(iString, 16, "%d", i);
		
		sqlCommand = "DROP TABLE IF EXISTS mfccardtype_";
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, ";");

		executeSQLCommand(conn, sqlCommand);
		free(sqlCommand);

	}

	//deleting an arry of tables => ValveCards
	for(i = 0; i < MAX_VALVE_CARD_HORUS; i++){

		snprintf(iString, 16, "%d", i);
		
		sqlCommand = "DROP TABLE IF EXISTS valvecardtype_";
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, ";");

		executeSQLCommand(conn, sqlCommand);
		free(sqlCommand);

	}

	//deleting an arry of tables => DCDC4Cards
	for(i = 0; i < MAX_DCDC4_CARD_HORUS; i++){

		snprintf(iString, 16, "%d", i);
		
		sqlCommand = "DROP TABLE IF EXISTS dcdc4cardtype_";
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, ";");

		executeSQLCommand(conn, sqlCommand);
		free(sqlCommand);

	}

	//deleting an arry of tables => TempSensorCards
	for(i = 0; i < MAX_TEMP_SENSOR_CARD_HORUS; i++){

		snprintf(iString, 16, "%d", i);
		
		sqlCommand = "DROP TABLE IF EXISTS tempsensorcardtype_";
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, ";");

		executeSQLCommand(conn, sqlCommand);
		free(sqlCommand);

	}

	//deleting an arry of tables => MirrorDataType
	for(i = 0; i < MAX_MIRROR_CONTROLLER; i++){

		snprintf(iString, 16, "%d", i);
		
		sqlCommand = "DROP TABLE IF EXISTS mirrordatatype_";
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, ";");

		executeSQLCommand(conn, sqlCommand);
		free(sqlCommand);

	}

	//tables have been dropped, creating them anew

	printf("done!\n");
	printf("Building table countercardtype...");

	//countercardtype - TABLE
	sqlCommand = "CREATE TABLE countercardtype(id SERIAL primary key, timeval timestamp with time zone null,";

	
	for(i = 0; i < ADC_CHANNEL_COUNTER_CARD; i++){

		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, " adcdata_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

	}

	for(i = 0; i < MAX_COUNTER_CHANNEL; i++){

		snprintf(iString, 16, "%d", i);		

		sqlCommand = concatenateChars(sqlCommand, " shiftdelay_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		sqlCommand = concatenateChars(sqlCommand, " gatedelaydelay_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		sqlCommand = concatenateChars(sqlCommand, " gatewidth_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		sqlCommand = concatenateChars(sqlCommand, " counts_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		sqlCommand = concatenateChars(sqlCommand, " pulses_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");
	
		int j = 0;
		for(j = 0; j < MAX_COUNTER_TIMESLOT; j++){

			snprintf(jString, 16, "%d", j);

			sqlCommand = concatenateChars(sqlCommand, " data_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, "_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");			

		}

		for(j = 0; j < COUNTER_MASK_WIDTH; j++){

			char* jString = malloc(16);
			snprintf(jString, 16, "%d", j);

			sqlCommand = concatenateChars(sqlCommand, " mask_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, "_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

		}

	}

	sqlCommand = concatenateChars(sqlCommand, " masterdelay integer null);");
	
	executeSQLCommand(conn, sqlCommand);
	free(sqlCommand);

	printf("done!\n");
	printf("Building table etalondatatype...");

	//etalondata
	executeSQLCommand(conn, "CREATE TABLE etalondatatype(timeval timestamp with time zone primary key,scanstepwidth integer null, ditherstepwidth integer null,offlinestepleft integer null,offlinestepright integer null,curspeed integer null,setspeed integer null,setaccl integer null,statusword integer null,refchannel integer null,unused1 integer null,endswitchright integer null,endswitchleft integer null,unused2 integer null,set_low integer null,set_high integer null,set_position integer null,current_low integer null,current_high integer null,current_position integer null,encoder_low integer null,encoder_high integer null,encoder_position integer null,index_low integer null,index_high integer null,index_position integer null,online_low integer null,online_high integer null,online_position integer null,scanstart_low integer null,scanstart_high integer null,scanstart_position integer null,scanstop_low integer null,scanstop_high integer null,scanstop_position integer null );");

	printf("done!\n");
	printf("Building table filamentcardtype...");

	//filamentcardtype
	sqlCommand = "CREATE TABLE filamentcardtype(timeval timestamp with time zone primary key,usdummy integer null,usemissionvoltage integer null,usemissioncurrent integer null,uccommstatus integer null,ucerrcount integer null,usoscfinecount integer null,actioncnt integer null,usemissionuread integer null,";

	for(i = 0; i < 32; i++){

		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "rawarray_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		if(i < 8){

			sqlCommand = concatenateChars(sqlCommand, "asignature_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

		}

		if(i < 7){

			sqlCommand = concatenateChars(sqlCommand, "amainfill_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

		}

		if(i < 14){

			sqlCommand = concatenateChars(sqlCommand, "wordarray_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

		}
	}

	sqlCommand = concatenateChars(sqlCommand, " usemissioniread integer null);");
	
	executeSQLCommand(conn, sqlCommand);
	free(sqlCommand);

	printf("done!\n");
	printf("Building array of tables => adccardtype...");

	//Array of Tables => adccardtype
	for(i = 0; i < MAX_ADC_CARD_HORUS; i++){

		sqlCommand = "";
		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "CREATE TABLE adccardtype_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, "(timeval timestamp with time zone primary key,");

		//adding a number of structures
		int j = 0;
		for(j = 0; j < MAX_ADC_CHANNEL_PER_CARD; j++){

			snprintf(jString, 16, "%d", j);

			sqlCommand = concatenateChars(sqlCommand, "adcdata_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "sumdat_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "sumsqr_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "adcchannelconfig_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "muxchannel_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "bridge_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "gain_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "offset_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "unused_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

		}

		sqlCommand = concatenateChars(sqlCommand, "numsamples integer null);");

		executeSQLCommand(conn, sqlCommand);
		free(sqlCommand);

	}

	printf("done!\n");
	printf("Building array of tables => mfccardtype...");

	//Array of Tables => mfccardtype
	for(i = 0; i < MAX_MFC_CARD_HORUS; i++){

		sqlCommand = "";
		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "CREATE TABLE mfccardtype_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, "(timeval timestamp with time zone primary key,");

		//adding a number of structures
		int j = 0;
		for(j = 0; j < MAX_MFC_CHANNEL_PER_CARD; j++){

			snprintf(jString, 16, "%d", j);

			sqlCommand = concatenateChars(sqlCommand, "flow_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "sumdat_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "setflow_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "sumsqr_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "mfcchannelconfig_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "muxchannel_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "unused_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "ch0_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "ch1_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "ch2_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "ch3_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

		}

		sqlCommand = concatenateChars(sqlCommand, "numsamples integer null);");

		executeSQLCommand(conn, sqlCommand);
		free(sqlCommand);

	}

	printf("done!\n");
	printf("Building array of tables => valvecardtype...");

	//Array of Tables => valvecardtype
	for(i = 0; i < MAX_VALVE_CARD_HORUS; i++){

		sqlCommand = "";
		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "CREATE TABLE valvecardtype_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, "(timeval timestamp with time zone primary key, valvevolt integer null, valve integer null, voltredcnt integer null);");

		executeSQLCommand(conn, sqlCommand);
		free(sqlCommand);

	}

	printf("done!\n");
	printf("Building array of tables => dcdc4cardtype...");

	//Array of Tables => dcdc4cardtype
	for(i = 0; i < MAX_DCDC4_CARD_HORUS; i++){

		sqlCommand = "";
		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "CREATE TABLE dcdc4cardtype_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, "(");

		//adding a number of structures
		int j = 0;
		for(j = 0; j < MAX_DCDC4_CHANNEL_PER_CARD; j++){

			snprintf(jString, 16, "%d", j);

			sqlCommand = concatenateChars(sqlCommand, "channel_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

		}

		sqlCommand = concatenateChars(sqlCommand, "timeval timestamp with time zone primary key);");

		executeSQLCommand(conn, sqlCommand);
		free(sqlCommand);

	}

	printf("done!\n");
	printf("Building array of tables => tempsensorcardtype...");

	//Array of Tables => tempsensorcardtype_
	for(i = 0; i < MAX_TEMP_SENSOR_CARD_HORUS; i++){

		sqlCommand = "";
		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "CREATE TABLE tempsensorcardtype_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, "(timeval timestamp with time zone primary key,numerrcrc integer null, numerrnoresponse integer null,word integer null,busy integer null,update integer null,scan integer null,unused integer null,reset integer null,");

		//adding a number of structures
		int j = 0;
		for(j = 0; j < MAX_TEMP_SENSOR; j++){

			snprintf(jString, 16, "%d", j);

			sqlCommand = concatenateChars(sqlCommand, "wordtemp_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "wordlimit_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "wordid_0_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "wordid_1_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "word_id_2_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "tempfrac_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "tempmain_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "bvalid_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "bcrcerror_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "bnoresponse_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "balarmflag_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "aromcode_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " character(6) null,");

			sqlCommand = concatenateChars(sqlCommand, "ctemplimitmax_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " text null,");

			sqlCommand = concatenateChars(sqlCommand, "ctemplimitmin_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " text null,");

		}

		sqlCommand = concatenateChars(sqlCommand, " numsensor integer null);");

		executeSQLCommand(conn, sqlCommand);
		free(sqlCommand);

	}

	printf("done!\n");
	printf("Building table backplaneadctype...");

	//backplaneadctype
	sqlCommand = "CREATE TABLE backplaneadctype(";

	for(i = 0; i < 32; i++){

		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "rawarray_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		if(i < 8){

			sqlCommand = concatenateChars(sqlCommand, "dc_currents_a_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "dc_currents_b_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "dcvoltages_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

			sqlCommand = concatenateChars(sqlCommand, "truerms_reserved_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

		}

	}

	sqlCommand = concatenateChars(sqlCommand, "timeval timestamp with time zone primary key);");
	
	executeSQLCommand(conn, sqlCommand);
	free(sqlCommand);

	printf("done!\n");
	printf("Building table no2countercardtype...");

	//no2countercardtype
	sqlCommand = "CREATE TABLE no2countercardtype(timeval timestamp with time zone primary key,cardsignature integer null, cardrevision integer null, cardspare1 integer null, cardspare2 integer null, delayregister integer null, pulsewidth integer null,rawregister integer null,bit_reset integer null,reserved integer null,bit_modaux integer null,bit_modmain integer null,bit_modenable integer null,bit_testmode integer null,bit_evenmorereserved integer null,bit_startcopy integer null,";

	for(i = 0; i < 224; i++){

		snprintf(iString, 16, "%d", i);

		if(i < 8){

			sqlCommand = concatenateChars(sqlCommand, "adcchannels_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

		}

		if(i < 112){

			sqlCommand = concatenateChars(sqlCommand, "counterarray_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

		}
		
			sqlCommand = concatenateChars(sqlCommand, "rawarray_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

	}

	sqlCommand = concatenateChars(sqlCommand, "pausewidth integer null);");
	
	executeSQLCommand(conn, sqlCommand);
	free(sqlCommand);

	printf("done!\n");
	printf("Building table instrumentflagstype...");

	//instrumentflagstype
	executeSQLCommand(conn, "CREATE TABLE instrumentflagstype(timeval timestamp with time zone primary key,statussave text null, statusquery text null, etalonaction text null, instrumentaction text null);");

	printf("done!\n");
	printf("Building table gpsdatatype...");

	//gpsdatatype
	executeSQLCommand(conn, "CREATE TABLE gpsdatatype(timeval timestamp with time zone primary key,ucutchours text null, ucutcmins text null, ucutcseconds text null, dlongitude decimal null,dlatitude decimal null,faltitude decimal null, fhdop decimal null, ucnumberofsatellites text null, uclastvaliddata text null, uigroundspeed integer null, uiheading integer null);");


	printf("done!\n");
	printf("Building array of tables => mirrordatatype...");

	//Array of Tables => mirrordatatype
	for(i = 0; i < MAX_MIRROR_CONTROLLER; i++){

		sqlCommand = "";
		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "CREATE TABLE mirrordatatype_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, "(timeval timestamp with time zone primary key,minuvdiffcts integer null,word integer null, movingflagbyte integer null, realigning integer null, unused integer null,");

		//adding a number of structures
		int j = 0;
		for(j = 0; j < MAX_MIRROR; j++){

			snprintf(jString, 16, "%d", j);

			int h = 0;
			for(h = 0; h < MAX_MIRROR_AXIS; h++){

				snprintf(hString, 16, "%d", h);

				sqlCommand = concatenateChars(sqlCommand, "position_");
				sqlCommand = concatenateChars(sqlCommand, jString);
				sqlCommand = concatenateChars(sqlCommand, "_");
				sqlCommand = concatenateChars(sqlCommand, hString);
				sqlCommand = concatenateChars(sqlCommand, " integer null,");

			}

		}

		sqlCommand = concatenateChars(sqlCommand, " realignminutes integer null);");

		executeSQLCommand(conn, sqlCommand);
		free(sqlCommand);

	}

	printf("done!\n");
	printf("Building table tscdatatype...");

	//tscdatatype
	executeSQLCommand(conn, "CREATE TABLE tscdatatype(timeval timestamp with time zone primary key,tscreceived_tscword_0 bigint null,tscreceived_tscword_1 bigint null,tscreceived_tscword_2 bigint null,tscreceived_tscword_3 bigint null,tsreceived_tscvalue bigint null, tscprocessed_tscword_0 bigint null,tscprocessed_tscword_1 bigint null,tscprocessed_tscword_2 bigint null,tscprocessed_tscword_3 bigint null, tsprocessed_tscvalue bigint null);");

	printf("done!\n");
	printf("Building table butterflytype...");

	//butterflytype
	executeSQLCommand(conn, "CREATE TABLE butterflytype(timeval timestamp with time zone primary key,positionvalid integer null, currentposition integer null, targetpositiongot integer null, targetpositionset integer null, cpufield_unused integer null,cpufield_jtrf integer null,cpufield_wdrf integer null,cpufield_borf integer null,cpufield_extrf integer null,cpufield_porf integer null,butterflycpuword integer null,motorfield_oca integer null,motorfield_ocb integer null,motorfield_ola integer null,motorfield_olb integer null,motorfield_ochs integer null,motorfield_uv integer null,motorfield_otpw integer null,motorfield_ot integer null,motorfield_one integer null,motorfield_load integer null,motorfield_unused integer null, butterflystatusword integer null);");


	printf("done!\n");
	printf("Building table profibusmfcstatustype...");

	//profibusmfcstatustype
	sqlCommand = "CREATE TABLE profibusmfcstatustype(timeval timestamp with time zone primary key,uscomstatus integer null, ";

	for(i = 0; i < MAX_MFC_PROFIBUS*36; i++){

		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "ucrawpayload_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

	}

	for(i = 0; i < MAX_MFC_PROFIBUS; i++){

		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "usflow_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		sqlCommand = concatenateChars(sqlCommand, "ussetpointread_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		sqlCommand = concatenateChars(sqlCommand, "ucpadding_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		int j = 0;
		for(j = 0; j < 20; j++){

			snprintf(jString, 16, "%d", j);

			if(j < 7){

				sqlCommand = concatenateChars(sqlCommand, "ucunit_");
				sqlCommand = concatenateChars(sqlCommand, iString);
				sqlCommand = concatenateChars(sqlCommand, "_");
				sqlCommand = concatenateChars(sqlCommand, jString);
				sqlCommand = concatenateChars(sqlCommand, " integer null,");

			}

			sqlCommand = concatenateChars(sqlCommand, "ucserialnumber_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, "_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

		}

		sqlCommand = concatenateChars(sqlCommand, "maxflow_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " decimal null,");

		sqlCommand = concatenateChars(sqlCommand, "low_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		sqlCommand = concatenateChars(sqlCommand, "high_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");
		//***End profibus mfctype_normal

	}

	for(i = 0; i < 6; i++){

		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "usflow_dummy_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		sqlCommand = concatenateChars(sqlCommand, "ussetpointread_dummy_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		sqlCommand = concatenateChars(sqlCommand, "ucpadding_dummy_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		int j = 0;
		for(j = 0; j < 20; j++){

			snprintf(jString, 16, "%d", j);

			if(j < 7){

				sqlCommand = concatenateChars(sqlCommand, "ucunit_dummy_");
				sqlCommand = concatenateChars(sqlCommand, iString);
				sqlCommand = concatenateChars(sqlCommand, "_");
				sqlCommand = concatenateChars(sqlCommand, jString);
				sqlCommand = concatenateChars(sqlCommand, " integer null,");

			}

			sqlCommand = concatenateChars(sqlCommand, "ucserialnumber_dummy_");
			sqlCommand = concatenateChars(sqlCommand, iString);
			sqlCommand = concatenateChars(sqlCommand, "_");
			sqlCommand = concatenateChars(sqlCommand, jString);
			sqlCommand = concatenateChars(sqlCommand, " integer null,");

		}

		sqlCommand = concatenateChars(sqlCommand, "maxflow_dummy_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " decimal null,");

		sqlCommand = concatenateChars(sqlCommand, "low_dummy_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

		sqlCommand = concatenateChars(sqlCommand, "high_dummy_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");
		//***End profibus mfctype_dummy

	}

	//**sReadJumoPID
	sqlCommand = concatenateChars(sqlCommand, "ucinterfacemode integer null, factualreading decimal null, fsetpointreading decimal null, fcontroloutput1 decimal null, fcontroloutput2 decimal null, fanalogueinput1 decimal null, fanalogueinput2 decimal null, sdigitalmarker integer null, fanaloguemarker decimal null, fsetpoint1 decimal null, srelayoutputs0021 integer null, srelayoutputs0023 integer null, scontrolcontacts0025 integer null, sbinarysignals0026 integer null,");

	for(i = 0; i < 8; i++){

		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "aacyclicdatawrite_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

	}

	//**smodbustunnel
	sqlCommand = concatenateChars(sqlCommand, "jobok integer null, jobbroken integer null, jobtoggle2 integer null, jobtoggle1 integer null, reserved integer null, modbusaccessmode integer null, modbusaddress integer null, integerdata integer null, floatdata decimal null,");


	//**swritejumopid
	for(i = 0; i < 8; i++){

		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "aacyclicdatawrite_write_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

	}

	sqlCommand = concatenateChars(sqlCommand, "jobok_write integer null, jobbroken_write integer null, jobtoggle2_write integer null, jobtoggle1_write integer null, reserved_write integer null, modbusaccessmode_write integer null, modbusaddress_write integer null, integerdata_write integer null, floatdata_write decimal null,");

	//ucpadding
	for(i = 0; i < 85; i++){

		snprintf(iString, 16, "%d", i);

		sqlCommand = concatenateChars(sqlCommand, "ucPadding_jumo_");
		sqlCommand = concatenateChars(sqlCommand, iString);
		sqlCommand = concatenateChars(sqlCommand, " integer null,");

	}

	sqlCommand = concatenateChars(sqlCommand, " uidatavalid integer null);");

	executeSQLCommand(conn, sqlCommand);
	free(sqlCommand);

	printf("done!\n");
	printf("Building table receivelifohdatatype...");

	//receivelifohdatatype
	executeSQLCommand(conn, "CREATE TABLE receivelifohdatatype(timeval timestamp with time zone primary key,ucdatavalid integer null,switches text null, valves text null, abs_pressure decimal null, diff_pressure decimal null,pmt_signal decimal null,pitot_temp decimal null,ai_power text null, ohtube_temp_0 decimal null, ohtube_temp_1 decimal null,ohtube_temp_2 decimal null, roxtube_temp_0 decimal null,roxtube_temp_1 decimal null,penray_temp decimal null,pmt_temp decimal null,preamp_temp decimal null,ohtube_current_0 decimal null,ohtube_current_1 decimal null,ohtube_current_2 decimal null,roxtube_current_0 decimal null,roxtube_current_1 decimal null,penray_current decimal null,pmt_current decimal null,preamp_current decimal null,ai_restrictor decimal null,ai_small_nose decimal null,ai_big_nose decimal null,ai_small_ring decimal null,ai_big_ring decimal null);");

	printf("done!\n");
	printf("Building table receivebahamaslighttype...");

	//receivebahamaslighttype
	executeSQLCommand(conn, "CREATE TABLE receivebahamaslighttype(timeval timestamp with time zone primary key,ucdatavalid integer null,hp decimal null,tas decimal null,alpha decimal null,beta decimal null,tv decimal null,mixratio_h decimal null,abshum_h decimal null,tsl decimal null,rhos decimal null,wi_b decimal null,b_psa decimal null,b_ax decimal null,b_ay decimal null,b_az decimal null,c_p decimal null,c_pt decimal null,n_tat1 decimal null,igi_lat decimal null,igi_lon decimal null,igi_alt decimal null,igi_nsv decimal null,igi_ewv decimal null,igi_vv decimal null,igi_roll decimal null,igi_pitch decimal null,igi_yaw decimal null,c_r115aca decimal null,c_r115acb decimal null,c_r115acc decimal null,v_r28dc decimal null,c_r230ac decimal null,c_l115aca decimal null,c_l115acb decimal null,c_l115acc decimal null,c_l28dc decimal null,c_l230ac decimal null);");

	printf("done!\n");
	printf("Building table greenlaserstatustype...");

	//greenlaserstatustype
	executeSQLCommand(conn, "CREATE TABLE greenlaserstatustype(timeval timestamp with time zone primary key,usflags integer null,userrorcode integer null,fdiodecurrentreading decimal null,fdiodecurrentsetpoint decimal null,fheatsinktemperature_0 decimal null,fheatsinktemperature_1 decimal null,fheatsinktemperature_2 decimal null,fheatsinktemperature_3 decimal null,firpower decimal null,fqswitchfrequency decimal null,fthgttemp decimal null,fshgttemp decimal null,fspare1 decimal null,fspare2 decimal null);");

	printf("done!\n");
	printf("Building table bluelasertatustype...");

	//bluelaserstatustype
	executeSQLCommand(conn, "CREATE TABLE bluelaserstatustype(timeval timestamp with time zone primary key,uspeakpower integer null,usavgpower integer null,usactualbias integer null,usmodulatedbiaslevel integer null,usmodlevel integer null,usdiodetemperature integer null,usctrlvoltage integer null,ustriggervoltage integer null,digitalmodulationmode integer null,pwrstatus integer null);");

	printf("done!\n");
	printf("Building table dyelaserstatustype...");

	//dyelaserstatustype
	executeSQLCommand(conn, "CREATE TABLE dyelaserstatustype(timeval timestamp with time zone primary key,fsetpressure decimal null,fispressure decimal null,fsettempdyelaser decimal null,fsistempdyelaser decimal null,fsettemplaserplate decimal null,fsistemplaserplate decimal null,usflags integer null);");

	printf("done!\n");
	printf("Building table receivegsbstatustype...");

	//receiveGSBStatusType
	executeSQLCommand(conn, "CREATE TABLE receivegsbstatustype(timeval timestamp with time zone primary key,ucdatavalid integer null,timeofdaymaster timestamp with time zone null,timeofdaygsb timestamp with time zone null,etypeofgsb integer null, irawflowmfc1 integer null,irawflowmfc2 integer null,irawflowmfc3 integer null,irawpressureno1 integer null,irawpressureno2 integer null,irawpressureno3 integer null,irawpt100no1 integer null,irawpt100no2 integer null,irawpressureco1 integer null,irawpressureco2 integer null,irawpressureco3 integer null,irawpt100co1 integer null,itempadc0 integer null,itempadc1 integer null,ispare0 integer null,ispare1 integer null,ispare2 integer null,ispare3 integer null,uisetpointmfc0 integer null,uisetpointmfc1 integer null,uisetpointmfc2 integer null,uisetpointmfc3 integer null,uiavrfirmwarerevision integer null,uivalvecontrolword integer null,uivalvevoltageswitch integer null,uivalvevoltagehold integer null,uivalvevoltageis integer null,uivalvevurrent integer null,uiledcurrentset integer null,uiledcurrentis integer null,uiledvoltage integer null,dflowmfc1 numeric null,dflowmfc2 numeric null,dflowmfc3 numeric null,dpressureno1 numeric null,dpressureno2 numeric null,dpressureno3 numeric null,dpt100no1 numeric null,dpt100no2 numeric null,dpressureco1 numeric null,dpressureco2 numeric null,dpressureco3 numeric null,dpt100co1 numeric null,dtempadc0 numeric null,dtempadc1 numeric null,dledcurrentis numeric null,dledcurrentset numeric null,dledvoltage numeric null,dvalvevoltageswitch numeric null,dvalvevoltagehold numeric null,dvalvevoltageis numeric null,dvalvecurrent numeric null);");

	printf("done!\n");
	printf("Database built successfully!\n");


}


/*
*
* Fills the database with a binary file
*
* Reads the file, identifies the substructs and adds them to the database
*
*/

int insertDataFromFile(char* dbname, char* user, char* password, char* fileName, char** singleStructSQLArray, int* singleStructParameter, char*** arrayStructSQLArray, int* arrayStructParameter, char** testStruct){
	int i = 0;
	for(i = 0; i < 3; i++){
		printf("%s\n", arrayStructSQLArray[1][i]);
	}

	struct elekStatusType tempStatus;
	FILE *pFile;

	printf("Connecting to database...");
	PGconn *conn = createConnection(dbname, user, password);
	printf("done!\n");


	printf("Opening file %s...", fileName);
	if(!(openFileForReading(fileName, &pFile))){

		return 0;

	}

	//file exists and can be read from
	//it is assumed that it is a bynary-file
	if(pFile == NULL){

		printf("File pointer is NULL! Exiting!\n");
		return 0;

	}

	printf("Reading file...\n");

	int timeCounter = 0;
	while(fread(&tempStatus, sizeof(struct elekStatusType), 1, pFile) == 1){

		//adding, one at a time, structs to check if they are read correctly
		//insertCounterCardTypeIntoDatabase(singleStructSQLArray[0], singleStructParameter[0], tempStatus.CounterCardMaster, conn);
		//insertEtalonDataTypeIntoDatabase(singleStructSQLArray[1], singleStructParameter[1], tempStatus.EtalonData, conn);
		//insertFilamentCardTypeIntoDatabase(singleStructSQLArray[2], singleStructParameter[2], tempStatus.FilamentCard, conn);
		//insertBackplaneADCTypeIntoDatabase(singleStructSQLArray[3], singleStructParameter[3], tempStatus.BackplaneADC, conn);
		//insertNO2CounterCardTypeIntoDatabase(singleStructSQLArray[4], singleStructParameter[4], tempStatus.CounterCardNO2, conn);
		//insertInstrumentFlagsTypeIntoDatabase(singleStructSQLArray[5], singleStructParameter[5], tempStatus.InstrumentFlags, conn);
		//insertGPSDataTypeIntoDatabatse(singleStructSQLArray[6], singleStructParameter[6], tempStatus.GPSData, conn);
		//insertTSCDataTypeIntoDatabase(singleStructSQLArray[7], singleStructParameter[7], tempStatus.TimeStampCommand, conn);
		//insertButterflyTypeIntoDatabase(singleStructSQLArray[8], singleStructParameter[8], tempStatus.Butterfly, conn);
		//insertProfibusMFCStatusTypeIntoDatabase(singleStructSQLArray[9], singleStructParameter[9], tempStatus.profibusMFCData, conn);
		//insertRecieveLIFOHDataTypeIntoDatabase(singleStructSQLArray[10], singleStructParameter[10], tempStatus.LIFOHData, conn);
		//insertReceiveBahamasLightTypeintoDatabase(singleStructSQLArray[11], singleStructParameter[11], tempStatus.BahamasLightData, conn);
		//insertGreenLaserStatusTypeIntoDatabase(singleStructSQLArray[12], singleStructParameter[12], tempStatus.GreenLaser, conn);
		//insertBlueLaserStatusTypeIntoDatabase(singleStructSQLArray[13], singleStructParameter[13], tempStatus.BlueLaser, conn);
		//insertDyeLaserStatusTypeIntoDatabase(singleStructSQLArray[14], singleStructParameter[14], tempStatus.DyeLaserStatus, conn);
		//insertReceiveGSBStatusTypeIntoDatabase(singleStructSQLArray[15], singleStructParameter[15], tempStatus.GSB, conn);
		printf("%s\n", arrayStructSQLArray[0][0]);
		printf("%s\n", testStruct[0]);
		printf("%p\n", arrayStructSQLArray);
		insertADCCardTypesIntoDatabase(arrayStructSQLArray[0], arrayStructParameter[0], tempStatus, conn); //utf8 Encoding?
		//insertMFCCardTypeIntoDatabase(arrayStructSQLArray[1], arrayStructParameter[1], tempStatus, conn); // => PSQL-Error 7
		//insertValveCardTypeIntoDatabase(arrayStructSQLArray[1], arrayStructParameter[1], tempStatus, conn); // => PSQL-Error 7
		//insertDCDC4CardTypeIntoDatabase(arrayStructSQLArray[1], arrayStructParameter[1], tempStatus, conn); // => PSQL-Error 7
		//insertTempSensorCardTypeIntoDatabase(arrayStructSQLArray[1], arrayStructParameter[1], tempStatus, conn); // memory corruption
		//insertMirrorDataTypeIntoDatabase(arrayStructSQLArray[1], arrayStructParameter[1], tempStatus, conn); // => PSQL-Error 7

	}

}

int main(int argc, char** argv){

	//make sure that sqlCommands and parameternumber have the right size, i.e. allocated memory

	//intalling handler
	signal(SIGSEGV, handler);

	char* singleStructSQLArray[NUMBER_OF_STRUCTS];
	int singleStructParameter[NUMBER_OF_STRUCTS];

	//building arrays
	if(!(buildSQLCommandsAndNumberOfParametersForInsertionForStructs(singleStructSQLArray, singleStructParameter))){

		exit(0);

	}

	char** arrayStructSQLArray[NUMBER_OF_ARRAYSTRUCTS];
	int arrayStructParameter[NUMBER_OF_ARRAYSTRUCTS];

	if(!(buildSQLCommandsAndNumberOfParametersForInsertionForArraystructs(arrayStructSQLArray, arrayStructParameter))){

		printf("Error building the SQL-Commands for insertion of array-structs into the database.\nExited with (1)");
		exit(1);

	}

	printf("%s\n", arrayStructSQLArray[0][0]);
	printf("%p\n", arrayStructSQLArray);

	if(argc < 2){

		printf("To few arguments. Abort\n");
		exit(0);

	}else if(strcmp(argv[1],"build") == 0){

		if(argc != 5){

			printf("To few arguments for build. Additional arguments are needed in the following order:\ndbame user password\n");
			exit(0);

		}

		buildDatabase(argv[2], argv[3], argv[4]);

	}else if(strcmp(argv[1],"fillbyfile") == 0){

		if(argc != 6){

			printf("To few arguments for fillbyfile. Additional arguments are needed in the following order:\ndbname user password filename\n");
			exit(0);

		}
 
 		insertDataFromFile(argv[2], argv[3], argv[4], argv[5], singleStructSQLArray, singleStructParameter, arrayStructSQLArray, arrayStructParameter, arrayStructSQLArray[0]);

	}else{

		printf("No mode selected. Try 'build' or 'fillbyfile'\n");

	}

	//testing some const pointing

/* Some testing regarding constant chars
	char *nonconst[2];
	nonconst[0] = "test";
	nonconst[1] = "second";

	const char** constArray = (const char**) nonconst;
	nonconst[0] = "error";

	printf("%s %s\n", nonconst[0], nonconst[1]);
	printf("%s %s\n", constArray[0], constArray[1]);

	//const char *constarray[2] = (const cnonconst;
*/
	return 0;
}