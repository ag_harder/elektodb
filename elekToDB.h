#ifndef ELEKTODB   /* Include guard */
#define ELEKTODB

/*
* -------------------------------------------
* Includes
* -------------------------------------------
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <netinet/in.h> //htons hontl, etc
#include <libpq-fe.h> //psql frontend library
#include <execinfo.h> //stack trace after segfault
#include <signal.h>
#include <unistd.h>
#include "elekIO.h" //structure definitions

/*
* -------------------------------------------
* Definitions
* -------------------------------------------
*/
#define TIME_BUFFER_SIZE 128
#define INT_BUFFER_SIZE 64
#define DOUBLE_BUFFER_SIZE 128

//Magic-Numbers regarding column-numbers in a table - needs to be adjusted if the table-structure changes
#define ETALON_DATA_TYPE_COUNTER 35 //number of columns in the etalondatatype table
#define NUMBER_OF_STRUCTS 16 //number of structs in elekStatusType
#define NUMBER_OF_ARRAYSTRUCTS 6 //number of structs in elekStatusType

/*
* -------------------------------------------
* Enums
* -------------------------------------------
*/

enum singleStructs {COUNTERCARDTYPE, ETALONDATATYPE, FILAMENTCARDTYPE, BACKPLANEADCTYPE, NO2COUNTERCARDTYPE, INSTRUMENTFLAGSTYPE, GPSDATATYPE, TSCDATATYPE, BUTTERFLYTYPE, PROFIBUSMFCSTATUSTYPE, RECEIVELIFOHDATATYPE, RECEIVEBAHAMASLIGHTTYPE, GREENLASERSTATUSTYPE, BLUELASERSTATUSTYPE, DYELASERSTATUSTYPE, RECEIVEGSBSTATUSTYPE};

enum arrayStructs {ADCCARDTYPE, MFCCARDTYPE, VALVECARDTYPE, DCDC4CARDTYPE, TEMPSENSORCARDTYPE, MIRRORDATATYPE};

/*
* -------------------------------------------
* Helper functions - Parsing datatypes to chars
* -------------------------------------------
*/
char* concatenateChars(char* s1, char* s2);
char* parseTimeval(struct timeval tv);
int insertCharIntoCharArray(char* data, char** array, int position);
int insertUnsignedCharIntoCharArray(unsigned char* data, char** array, int position);
int insertIntIntoCharArray(int data, char** array, int position);
int insertUintIntoCharArray(unsigned int data, char** array, int position);
int insertuint8_tIntoCharArray(uint8_t data, char** array, int position);
int insertint16_tIntoCharArray(int16_t data, char** array, int position);
int insertuint16_tIntoCharArray(uint16_t data, char** array, int position);
int insertint32_tIntoCharArray(int32_t data, char** array, int position);
int insertuint32_tIntoCharArray(uint32_t data, char** array, int position);
int insertuint64_tIntoCharArray(uint64_t data, char** array, int position);
int insertDoubleIntoCharArray(double data, char** array, int position);
int insertFloatIntoCharArray(float data, char** array, int position);

/*
* -------------------------------------------
* helper functions - File Handler
* -------------------------------------------
*/
int openFileForReading(char* fileName, FILE** pFile);

/*
* -------------------------------------------
* Error handling and log functions
* -------------------------------------------
*/
void handler(int sig); //prints a stack trace in case of an error; useful for debugging
void exit_nicely(PGconn *conn);
void exit_nicely_with_res(PGconn *conn, PGresult *res);
void exit_nicely_with_res_and_error_code(PGconn *conn, PGresult *res, int errorCode, char *message);

/*
* -------------------------------------------
* Database functions
* -------------------------------------------
*/
PGconn* createConnection(char* dbname, char* user, char* password);
void executeSQLCommand(PGconn *conn, char* sqlString);
void executePreparedStatement(PGconn *conn, char* sqlString, int numberOfParams, const char** values, int lengths[], int binary[]);

/*
* -------------------------------------------
* SQl-String creation functions
* -------------------------------------------
*/
int buildSQLCommandsAndNumberOfParametersForInsertionForStructs(char** sqlArray, int* numberOfParameters);
int buildSQLCommandsAndNumberOfParametersForInsertionForArraystructs(char*** sqlArray, int* numberOfParameters);


/*
* -------------------------------------------
* Insert structs into database
* -------------------------------------------
*/
int insertCounterCardTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct CounterCardType data, PGconn *conn);
int insertEtalonDataTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct EtalonDataType data, PGconn *conn);
int insertBackplaneADCTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct BackplaneADCType data, PGconn *conn);
int insertFilamentCardTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct FilamentCardType data, PGconn *conn);
int insertNO2CounterCardTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct NO2CounterCardType data, PGconn *conn);
int insertInstrumentFlagsTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct InstrumentFlagsType data, PGconn *conn);
int insertGPSDataTypeIntoDatabatse(char* sqlCommand, int numberOfEntries, struct GPSDataType data, PGconn *conn);
int insertTSCDataTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct TSCDataType data, PGconn *conn);
int insertButterflyTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct ButterflyType data, PGconn *conn);
int insertProfibusMFCStatusTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct profibusMFCStatusType data, PGconn *conn);
int insertRecieveLIFOHDataTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct receiveLIFOHDataType data, PGconn *conn);
int insertReceiveBahamasLightTypeintoDatabase(char* sqlCommand, int numberOfEntries, struct receiveBahamasLightType data, PGconn *conn);
int insertGreenLaserStatusTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct greenLaserStatusType data, PGconn *conn);
int insertBlueLaserStatusTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct blueLaserStatusType data, PGconn *conn);
int insertDyeLaserStatusTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct DyeLaserStatusType data, PGconn *conn);
int insertReceiveGSBStatusTypeIntoDatabase(char* sqlCommand, int numberOfEntries, struct receiveGSBStatusType data, PGconn *conn);

/*
* -------------------------------------------
* Insert array-structs into database
* -------------------------------------------
*/
int insertADCCardTypesIntoDatabase(char** sqlCommands, int numberOfEntries, struct elekStatusType data, PGconn *conn);
int insertMFCCardTypeIntoDatabase(char** sqlCommands, int numberOfEntries, struct elekStatusType data, PGconn *conn);
int insertValveCardTypeIntoDatabase(char** sqlCommands, int numberOfEntries, struct elekStatusType data, PGconn *conn);
int insertDCDC4CardTypeIntoDatabase(char** sqlCommands, int numberOfEntries, struct elekStatusType data, PGconn *conn);
int insertTempSensorCardTypeIntoDatabase(char** sqlCommands, int numberOfEntries, struct elekStatusType data, PGconn *conn);
int insertMirrorDataTypeIntoDatabase(char** sqlCommands, int numberOfEntries, struct elekStatusType data, PGconn *conn);

/*
* -------------------------------------------
* Public functions
* -------------------------------------------
*/



#endif // ELEKTODB